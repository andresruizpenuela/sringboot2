# Cambiar el fondo desde el controlador: Web-Task-v1

### Application.properties

```properties
server.port=8090

#Connet to BBDD - MySQL
spring.datasource.url=jdbc:mysql://localhost:3306/test
spring.datasource.username=root
spring.datasource.password=
```

### Vista

```html
<!DOCTYPE html>
<html>
<!-- Plantilla de sitio de tres columnas en HTML5 -->
<head lang=”es” xmlns:th=http://www.thymeleaf.org>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="JMSR">
  <meta name="description" content="Plantilla para documento HTML 5">
  <meta name="keywords" content="HTML, CSS">
  
  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  
  <title>Cambia el fondo de blanco a oscuro</title>
</head>
<body>
	<h1>A Web Page</h1>
	<button type="button" onclick="myFunction()">Try it</button>
	
	<select id="myselect" onchange="myFuntion2()">
	    <option value="dark" th:selected="${theme=='black'}">Oscuro</option>
	    <option value="white" th:selected="${theme=='white'}">Claro</option>
	</select>
	<p id="demo" th:class=" | bg-${theme} |">A Paragraph</p>
	<!-- Optional JavaScript; choose one of the two! -->
	<script type="text/javascript">
        /*
        * Funcion llamada cuando se preisoan el botno
        */
		function myFunction() {
		  document.getElementById("demo").innerHTML = "Paragraph changed.";
		}
        /*
        * Funcion llamda cuadno cambia de estado el "seleector"
        */
		function myFuntion2(){ 
			var valueThemeText = $( "#myselect option:selected").text();
			var valueThemeValue = $( "#myselect option:selected").val();
			alert(valueThemeText+"-"+valueThemeValue);
             
             //Recargarmalos la pagina meidante AJAX
			window.location.replace("/udapteTheme?theme="+valueThemeValue);

		}
	</script>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
</body>
</html>
```

### Controlador

```java
package com.web.task.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class IndexController {
	
	@GetMapping("/")
	public String getIndexView(Model model) {
		//model.addAttribute("theme", "dark");
		model.addAttribute("theme", "white");
		return "indexView";
	}
	
    /*
    * Recibe el paraemtro y cambai del color
    */
	@GetMapping("/udapteTheme")
	public String udapteTheme(@RequestParam String theme, Model model) {
		Logger logger = LoggerFactory.getLogger(IndexController.class);
		logger.info(theme);
		model.addAttribute("theme", theme);
		return "indexView";
	}
}

```



Nota:

https://unpocodejava.com/2014/03/18/ajax-con-thymeleaf/