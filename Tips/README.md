# TIPS OF SPRING BOOT 2

## Plantilla HTML5

```html
<!DOCTYPE html>
<html>
<!-- Plantilla de sitio de tres columnas en HTML5 -->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="JMSR">
  <meta name="description" content="Plantilla para documento HTML 5">
  <meta name="keywords" content="HTML, CSS">
  
  <title>Plantilla para documento HTML 5</title>
  
  <link rel="icon" type="image/png" href="favicon.ico">
  <link rel="stylesheet" type="text/css" href="base.css">
  <style>
    *
    {
      margin: 0;
      padding: 0;
    }
    #espacioSuperior
    {
      background-color: #E6E6E6;
    }
    #contenedorPagina
    {
      margin: 0 auto;
      border: 1px solid #9900CC;
      height: 85%;
    }
    
    #encabezado
    {
      height: 50px;
      background-color: #FF3300;
    }
    #aLaIzquierda
    {
      width: 20%;
      height: 500px;
      float: left;
      background-color: #0066FF;
    }
    #contenidoPrincipal
    {
      width: 60%;
      float: left;
      background-color: #FFFFFF;
    }
    #aLaDerecha
    {
      width: 20%;
      height: 500px;
      float: right;
      background-color: #006600;
    }
    #piePagina
    {
      clear: both;
      height: 25px;
      background-color: #CC6699;
    }
  </style>
</head>
<body>
  <div id="espacioSuperior">
    <h1>Plantilla para documento HTML 5</h1>
  </div>
  <div id="contenedorPagina">
    <header id="encabezado">
      <p>#encabezado</p>
    </header>
    <aside id="aLaIzquierda">
      <p>#aLaIzquierda</p>
    </aside>
    <section id="contenidoPrincipal">
      <p>#contenidoPrincipal</p>
    </section>
    <aside id="aLaDerecha">
      <p>#aLaDerecha</p>
    </aside>
    <footer id="piePagina">
      <p>#piePagina</p>
    </footer>
  </div>
</body>
</html>
```

## HTML5 y Themelaf

Tendríamos que añadir este espacio de nombres a la etiqueta HTML:

```html
<!DOCTYPE html>
<html lang=”es” xmlns:th=http://www.thymeleaf.org>
```

## Ventana responsiva

Para generar una ventaga gráfica responsiva, debes añadir en la sección del `<head>`, la siguiente línea;

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```

## Añadir Boostrap vía CDN

### CSS

Para cargar la hoja de estilos de Boostrap ("stylesheet" - extension CSS) debes copiar debes cargarla en tu cabecera mediante "link"

```html
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
```

### JS

Muchos de los componentes requiere el uso de JavaScritpt para funcionar. Especialmente, requieren nuestros propios complementos de JavaScript y [Popper](https://popper.js.org/).

Coloque **uno de los siguientes `<script>` s** cerca del final de sus páginas, justo antes de la etiqueta de cierre `</body>`, para habilitarlos

#### Bundle

Bundle incluye todos los complementos y dependencias de JavaScript para Bootstrap. Tanto `bootstrap.bundle.js` como `bootstrap.bundle.min.js` incluyen **Popper** para nuestra información sobre herramientas y ventanas emergentes.

```html
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
```

#### Separete

Si decide optar por la solución de scripts por separado, Popper debe ser lo primero (*si está usando información sobre herramientas o ventanas emergentes*), y luego nuestros complementos de JavaScript

```html
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
```

### Template

```html
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <h1>Hello, world!</h1>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>
```

## Conectar Spring Boot a MySQL

**Dependencias:** Spring Data JPA, MySQL Driver

Agregar en el fichero **application.properties**:

```properties
#Connet to BBDD - MySQL
spring.datasource.url=jdbc:mysql://localhost:3306/test
spring.datasource.username=root
spring.datasource.password=
```

## Llamar función JavaScript al hacer click sobre el elemento

```html
<button type="button" onclick="myFunction()">Try it</button>
<p id="demo" th:class=" | bg-${theme} |">A Paragraph</p>
```

```javascript
function myFunction() {
	document.getElementById("demo").innerHTML = "Paragraph changed.";
}
```

## Ajax petición GET básica

```html
<select id="myselect" onchange="myFuntion2()">
	<option value="dark" th:selected="${theme=='black'}">Oscuro</option>
	<option value="white" th:selected="${theme=='white'}">Claro</option>
</select>
```

```html

<script type="text/javascript">   
	function myFuntion2(){ 
			var valueThemeText = $( "#myselect option:selected").text();

			alert(valueThemeText);
			$.ajax({
			    type: "GET",
			    url: "/udapteTheme",
			    data: { theme: valueThemeValue },
			    success: function (data) {
					alert(data.toString());
				},
				error: function () {
					alert("Error");
				}
			});

		}
</script>
```

## Recargar una pagina con Ajax

```javascript
window.location.replace("/URL");
```

## Formulario con Ajax

Enviar los datos por petición **GET**

```javascript
var formData = new FormData();
var request = new XMLHttpRequest();
request.open("GET", "/udapteTheme?theme="+valueThemeValue);
request.send(formData);
```

https://developer.mozilla.org/es/docs/Web/API/FormData/Using_FormData_Objects

## Cargar fragmentos thymeleaf con ajax

https://riptutorial.com/thymeleaf/example/28530/replacing-fragments-with-ajax 