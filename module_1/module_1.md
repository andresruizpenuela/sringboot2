# Desarrollo web con Spring MVC

_Modulo 1: Desarrollo de aplicaciones web con Spring MVC y Thymeleaf_

![Spring MVC](..\img\module_1\img1.png)

[TOC]

## Capítulo 1. ¿Qué es Spring MVC?

<div style="text-align: justify">
  <p>
        <img src="..\img\module_1\img2.png" style="zoom:50%; float:left;" />
        <strong>Spring MVC</strong> es un framework web basado en Servlets que viene includio en Spring Framework, y su nombre se debe a que implementa la arquitectura de aplicaciones cuyo patrón de diseño es el conocido MVC (<i><b>M</b>odel <b>V</b>iew <b>C</b>ontroller</i>.<br>
       Un <b>Servlets</b>, deriva de la palabra applet, y se reifere a programas que se ejecutan en el contexto de un navegador web, cuyo objetivo principal es la de crear páginas web de forma dinámica a paritr de los paráemtros de la petición que envia el navegador web.<br>
       Por otro lado, Spring MVC esta diseñado siguiendo el patrón de diesño de <b>Front Controller</b>, siendo el más usado en este framework el conocido como <b>DispatcherServlet</b>, cuya función principal es:
            <ul>
                <li>Enviar las peticiones (<i>requests</i>) a los manejadores (<i>handlers</i>) para que sean procesadas, teniendo que decir que los handler por defecto, son los famosos controladores (<i>"@Controller","@RequestMapping",...</i>)</li>
                <li>Resolver las vistas (<i>views</i>).</li>
        </ul>
</p>
</div>

### 1.1. ¿Qué es un Controlador en Spring MVC?.

<div style="text-align: justify">
  <p>Un Controlador (<i>Controller</i>) en Spring MVC es una clase normal a la cual se le agrega la anotaicón <b>@Controller</b> a nivel de la clase, estando además, sus métodos marcados principalmente con las anotaciones de "<i>Action Controller</i>", conocidas como @GetMapping, @PostMapping, @RequestMapping, etc.<br>
    Cada método puede tener un nombre cualesquiera y debe regresar por defecto un String cuyo valor es coincide con el nombre de la vista.<br>
    Cada método es ejecutado al ser invocado por medio de la URL escpevidicad como parámetro u opcion de la anotaciones de Action Controller.</p>
    Por otro lado, a partir de Spring 3.0 se puede crear RESTFul Web Services a través de la anotacioens <b>"@RestController"</b> y <b>"@PathVariable".</b>
    También esta basado en Spring IOC container (<i>Inyección de dependencias</i>) y Spring MVC se integraba facilmente con otros proyectos de Spring como <b>Spring Boot</b>,<b>Spring Data JPA</b>,<b>Spring Security</b>,<b>Spring REST</b>, etc.
        </p>
<img src="..\img\module_1\img3.png" style="zoom:100%; float:center;" />
<p>
    Normalmente, los controladores suele estar includios en un paquete específico que cuelga del paquete donde se encuentra la aplicación prinpical anotada por "@SpringBootApplication", la cual, se encargará de escanear automáticamente los controladores, entidades, servicios, etc.
</p>
</div>

#### 1.1.a. Ejemplo de un controlador sencillo: 02-ServerEmpelados-v1

<div style="text-align: justify">
  <p>Normalmente, los controladores suele estar includios en un paquete específico que cuelga del paquete donde se encuentra la aplicación prinpical anotada por "@SpringBootApplication", la cual, se encargará de escanear automáticamente los controladores, entidades, servicios, etc.<br>
      Un ejemplo, de un Servicio Spring MVC con un contralor que devuelve una página html sencilla, tiene la siguiente estructura:<br>
  </p>
  <img src="..\img\module_1\img4.png" style="zoom:80%; float:center;" />
</div>

##### 1.1.a.i. Dependencias.

| Dependencia                   | Descripción                                                  |
| ----------------------------- | ------------------------------------------------------------ |
| spring-boot-starter-thymeleaf | Motor de plantillas de Spring Boot que reemplaza JSP.        |
| spring-boot-starter-web       | Trae de forma transitiva todas las dependencias relacionadas con el desarrollo web. También reduce el recuento de dependencias de compilación. |
| spring-boot-devtools          | Herramienta de Spring Boot que nos permite reiniciar de forma automática nuestras aplicaciones cada vez que se produce un cambio en nuestro código |
| spring-boot-starter-test      | Herramienta de Spring Boot que nos permite realizar diferentes pruebas de nuestra aplicación web. |

##### 1.1.a.ii. Clase principal "EmpeladosApplication.java".

```java
package net.andres.empleados;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpleadosApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmpleadosApplication.class, args);
    }

}
```

##### 1.1.a.iii. Controlador "HomeController.java".

```java
package net.andres.empleados.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    public String mostrarHome(){
        return "home";
    }
}
```

##### 1.1.a.iv. Vista "home.html".

```html
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Página Principal</title>
</head>
<body>
    <h1>Bienvenido a Empelos APPs</h1>
</body>
</html>
```

##### 1.1.a.v. Llamada al handler "/".

```shell
$ Get -  http://localhost:8080/
```

![http://localhost:8080/](..\img\module_1\img5.png)

## Capítulo 2. ¿Qué es Thymeleaf?

<div style="text-align: justify">
  <p> <img src="..\img\module_1\img6.png" style="zoom:50%; float:left;" /><a href="https://www.thymeleaf.org"><b>Thymeleaf</b></a> es un motor de plantillas para aplicaciones web desarrolladas con Java, es algo similar a los JSPs, pero con algunas diferencias.<br>
      La documentación oficial la puedes encontrar en "www.thymeleaf.org" y es utilizado conmúnmente utilizado con Spring Boot ara generar vistas con código HTML para aplicaciones web.<br>
      Es un proyecto de Spring Boot, por lo que ya viene configuraro Thymeleaf con valores or defecto al momento de agregar la depenendica en el fichero <b>pom.xml</b>:
    </p>
</div>

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```

<div style="text-align: justify">
  <p> Lo primero a tener en cuenta a usar <b>Thymeleaf</b> en un archivo HTML es agregar el namespace en el mismo: "<code></code>"
   </p>
</div>

```html
<html xmlns:th="http://www.thymeleaf.org">
```

<div style="text-align: justify">
  <p> Así por ejemplo, el documento html tendría la siguiente estrucura base:
   </p>
</div>

```html
<!DOCTYPE html>

<html>
  <head>
    <title>Good Thymes Virtual Grocery</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  </head>
  <body>
    ...
  </body>
</html>
```

### 2.1. Ejemplo de Spring Boot 2 + Thymeleaf: 03-ServerHelloMundo-v1.

<div style="text-align: justify">
  <p> Para ver un ejemplo sencillo, sobre como integrar el motor de plantillas Thymeleaf a un proyecto Spring, vamos a crear un proyecto nuevo en al solicitar el recuerso "/", el controlador envía a la vista un mensaje de bienvenida.
   </p>
    <img src="..\img\module_1\img7.png" style="zoom:100%; float:center;" />
    <p>
        Cuya esctructura es la siguiente:
    </p>
    <img src="..\img\module_1\img8.png" style="zoom:100%; float:center;" />
</div>

#### 2.1.a. Dependencias & Propiedades.

##### 2.1.a.i. Dependencias.

| Dependencia                   | Descripción                                                  |
| ----------------------------- | ------------------------------------------------------------ |
| spring-boot-starter-thymeleaf | Motor de plantillas de Spring Boot que reemplaza JSP.        |
| spring-boot-starter-web       | Trae de forma transitiva todas las dependencias relacionadas con el desarrollo web. También reduce el recuento de dependencias de compilación. |
| spring-boot-devtools          | Herramienta de Spring Boot que nos permite reiniciar de forma automática nuestras aplicaciones cada vez que se produce un cambio en nuestro código |
| spring-boot-starter-test      | Herramienta de Spring Boot que nos permite realizar diferentes pruebas de nuestra aplicación web. |

##### 2.1.a.ii Propiedades.

```properties
server.port=8080
```

#### 2.1.c. Clase principal "HolamundoApplication.java".

```java
package net.andres.holamundo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolamundoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HolamundoApplication.class, args);
    }
}
```

#### 2.1.d. Controlador "HomeController.java".

```java
package net.andres.holamundo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

@Controller
public class HomeController {

    @GetMapping("/")
    public String mostrarHome(Model model){
        model.addAttribute("mensaje","Hola Mundo!");
        model.addAttribute("fecha",new Date());
        return "home";
    }
}
```

#### 2.1.e Vista "home.html".

```html
<!DOCTYPE html>
<html lang="es" xmlns:th="http:/tymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <h1 th:text="${mensaje}">Hello my friends!</h1>
    <h2> Hoy es [[${fecha}]]</h2>
</body>
</html>
```

#### 2.1.f Llamada al handler "/".

```shell
$ Get -  http://localhost:8080/
```

![http://localhost:8080/](..\img\module_1\img9.png)

### 2.2. Ejemplo De Spirng Boot 2 + Thymeleaf: 03-ServerAddDatosToView-v1.

<div style="text-align: justify">
  <p> En este ejemplo, se vera como enviar pasar datos simples del controlador al modelo, cuya estructura es la siguiente:  </p>
</div>

![Estructura del proyecto](..\img\anexos\anexo_4\img2.png)

#### 2.2.a.i. Dependencias & Propiedades.

##### 2.2.a.i.2. Dependencias.

| Dependencia                   | Descripción                                                  |
| ----------------------------- | ------------------------------------------------------------ |
| spring-boot-starter-thymeleaf | Motor de plantillas de Spring Boot que reemplaza JSP.        |
| spring-boot-starter-web       | Trae de forma transitiva todas las dependencias relacionadas con el desarrollo web. También reduce el recuento de dependencias de compilación. |
| spring-boot-devtools          | Herramienta de Spring Boot que nos permite reiniciar de forma automática nuestras aplicaciones cada vez que se produce un cambio en nuestro código |
| spring-boot-starter-test      | Herramienta de Spring Boot que nos permite realizar diferentes pruebas de nuestra aplicación web. |

##### 2.2.a.i.2. Propiedades.

```yaml
server:
  port: 8080 #Port run Server Web
```

#### 2.2.a.ii. Clase principal "ModelApplication.java".

```java
package net.andres.model;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModelApplication {

    public static void main(String[] args) {
        SpringApplication.run(ModelApplication.class, args);
    }

}
```

#### 2.2.a.iii. Controlador "HomeController.java".

```java
package net.andres.model.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.sql.Timestamp;

@Controller
public class HomeController {
    @GetMapping("/")
    public String mostrarHome(Model model){
        /* Defenimos las varaibles */
        String nombre = "Auxiliar de contabilidad";
        Timestamp fechaPublicacion = new Timestamp(System.currentTimeMillis());
        double salario = 9000.0;
        boolean vigente = true;

        /* La agregamos al Model*/
        model.addAttribute("titleWeb","Spring Boot + Thymeleaf");
        model.addAttribute("name",nombre);
        model.addAttribute("date",fechaPublicacion);
        model.addAttribute("salary",salario);
        model.addAttribute("status",vigente);
        return "home";

    }
}
```

#### 2.2.a.iv. Vista "home.html".

```html
<!DOCTYPE html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title th:text="${titleWeb}">Title</title>
</head>
<body>

 <h1 th:text="${titleWeb}"></h1>
 <h2>Bienvenido - APP de la ofertas de trabajo</h2>

 <h3 th:text="'Titulo:' + ${name}" ></h3>
 <p>
   Fecha de Publicación: [[${date}]]<br>
   Salario Ofrecido: [[${salary}]]
 </p>
 <p th:text="'Vigente: '+ ${status}"></p>

</body>
</html>
```

#### 2.2.a.v. Llamada al handler "/".

```shell
$ Get -  http://localhost:8080/
```

![http://localhost:8080/](../img\anexos\anexo_4\img3.png)



## Capítulo 3. El modelo.

<div style="text-align: justify">
  <p>Si seguimos el patrón MVC para el desarrollo de una aplicación Web, la <b>capa modelo</b> (<i>o de negocio</i>) de la aplicación, es la <u>capa más baja de acceso a datos y manipulación</u>.<br>Suele estar dividida en:
    <ul>
        <li>Entidad</li>
        <li>Repositorio</li>
        <li>Servicio</li>
    </ul>
    </p>
</div>

### 3.1. Entidades.

<div style="text-align: justify">
  <p>Son clases y objetos que <b>representan un elemento del modelo</b> y que, a menudo, tienen una correspondencia directa con una tabla de la base de datos.<br> Por defecto suelen estar dentro de un paquete denoamindo "entidades", además, las entidades estan caracterizada por: Tienen las siguientes características:
    <ul>
        <li>Estan anotadas como <i>@Entity</i></li>
        <li>Están anotadas como <i>@Table</i>. La cual, es una anotación a la que se le indica el nombre de la tabla que «<i>mappea</i>«.</li>
        <li>Define los atributos de la clase/table.Para cada uno de ellos se le indica si es identificador, qué tipo de dato es, el nombre de columna, relación con otras tablas, etc.</li>
    </ul>
    Por ejmplo <a href="https://github.com/cleventy/springboilerplate/tree/master/src/main/java/com/cleventy/springboilerplate/business/entities">Ejemplo de una capa de modelo</a>:
    </p>
</div>

```java
@Entity
@Table(name = "USER")
@Data @NoArgsConstructor @AllArgsConstructor @Builder
public class User implements Serializable {

    private static final long serialVersionUID = 3233149207833106460L;
    
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    
    @Column(name = "USERNAME")    
    private String username;
    
    @Column(name = "PASSWORD")
    private String password;
    
    @Column(name = "EMAIL")
    private String email;
    
    @Column(name = "NAME")
    private String name;
    
    @Column(name = "ROLE")
    private String role;
    
    @Column(name = "STATE")
    private Integer state;
    
    @Column(name = "REGISTER_DATE")
    private Calendar registerDate;
    
    @Version
    @Column(name = "VERSION")
    private Long version;

}
```

```java
public class UserConstants {

    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_USER = "USER";
    public static final String ROLE_SHOP = "SHOP";
    public static final Set<String> ROLES = new HashSet<>(Arrays.asList(ROLE_ADMIN, ROLE_USER, ROLE_SHOP));

    public static final Integer STATE_INACTIVE = Integer.valueOf(0);
    public static final Integer STATE_ACTIVE = Integer.valueOf(1);
    public static final Set<Integer> STATES = new HashSet<>(Arrays.asList(STATE_INACTIVE, STATE_ACTIVE));

}
```

### 3.2. Repositorio.

<div style="text-align: justify">
  <p>Los repositorios son las clases encargadas de <b>gestionar el acceso a los datos</b>. Suele estar también están bajo el mismo paquete que las entidades, aunque podmeos encontarlas en unpaquete especiico para repositorios, y su principal diferencia es que su nombre termina en <i>Repository</i>. Algunas características de los repositorios son:
    <ul>
        <li>Son interfaces que extienden el repositorio base <a href="https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html)">CrudRepository</a></li>
        <li>Disponen de métodos básicos de <a href="https://es.wikipedia.org/wiki/CRUD">CRUD</a>.</li>
        <li>Tienen un nombrado estándar que permite su implementación automática. Además, podemos definir nuestras propias consultas (<i>con la ayuda, por ejemplo, de la anotación "<a>@Query</a>"</i>) (<i>Más información en <a href="https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#reference">documentación de JPA</a></i>
.</li>
    </ul>
    Por ejmplo:
    </p>
</div>

```java
public interface UserRepository extends CrudRepository<User, Long> {

	public User findByUsername(String username);

	public User findByEmail(String email);
	
	public Iterable<User> findAllByOrderByIdAsc();
	
	Page<User> findAllByOrderByIdAsc(Pageable pageable);

	public boolean existsByUsername(String username);
	public boolean existsByEmail(String email);
}
```

### 3.3. Servicio.

<div style="text-align: justify">
  <p>Los servicios son clases que se encargan de la <b>capa de negocio</b> de la aplicación. Para ello, normalmente, <b>acceden a los datos</b> almacenados en la base de datos de la aplicación a través de los repositorios, hacen una serie de <b>operaciones</b>, y envían los datos al controlador. Algunas características de los repositorios son:
    <ul>
        <li>Estar anotados por <b>@Service</b> <a href="https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html)">CrudRepository</a></li>
        <li>Suele implementar una interfaz fachada, como por ejemplo puede ser <a href="https://github.com/cleventy/springboilerplate/blob/master/src/main/java/com/cleventy/springboilerplate/business/services/userservice/UserService.java">IUserService</a>, que es llamada por una capa superior (controlador) <a href="https://es.wikipedia.org/wiki/CRUD">CRUD</a>.</li>
        <li>Son métodos que suelen acceder a base de datos y por lo tanto tienen su transaccionalidad definida con la anotación *@Transactional*, diferenciando si realizan escrituras, únicamente lecturas, casos de *rollback*, etc.</a></i>
		<li>Construír/Utilizar objetos personalizados que serán devueltos al controlador (bajo el paquete <a href="https://github.com/cleventy/springboilerplate/tree/master/src/main/java/com/cleventy/springboilerplate/business/services/userservice/cos">custom object’s</a></li>
    </ul>
    Por ejmplo:
    </p>
</div>

```JAVA
public interface IUserService {
	
	public List<UserBasicDetailsCO> getUsers();
	public Chunk<UserBasicDetailsCO> getUsers(Integer page);

	public User findUserAndCheckNullAndState(String username) throws InstanceNotFoundException;
	public User findUserAndCheckNullAndState(Long userId) throws InstanceNotFoundException;

	public UserDetailsCO createUser(UserDetailsForm userDetailsForm) throws DuplicateInstanceException;
	public UserDetailsCO getUser(Long userId) throws InstanceNotFoundException; 
	public UserDetailsCO updateUser(Long userId, UserDetailsForm userDetailsForm) throws InstanceNotFoundException, DuplicateInstanceException;
	public UserDetailsCO deleteUser(Long userId) throws InstanceNotFoundException;

	public void sendWelcomeEmail(Long userId) throws InstanceNotFoundException, IOException, MessagingException;

}
```

```java
public class UserServiceImpl implements UserService {
	
	@Autowired
    private UserRepository userRepository;
    
	@Autowired
    private PasswordEncoder passwordEncoder;
    
	@Autowired
    private EmailSender emailSender;
    
	@Override
	@Transactional(readOnly = true)
	public List<UserBasicDetailsCO> getUsers() {...}

	@Override
	@Transactional(readOnly = true)
	public Chunk<UserBasicDetailsCO> getUsers(Integer page) {...}

	}
	
```

### 3.4. Ejemplo de Spring Boot 2 + Capa modelo: 02-ServerEmpleados-v2.

<div style="text-align: justify">
  <p> Para ver un ejemplo sencillo, sobre como integrar nuestra capa de modelo a un proyecto Spring, vamos a crear un ejempleo de Empleados, donde se creará la entidad "Vacante" y haremos uso de la misma, teniendo la siguiente estructura:
   </p>
    <img src="..\img\module_1\img10.png" style="zoom:100%; float:center;" />
</div>

## Capítulo 4. Condicionales en Thymeleaf.

<div style="text-align: justify">
  <p> Para aplicar condiciones en nuestro documento web con Thymeleaf, utilizamos la propiedad "<b>th:if/th:unless</b>".
   </p>
</div>
### 4.1. Operador Elvis (?:).

<div style="text-align: justify">
  <p> El operadro Elvis permite renderizar texto DENTRO de un elemento HTML, dependiendo de una expresión Booleana. Es muy similar al operador ternairo de otros lenaguajes de programación.
      <br>
      Ejemplo:
   </p>
</div>

```html
<td th:text="${usuario.estatus == 1} ? 'ACTIVO' : 'BLOQUEADO' " />
```

### 4.2. Operador if-unless.

<div style="text-align: justify">
  <p> La expreisón <b>if-unless</b> permite renderizar un elmentto HTML, dependiendo de una expresión Booleana. Es muy similar a un <b>if-else</b> en otors lenagues de programaicón.
      <br>
      Ejemplo:
   </p>
</div>

```html
 <td>
 	<span th:if="${alumno.genero == 'F'}"> Femenino </span>
 	<span th:unless="${alumno.genero == 'F'}"> Masculino</span>
 </td>
```

### 4.3. Ejemplo de Spring Boot 2 + Condicionales Thyemeleaf + Boostrap: 02-ServerEmpelados-v3

<div style="text-align: justify">
  <p> Para ver un ejemplo sencillo, sobre como integrar nuestra capa de modelo a un proyecto Spring, vamos a crear un ejempleo de Empleados, donde se creará la entidad "Vacante" y haremos uso de la misma, teniendo la siguiente estructura:
   </p>
    <img src="..\img\module_1\img12.png" style="zoom:100%; float:center;" />
    <p>
        Podemos ver que la estrucutra es la misma que la expuesta en el ejemplo <b>"<i>3.4. Ejemplo de Spring Boot 2 + Capa modelo: 02-ServerEmpleados-v2</i>"</b>, ya que la diferencia esta a nivel de código, donde se añade un parametro "outstanding" a la entidad <b>Vacante</b> para saber si la oferta es <i>destacada (1)/no destacada (0)</i>", tal que, el documento web podemos indicar Si y No, y aplicarle estilos de Boostrap diferentes.
    </p>
</div>

* Fragmento de código de "tabla.html", donde comprueba si la oferta es destacada o no.

```html
....
<table style="width: 100%" border="2" class="table table-hover ">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Fecha Publicación</th>
                <th>Descripción</th>
                <th>Salario</th>
                <th>Destacado</th>
            </tr>
        </thead>
        <tbody>
            <tr th:each="vacante:${vacantes}">
                <td th:text="${vacante.id}"></td>
                <td>[[${vacante.name}]]</td>
                <td>[[${vacante.date}]]</td>
                <td>[[${vacante.description}]]</td>
                <td>[[${vacante.salary}]]</td>
                <td>
                    <!-- Comprobamos si la vacante esta destacada o no -->
                    <span th:if="${vacante.outstanding == 1}" class="badge bg-success"> Si </span>
                    <span th:unless="${vacante.outstanding == 1}" class="badge bg-danger"> No </span>
                </td>
            </tr>
        </tbody>
    </table>
....
```

* Resultado visual, de aplicar el condicional.

![Resultado de condicionales](..\img\module_1\img11.png)

## Capítulo 5. URLs relativas al ContextPacth.

<div style="text-align: justify">
  <p> Las <b>URLs</b> relativas al ContextPatch son todas aquellas direccións que son relativas al directorio raíz (<i>ROOT</i>) de una aplicación web, una vez que están publicadas en el servidor web. Además, se caracterizan por:
    <ul>
    	<li>Las URLs relativas al ContexPath deben iniciar con "/" cuando vayamos a formar una URL para referenciar un recueros (<i>imagenes, CSS, JS, PDF, etc</i>) en nuestra aplicación.</li>
    	<li>En un proyecto web cuando se utiliza <b>Thymeleaf</b> como motor de plantillas, los recursos estáticos deben guardarse por defecto en el directorio <i>src/main/resources/static</i> </li>
    </ul>
    Por ejemplo:
    </p>
</div>


![](..\img\module_1\img13.png)

## Capítulo 6. Imágenes en Thymeleaf.

<div style="text-align: justify">
  <p> A continuación, veremos un ejemplo mediante la creación de un proyecto Spring Boot 2, como insertar y usar imegenes en nuestra aplicación tanto de manera estática como de manera dinámica.
    </p>
</div>

### 6.1. Imágenes Estáticas : 02-ServerEmpleados-v4

<div style="text-align: justify">
  <p> La estructura será la misma que hemos utilizado en el ejemplo anterior de nuestra aplicaicón de Empresas, pero con la salvedad, a la cual, le vamos a incrustar un logo en el documento web "listado.html" utilizando <b>Thymeleaf</b>.
    </p>
</div>

![](..\img\module_1\img14.png)



<div style="text-align: justify">
  <p> La principal diferencia, es la insercción de la imagne "logo.jfif" en la carpate <i>images</i> y la siguiente línea el documento web "listado.html"
    </p>
</div>

```html
<img th:src="@{/images/logo.jfif}" width="136" height="136">
```

<div style="text-align: justify">
  <p> El resultado visual, sería el siguiente:
    </p>
</div>

![](..\img\module_1\img15.png)

### 6.2. Imágenes Dinámicas : 02-ServerEmpleados-v5

<div style="text-align: justify">
  <p> La estructura será la misma que hemos utilizado en el ejemplo anterior para demostrar como insterar agregar imagenes estáticas, pero con la salvedad, de que vamos a a realizar las siguientes modificaciones:
      <ul>
          <li>En el documento web "tabla.html" vamos expecificar el nombre de la imagen mediante un atributo, utilizando la sintaxis siguiente:</li>
    </ul>
    </p>
</div>

```html
<img th:src="@{/patch/{img}} (img=${atributo})">
<!-- usando una variable temporal en lugar de -->
<img th:src="@{/images/imagen.png}"">
<!-- Ejemplo -->
<img th:src="@{/image/{img} (img=${vacante.imagen}) }"
```

<div style="text-align: justify">
  <p>
      <ul>
          <li>A la entidad <b>Vacante</b>, vamos añadir un nuevo atributo denomiando "<i>imagen</i>", el cual, contendra el nombre la imangen asociada a la vacante, cuyo valor por defecto será una imagen común para aquellas vacantes que no usen imange, por ejemplo "no-imagen.jpg"</li>
    </ul>
    De esta menera, podemos mostrar en el documento web, diferentes imagenes en función de la vacante.<br>
    La estrucutra del proyecto sería la siguiente:
    </p>
</div>

![](..\img\module_1\img16.png)

<div style="text-align: justify">
  <p>
      Siendo el resultado visual:
    </p>
</div>

![](..\img\module_1\img17.png)

## Capítulo 7. Ciclo de vida de una petición HTTP.

<div style="text-align: justify">
  <p>
      En la siguiente imagen se muestra el fujo o la secuencia que siguie una petición HTTP por defecto en una aplicación Spring MVC a modo resumen.
    </p>
</div>

![](..\img\module_1\img18.png)

<div style="text-align: justify">
  <p>
      El ciclo de vida de una petiicón HTTP empieza cuando un usuario realiza una petición a una aplicación web desarrollada en Spring MVC y la cual esta alojada en un servidor (<i>la petición se lanzaría mediante una llamda url "http://name-servidor:port/recurso"</i>.<br>
      El servidor Web tiene por lo general integrado un motor para procesar servlets y JSP, denomiando "<b>Servlet Engine</b>", siendo el más usado en Spring MVC el motor ya conocido <i>Apache Tomcat</i>.<br>
      Los serlver son conocidos como <b>Front Controller</b> o <b>DispatcherServlet</b>, se encarga de gestioanr las peticiones, es decir, recibe la petición y la envía al método (<b>handle</b>) del controlaldor correspondiete y generar la vista que será enviada como respueta a la petición, ya sea un documento web, un recurso, un json, etc.
    </p>
</div>

## Capítulo 8. Controladores.

<div style="text-align: justify">
  <p>
      Un controlador, aunque es más conocido con el termino controller (en inglés), es el encargado de responder a los eventos.
      <br>
      Usualmente es el encargado de preparar el modelo (<i>los datos manejados por la aplicación</i>) y seleccionar el nombre de la vista que será utilizada para mostrar el modelo al cliente. El modelo es una implementación de la interface Map en la cual podemos almacenar datos en formato "clave/valor", los cuales, serán enviados a la vista para su correcta representación, puede ser: HTML, PDF, etc., debemos decir que un controlador es capaz de generar una respuesta sin necesidad de una vista, esto es útil a la hora de crear servicios que generan respuestas en formatos como: XML, JSON, etc.
<br>
Típicamente definimos los controladores usando las anotaciones <b>@Controller</b>y <b>@RestController</b>, mientras que las peticiones (handler) que gestionará el controaldor son anotadas mediante <b>@RequestMapping</b>, por otro lado y además de la clase <b>ModelAndView</b> usada para devolver el modelo y el nombre de la vista.<br>
      En Spring MVC la creación de controladores es muy flexible, podemos crearlos usando distintos métodos, con o sin anotaciones.
<br>
      Por último, usamos la anotación <b>@Controller</b> para establecer las clases que serán usadas como controladores, teniendo que indicarle a Spring donde encontrar estas clases si se encuentra en un paquete difernte al paquete donde se encuentra la clase principal del proyecto, para ello usaremos:
      <ul>
          <li>Si la configuración es con anotaciones spring.</li></ul>
    </p></div>

```java
@ComponentScan(basePackages = {"..."})
```

<div style="text-align: justify">
  <p>
      <ul>
          <li>Si la configuracción es con mediante anotaciones XML.</li>
    </ul>
    </p>
</div>

```html
<context:component-scan base-package="..." />
```

<div style="text-align: justify">
  <p>
    Donde base-package indica el paquete donde se encuentran las clases controladores.
    </p>
</div>
> :information_source: Recuerda: Para generar un petición GET desde un docuemnto web, tenemos las siguientes opciones:
>
> * Formulario GET.
>
> ```html
> <form th:href="@{/vacantes/detalle}" method ="get">
> ```
>
> * Link estático.
>
> ```html
> <a th:href="@{/vacantes/detalle}">Detalle</a>
> ```
>
> * Link dinamico.
>
> ```html
> <a th:href="@{/vacantes/view/{id} (id=${vacante.id})  }">Detalle</a>
> ```

### 8.1. Anotación "@RequestMapping".

<div style="text-align: justify">
  <p>
      La anotación <b>@RequestMapping</b> nos permite indicar la petición a la responderá un determinado método, y suele incluirse antes de la delcaraicón de un método dentro de un contorlador, permitiendo especificar:
      <ul>
          <li>La dirección URL</li>
          <li>El método HTTP soportaod (<i>POST, GET, DELETE, PUT, etc</i>)</li>
    </ul>
    En versioens anterioes de <b>Spring 4.03</b> se utilizaba esta anotaicón para mapear los métodos de los controaldores a las URLs pero a artir de la misma se agregaron las siguientes variaciones de la anotaicón <b>@RequestMapping</b>:
    </p></div>

| Anotaciones (Spring 4.3+)  | Mapeo con la anotación @RequestMapping                       | Uso común                                              |
| -------------------------- | ------------------------------------------------------------ | ------------------------------------------------------ |
| @GetMapping("/lista")      | @RequestMapping(value="/lista", method=RequestMethod.GET)    | Desarrollo de aplicaciones web y RestFull WebServices. |
| @PostMapping("/guardar")   | @RequestMapping(value="/guardar", method=RequestMethod.POST) | Desarrollo de aplicaciones web y RestFull WebServices. |
| @DeleteMapping("/borrar")  | @RequestMapping(value="/borrar", method=RequestMethod.DELETE) | Desarrollo de RestFull WebServices.                    |
| @PutMapping("/actualizar") | @RequestMapping(value="/actualizar", method=RequestMethod.PUT) | Desarrollo de RestFull WebSerivces                     |

Ejemplo de y controlador sencillo seriá:

```java
@Controller
public class CategoriasController {
// @GetMapping("/index")
@RequestMapping(value="/index", method=RequestMethod.GET)
public String mostrarIndex(Model model) {
return "categorias/listCategorias";
}
// @GetMapping("/create")
@RequestMapping(value="/create", method=RequestMethod.GET)
public String crear() {
return "categorias/formCategoria";
}
// @PostMapping("/save")
@RequestMapping(value="/save", method=RequestMethod.POST)
public String guardar() {
return "categorias/listCategorias";
}
```

Ejemplo de llamadas al controlador:

![](..\img\module_1\img19.png)

#### 8.1.a. URL dinámicas. - @PathVariable.

<div style="text-align: justify">
  <p>
      Las URLs dinámicas (<i>URI template</i>) son usadas para obtener parte de ellaas en un método de un controlador  como parametro denominada <b>PathVariable</b> y que van dentro de llaves "{}".<br>
      Una URL dinámica se caractiera por:
      <ul>
          <li> Contner 1 o varias PathVariables</li>
          <ul>
              <li>URL dinámica con un parámetro - http://localhost:8080/detalle/{id}</li>
              <li>URL dinámica con dos parámetro - http://localhost:8080/detalle/{id}/{fecha
              </li>
          </ul>
          <li>Para vincular (<i>binding</i>) un parámetro de una URL dinámica a un parámetro en el controlador se utiliza la anatación <b>@PathVariable</b></li>
    </ul>
  	Ejemplo:
    </p></div>

```java
// Recogida de un parámetro
@GetMapping("/detalle/{id}")
public String mostrarDetalle(@PathVariable("id") int idVacante){
System.out.println("PathVariable: " + idVacante);
return "detalle";
}

// Varios parámetros
@GetMapping("/detalle/{id}/{fecha}")
public String mostrarDetalle(@PathVariable("id") int idVacante, @PathVariable("fecha") Date fecha){..}

// Recogida de varios parablmeos en un Map
@GetMapping(value = "/book/{author}/{title}")
public void process3(@PathVariable Map<String, String> vals) {

   logger.info("{}: {}", vals.get("author"), vals.get("title"));

}
```

Ejemplo de llamadas url dinámicas

![](..\img\module_1\img20.png)


#### 8.1.b. Parámetros - @RequestParam.

<div style="text-align: justify">
  <p>
      Los parámetros de una petición HTTP pueden ser vinculados (<i>binding</i>) a un parámetro en un método en el controlador mediante la anotación de Spring MVC <b>@RequestParam</b>, independientemente del método HTTP empleado.<br>
      Por ejemplo:
    </p></div>

```java
@GetMapping("/detalle")
public String verDetalle(@RequestParam("idVacante") int idVacante){
// Procesamiento del parámetro. Aquí, ya se hizo la conversión a String a int.
System.out.println("RequestParam: " + idVacante);
return "someView";
}
```

<div style="text-align: justify">
  <p>
      Por defecto los parámetros con esta anotación son <b style="color:red">REQUERIDOS</b>, aunque se puede modificar meidante el atributo "<b>required</b>" para que no sea obligatorio, si se pone su valor a <i>false</i>.<br>Por ejemplo:
    </p></div>

```java
@RequestParam(name="id",required=false)
```

Ejemplo de una llamada don parámetros GET:

![](..\img\module_1\img21.png)

Ejemplo de una llamada con parámetros POST:

![](..\img\module_1\img22.png)

### 8.2. Ejemplo de Spring Boot 2 + Controlador: 04-ServerEmpleados-v1.

![](..\img\module_1\img23.png)

## Capítulo 9. Inyección de dependencias (ID)

<div style="text-align: justify">
  <p>
      El procedimiento que vamos a utilizar para inyectar (<b>@Autowired</b>) una calse de servicio en un controlador sera el siguiente:
      <ol>
         <li>Anotar la clase servicio (<i>la implementación</i>) con la anotaicón <b>@Service</b> de Spring Framwerk</li>
    </ol>
    </p></div>

```java
package net.andres.service;
@Service
public class VacantesServiceImpl implemts IVacantesService{
	//Métodos de la lógica de negocio
	@Override
	public List<Vacante> buscarTodas(){
		return lista;
	}
}
```

> :notebook: Por defecto, las clases de servicio tiene alcance "Singleton" (_una sola instancia de la clase para toda la aplicación_)

<div style="text-align: justify">
  <p>
      <ol start="2">
         <li>Inyectamos en nuestro controlardor, una instancia  de la clase de servicio, de tipo interfaz.</li>
    </ol>
    </p></div>

```java
@Autowired
private IVacanteService serviceVacnates;
```

<div style="text-align: justify">
  <p>
      <ol start="3">
         <li>Utilizmaos los métodos de la clase de serivcio en el controladror.</li>
    </ol>
    </p></div>

```java
@Controller
public class HomeController{
	@Autowired
	private IVacantesServices serviceVacantes;
	
	@GetMapping("/tabla")
	public String mostrarTabla(Model model){
		List<Vacante> lista = serviceVacantes.buscarTodas();
		model.addAttribute("vacantes",lista);
		return "tabla";
	}
}
```

### 9.1. Ejemplo de Spring Boot 2 + ID : 05-ServerEmpelados-v1.

![](..\img\module_1\img24.png)

## Capítulo 10. Fragmentos con Thymeleaf.

<div style="text-align: justify">
  <p>
     Normalmente las aplicaciones web comparten componentes (<i>fragmentos de código HTML</i>) que se repiten en cada vista.<br>
      Algunos ejemplos son:
      <ul>
          <li>Cabecera (header).</li>
          <li>Menús.</li>
          <li>Pie de página (footer).</li>
          <li>y posiblemente muchos más.</li>
    </ul>
	En estos casos se recomienda separar este código repetitivo en archivos externos y solo mandarlos llamar en las vistas cada que sean requeridos, permitiendo así:
    <ul>
        <li>Evitar reptir el mismo código en cada vista.</li>
        <li>Cuando se requiera un cambipo por ejemplo, el header o agregar un nuevo menú, solo hace flata editar un archivo, aunque los cambios se verán reflejados en todas las vistas que esten incluidso estos archivos externos</li>
    </ul>
    Para este tipo de diseños de plantillas, Thymeleaf incluye las siguietes expresiones principalmente:
    <ul>
        <li><b style="color:red;">th:fragment</b> -> Permite difinir un fragmento de código HTML en un archivo externo (<i>código HTML que es común en nuestras vistas).</i></li>
        <li><b style="color:red;">th:insert</b> -> Permite <span style="color:green">INSERTAR EL CÓDIGO HTML</span> en un fragmento definitivo previamente, en el TAG en donde este declarada esta expresión.</li>
    </ul>
    Por ejemplo:
    </p></div>

![](..\img\module_1\img25.png)

<div style="text-align: justify">
    <p>
        Definimos un Fragmento de código HTML con el nombre de "menu-principal" y otro con el nombre "pie-pagina" y lo garadas en el directorio templates o en subdirectorio del mismo, por ejemplo "template/fragments", en eseos fragmentos isnertamos el codigo correspondinete.
    </p>
</div>

![](..\img\module_1\img26.png)

<div style="text-align: justify">
    <p>
        Por último en los diferentes docuementos web donde queremos ver los fragmentos, los agregamso en los contededores correspondientes.
    </p>
</div>

![](..\img\module_1\img27.png)

### 10.1. Ejemplo de Spring Boot 2 + Thymeleaf+Boostrap+Fragments: 02-ServerThymeleaf-Fragments

<div style="text-align: justify">
    <p>
        Para ver un poco mas de detalle, vamos a crear un proyecto en Spring Boot 2 con Thymeleaf donde vamos a insertar fragmenos y boostrap, la estructura es la siguiente:
    </p>
</div>
#### 10.1.a. Dependencias & Propiedades.

##### 10.1.a.i. Dependencias.

| Dependencia                   | Descripción                                                  |
| ----------------------------- | ------------------------------------------------------------ |
| spring-boot-starter-thymeleaf | Motor de plantillas de Spring Boot que reemplaza JSP.        |
| spring-boot-starter-web       | Trae de forma transitiva todas las dependencias relacionadas con el desarrollo web. También reduce el recuento de dependencias de compilación.<br>- Spring Web<br>- Spring Web Services |
| spring-boot-devtools          | Herramienta de Spring Boot que nos permite reiniciar de forma automática nuestras aplicaciones cada vez que se produce un cambio en nuestro código |
| spring-boot-starter-test      | Herramienta de Spring Boot que nos permite realizar diferentes pruebas de nuestra aplicación web. |

##### 10.1.a.ii. Propiedades.

<div style="text-align: justify">
En el fichero "<b>applicacitón.properties</b>" ubicado en la ruta <i>"src/main/resources"</i>, añadimos las siguientes propierdades:</div>

```properties
#Propieades Genericas del Proyecto
author=Andrés Ruiz Peñuela
copy=(C) Todos los derechos reservados
versionApp=v0.1
#Nombre del proyecto
spring.application.name=Templates Fragment with Web Empleos
#Configuracion de apache
server.port=9090
```

#### 10.1.b. Paquete de configuración.

<div style="text-align: justify">
No se incluira ficheros de configuración, nos centraermos solo en el tema de plantillas</div>

#### 10.1.c. Servicio "Home".

<div style="text-align: justify">
Se encarga de mostrar la pagina princpal de nuestra aplicación, tal y como muestra la imagen de abajo, entre otros servicios secundarios descritos posteriormente</div>


![Home-view](../img/anexos/template-thymelaf/home-view.png)

<div style="text-align: justify">
Este servicio contara con los siguientes métodos:
<ul>
    <li>Get: /, Devuelve la pagina principal del servicio pasando a la vista cada una de las ofertas cargadas en el sistema.</li>
</ul></div>
#### 10.1.d. Servicios de "Vacantes".

<div style="text-align: justify">
Representa la estructura de datos de una Vacante cuenta con los siguientes ficheros:</div>



![](../img/module_1/files-servicio-vante.png)

<div style="text-align: justify">
Completará las siguientes funcionalidades básicas:</div>

```java
	//Obtiene todas las vacantes diponibles
	List<Vacante> buscarTodas();
	//Busca la vacante dada un identificador
	Vacante buscarPorId(Integer idVacante);
	//Eño,oma ña vacamte dadp im identificador
	boolean borrarVacante(Integer idVacante);
```

<div style="text-align: justify">
Además, a través del controlador principal "HomeController" podremos se ofrecerá los siguientes servicios</div>

```java
	 
	 @GetMapping("/") //Devuelve la página principal 
	 @GetMapping("/tabla") //Muestra una pagina web con todas las vacantes en una table
		
	 
	  /** METODOS QUE DEVUELVEN JSON **/
	  	//Devuelve un JSON con todas las vacantes
	  @RequestMapping(value="/allOferts", method=RequestMethod.GET)  
	  //@RequestMapping(value="/allOfertsJson", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	  
	 	//Devuelve un JSON con las vacantes ordenadas por defecto
	  @RequestMapping(value="/allOfertsOrderDefault", method=RequestMethod.GET) 
	  //@RequestMapping(value="/allOfertsJson", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	  
	  	//Devuelve un JSON con todas las vacantes ordenadas por fecha
	  @RequestMapping(value="/allOfertsOrderDate", method=RequestMethod.GET)
	  //@RequestMapping(value="/allOfertsJson", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
```

<p style="text-align: justify">Y del controlador "VacantesController", se realiza los siguientes servicios:</p>

```java
	@Autowired
	IVacanteService daoVacantes;

	@GetMapping("/delete") //Bprrar una vacante, dado un id
	@GetMapping("view/{id}") //Mostrar una vancate dodo un id
```

#### 10.1.e. Servicio de "Categorías".

<p style="text-align: justify">Permite añadir a las vacntes una categíra, donde solo constara un controlador "CategoriasController" para guardar una categoría a través de un formualrio</p>

```java
	@RequestMapping(value="/create",method = RequestMethod.GET)

	// @PostMapping("/save")
	@RequestMapping(value="/save",method = RequestMethod.POST)

```

#### 10.1.f. Fragmentando.

<p style="text-align: justify">Se crearán dos fragmentos que son los repetidos en todas las vidas o en la mayoría de las vistas: 
<ul>
    <li>Menú de cabecera</li>
    <li> píe de pagina</li>
</ul>
Tal y como se visualiza en la imagen</p>

![](..\img\module_1\framgents-focus.png)

<p style="text-align: justify">Creando la siguiente estructura:</p>

![](..\img\module_1\struct-fragmens.png)

<p style="text-align: justify">El fichero "fragments/menu.html" contendra: </p>

```html
<nav th:fragment="menu-principal" class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
	<a class="navbar-brand" href="#">EmpleosApp</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarsExampleDefault">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
              <a class="nav-link active" href="#">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Menu 1</a>
            </li>
            <li class="nav-item">
				<a class="nav-link" href="#">Menu 2</a>
			</li>          
		</ul>  
		<a class="btn btn-primary" href="#">Ingresar</a>&nbsp;
		<a class="btn btn-primary" href="#">Registrarse</a>
	</div>
</nav>
```

<p style="text-align: justify">Insertando en el resto de ficheros html <i>Home, Detalle, ...</i> el menú de la cabecera principal de la siguiente manera:</p>

```html
  ...
  <body>
  	<header th:insert="fragments/menu :: menu-principal">    </header>
  	...
  </body>
```

<p style="text-align: justify">Por otro lado, el pie de página "fragments/footer.html" dentrá el siguiente código</p>

```html
<div th:fragment="pie-pagina" class="container">
	<p>
		&copy; 2019 EmpleosApp, Inc. | WebApp Desarrollada con Spring Boot
		2.1.2 | Autor: Iv&aacute;n E. Tinajero D&iacute;az | &middot; <a
			href="#">Privacy</a> &middot; <a href="#">Terms</a>
	</p>
</div>
```

<p style="text-align: justify">El cual se inserta de la misma manquera que el menú principal, es decir, meidnate un <code>th:insert="path :: nombre"</code></p>

```html
  ...
  <body>
	...
  	<footer th:insert="fragments/footer :: pie-pagina" class="footer"> </footer> 	
  </body>
```

## Capítulo 11. Formularios HTML (Data Binding).

## 11.2. Formulario HTML primeros pasos: 01-SpringBoot-DataBinding-v1.

<p style="text-align: justify">Vamos a insertar a nuestro servicio "VacantesController" la opción de crear una nueva oferta a través de un formulario</p>

```java
	@GetMapping("/create")
	public String crear(Vacante vacante, Model model) {
		model.addAttribute("categorias", serviceCategorias.buscarTodas() );
		model.addAttribute("vacante", serviceVacantes.buscarPorId(1) );
		return "vacantes/FormVacantes";
	}
```

![](..\img\module_1\11FormularioHTML\view-form.png)

<p style="text-align: justify">El código principal alojado dentro de la etiqueta "main" de fichero "FormVacantes.html"</p>

```html
 <hr>
      <div class="container"> 

        <div class="card">
          <h4 class="card-header"><strong>Datos de la oferta de trabajo</strong></h4>              
          <div class="card-body">
            <form th:action="@{/vacantes/save}" method="post" th:object="${vacante}">  
             
              <div th:if="${#fields.hasErrors('*')}"class='alert alert-danger' role='alert'>
				Por favor corrija los siguientes errores:
				<ul>
				<li th:each="err : ${#fields.errors('*')}" th:text="${err}"/>
				</ul>
			  </div>	
                       
              <div class="row">
                <div class="col-md-3"> 
                  <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" th:field="*{nombre}" id="nombre" name="nombre" placeholder="Titulo de la oferta de trabajo" required="required">
                  </div>
                </div>
               ...
      </div>
```

> :warning:  **Notas**
>
> * Con <code>th:action="@{/vacantes/save}"</code>indicamos el destino
> * Con <code>th:object="${vacante}"</code> enviamos un objeto recibido del controlado en el formulario
>
> > Para insertar la fecha de publicación **Calendario** mediante un calednario, se puede usar el método <u>"Datapicker" de jQuery UI</u>, por lo que se de deberá insertar el framework junto a jquery.
> >
> > ```html
> > <!-- Enlazando el CSS de Bootstrap -->
> > <link th:href="@{/bootstrap-5.0.0/css/bootstrap.css}" rel="stylesheet" />
> > <!-- CS de JQEURY UI -->
> > <link rel="stylesheet" type="text/css"
> > 	th:href="@{/jquery-ui-1.12.1/jquery-ui.css}">
> > 
> >     ....
> >     
> > <div class="col-md-3"> 
> > 	<div class="form-group">
> >        <label for="fecha">Fecha de Publicación <\ label> //cambiar \ por /
> > 	  <input type="text" class="form-control" th:field="*{fecha}" name="fecha" id="fecha" placeholder="Fecha de publicacion" required="required">
> > 	<\div>
> > <\div>
> > 
> > 	....
> > <script>
> > 	$(function () {
> >           $("#fecha").datepicker({dateFormat: 'dd-mm-yy'});
> >         }
> > 	);
> > <\script> //change \ for /
> >     
> >     ....
> > 	<script th:src="@{/jquery/3.6.0/jquery-3.6.0.min.js}"></script>
> > 	<script th:src="@{/bootstrap-5.0.0/js/bootstrap.bundle.js}"></script>
> > 
> > 	<!--  JQuery UI -->	
> > 	<script th:src="@{/jquery-ui-1.12.1/jquery-ui.js}"></script>
> > 	
> > 	<!-- Editor de texto de OpenSource -->
> > 	<script th:src="@{/tinymce/tinymce.min.js}"></script>
> > 
> > ```

<p style="text-align: justify">Para recoger los parámetros del controlador utilizamos la etiqueta "RequestParam(value-id-label-input-HTML) Tipo_Dato nombre_var", donde el valor de "value-id-label-input-HTML" debe coincidir con el valor del "id" del input que quiere leer, mientras que el valor de "nombre_var" no tiene la obligación de coincidir</p>

```java
@PostMapping("/save")
	public String guardar(@RequestParam("nombre") String nombre, //test
			@RequestParam("descripcion") String descripcion,  //test
			@RequestParam("estatus") String estatus, //
			@RequestParam("fecha") String fecha, //test
			@RequestParam(value = "destacado",defaultValue = "0" ) int destacado, ///radio-button -> value 1,0
			@RequestParam("salario") double salario, //number
			@RequestParam("detalles") String detalles) {
		System.out.println("Nombre Vacante: " + nombre);
		System.out.println("Descripcion: " + descripcion);
		System.out.println("Estatus: " + estatus);
		System.out.println("Fecha Publicación: " + fecha);
		System.out.println("Destacado: " + destacado);
		System.out.println("Salario Ofrecido: " + salario);
		System.out.println("detalles: " + detalles);
		return "vacantes/listVacantes"; 
	}
```

> :notebook:**RequestParam** también es usado para recoger parámetros que están en la url de la manera siguiente <code>url?param1=valu1&param2=value2&....</code>
>
> ```
> http://localhost:9090/vacantes/delete&id=1
> ```

## 11.1. Formulario HTML - DataBinding: 01-SpringBoot-DataBinding-v2

<p style="text-align: justify">Para recoger los parámetros del controlador utilizamos la etiqueta "RequestParam(value-id-label-input-HTML) Tipo_Dato nombre_var", donde el valor de "value-id-label-input-HTML" debe coincidir con el valor del "id" del input que quiere leer, mientras que el valor de "nombre_var" no tiene la obligación de coincidir</p>



## Capítulo 12. Su ir archivos.

<p style="text-align: justify">Para recoger los parámetros del controlador utilizamos la etiqueta "RequestParam(value-id-label-input-HTML) Tipo_Dato nombre_var", donde el valor de "value-id-label-input-HTML" debe coincidir con el valor del "id" del input que quiere leer, mientras que el valor de "nombre_var" no tiene la obligación de coincidir</p>



# Anexos 

s

### 1.1.a. Ejemplo de Spring Boot 2 + Thymeleaf: 03-ServerHelloMundo-v1.

#### 1.1.a.i. Dependencias & Propiedades.

##### 1.1.a.i.2. Dependencias.

##### 1.1.a.i.2. Propiedades.

#### 1.1.a.ii. Clase principal "".

#### 1.1.a.iii. Controlador "HomeController.java".

#### 1.1.a.iv. Vista "home.html".

#### 1.1.a.v. Llamada al handler "/".

# Anexos

## Anexo I. Cómo cerrar un puerto en Windows matando el proceso del que depende

<div style="text-align: justify">
  <p>
      En este anexo, vamos a ver como cerrar un puerto en Windows matando el proceso del que depende.<br>
	Algunas veces nos encontramos con la necesidad de  matar un proceso en Windows para cerrar el puerto que ese proceso ha  dejado abierto. Las razones para eso pueden ser varias: desde una  aplicación que se ha quedado colgada, dejando el puerto bloqueado, hasta la irrupción de virus en nuestro sistema. Aquí va una mini-receta para  solucionar este problema.<br>
	En primer lugar, se ha de lanzar desde una consola el comando  “netstat -aon”. Este comando nos permitirá ver qué puertos tiene  abiertos nuestra máquina, y el PID del proceso del que dependen. Una  captura de pantalla del comando:<br>
  </p>
  <img src="..\img\anexos\anexo_1\img1.png" style="zoom:100%;" />
  <p>
   Una vez hayamos identificado el puerto, así como su PID correspondiente, será tan sencillo como ejecutar el siguiente comando:
  </p>
</div>

```shell
$ taskkill /F /PID nro_pid
```

>  :notebook: Nota: La opción "_nro_pid_", corresponde con el PID identificado con anterioridad.

### Ejemplarizando - Matar un proceso en el puerto '8080'

<div style="text-align: justify">
    <p>
        Por ejemplo, cuando intamos lanzar nuestra aplicaicón Spring, nos da un error, poruqe nos dice que el puerto 8080 esta siendo ocupado por otro proceso.</p></div>

```shell
***************************
APPLICATION FAILED TO START
***************************

Description:

Web server failed to start. Port 8080 was already in use.

Action:

Identify and stop the process that's listening on port 8080 or configure this application to listen on another port.


Process finished with exit code 0

```

<div style="text-align: justify">
    <p>
        Por lo tanto, para poder lanzarla, buscamso el PID del proceso y lo "matamos", de esta manera ya podríamos lanzar nuevamente nuestra aplicaicón boot.</p></div>

```shell
# Identificamos el PID del proceso 8080
$ netstat -aon

Conexiones activas

  Proto  Dirección local          Dirección remota        Estado           PID
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       1184
  TCP    0.0.0.0:443            0.0.0.0:0              LISTENING       7240
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:902            0.0.0.0:0              LISTENING       5132
  TCP    0.0.0.0:912            0.0.0.0:0              LISTENING       5132
  TCP    0.0.0.0:1309           0.0.0.0:0              LISTENING       4808
  TCP    0.0.0.0:1433           0.0.0.0:0              LISTENING       5536
  TCP    0.0.0.0:2343           0.0.0.0:0              LISTENING       5444
  TCP    0.0.0.0:3306           0.0.0.0:0              LISTENING       6080
  TCP    0.0.0.0:3580           0.0.0.0:0              LISTENING       4984
  TCP    0.0.0.0:5040           0.0.0.0:0              LISTENING       5692
  TCP    0.0.0.0:7680           0.0.0.0:0              LISTENING       15808
  TCP    0.0.0.0:8080           0.0.0.0:0              LISTENING       9344
  TCP    0.0.0.0:33060          0.0.0.0:0              LISTENING       6080
  TCP    0.0.0.0:35729          0.0.0.0:0              LISTENING       9344

#Matamos el proceso
$ taskkill /F /PID 9344
Correcto: se terminó el proceso con PID 9344.

# Lanzamos nuevamente nuestro proyecto Spring
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v2.4.2)

2021-02-06 10:48:52.219  INFO 1520 --- [  restartedMain] n.andres.holamundo.HolamundoApplication  : Starting HolamundoApplication using Java 1.8.0_271 on DESKTOP-I54C6AK with PID 1520 (C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\module_1\sources\03-ServerHelloMundo-v1\target\classes started by andre in C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\module_1\sources\03-ServerHelloMundo-v1)
2021-02-06 10:48:52.222  INFO 1520 --- [  restartedMain] n.andres.holamundo.HolamundoApplication  : No active profile set, falling back to default profiles: default
2021-02-06 10:48:52.300  INFO 1520 --- [  restartedMain] .e.DevToolsPropertyDefaultsPostProcessor : Devtools property defaults active! Set 'spring.devtools.add-properties' to 'false' to disable
2021-02-06 10:48:52.300  INFO 1520 --- [  restartedMain] .e.DevToolsPropertyDefaultsPostProcessor : For additional web related logging consider setting the 'logging.level.web' property to 'DEBUG'
2021-02-06 10:48:53.718  INFO 1520 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2021-02-06 10:48:53.743  INFO 1520 --- [  restartedMain] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2021-02-06 10:48:53.744  INFO 1520 --- [  restartedMain] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.41]
2021-02-06 10:48:53.901  INFO 1520 --- [  restartedMain] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2021-02-06 10:48:53.904  INFO 1520 --- [  restartedMain] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1601 ms
2021-02-06 10:48:54.192  INFO 1520 --- [  restartedMain] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2021-02-06 10:48:54.454  INFO 1520 --- [  restartedMain] o.s.b.d.a.OptionalLiveReloadServer       : LiveReload server is running on port 35729
2021-02-06 10:48:54.496  INFO 1520 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2021-02-06 10:48:54.510  INFO 1520 --- [  restartedMain] n.andres.holamundo.HolamundoApplication  : Started HolamundoApplication in 2.692 seconds (JVM running for 3.838)

```



## Anexo II. Habilitar Spring-boot-devtools en IntelliJ

<div style="text-align: justify">
    <p>
        <b>Spring Boot DevTools</b> es una heramienta de Spring Boot cuyo objetivo principal es la de facilitar la tarea de mostrar los cambios, por defecto tenemos que parar e iniciar nuestra aplicación cada vez que realicemos un cambios, sin embargo, al integrar esta dependnecia en nuestro proyecto, cada vez que guardaemos un cambio y este es detectado por el framework, se encarga de reinicarla automáticamente.<br>
    Por defecto, el <b>IDE IntelliJ</b> no tiene trabaja con Spring Boot DevTools, para solucionar esto, debemos realizar los siguientes pasos:<ol>
    <li>Habilitar "<i>Build project automatically</i>": Es una configuración hay específica del proyecto que se deberá aplicar en cualquier proyecto en el que quieras usar devtools.</li>
    <li>Habilitar el registro "<i>compiler.automake.allow.when.app.running</i>": Es una configuración de registro de IDEA que se aplica a todos los proyectos. </li>
    </ol>
    </p></div>

<div style="text-align: justify">
    <p>
        Para habilitar "<i>Build project automatically</i>", realizamos el siguiente proceimiento:
    <ol>
    <li>Vamos a File>Setting.<br>
        <img src="..\img\anexos\anexo_2\img1.png" style="zoom:100%;" />
    </li>
    <li>En la ventna nueva, nos vamos "<i>Build, Execution, Deployment</i>>Compiler" y activamos la opción que pone "<b>Build project automatically</b>"<br>
        <img src="..\img\anexos\anexo_2\img2.png" style="zoom:100%;" />
    </li>
    </ol>
    Para activar el registro "<i>compiler.automake.allow.when.app.running</i>", realizmaos el siguiente procedimiento:
    <ol>
        <li>Presiona <code>Shift+Command+A</code> en macOS (OSX), o <code>Shift+Ctrl+A</code> en Windows), buscadno la configuraicón "Registry..."<br>
        <img src="..\img\anexos\anexo_2\img3.png" style="zoom:100%;" /> </li>
        <li>Buscamos y habilitamos la opción "<i>compiler.automake.allow.when.app.running</i>"<br>
        <img src="..\img\anexos\anexo_2\img4.png" style="zoom:100%;" /></li>
    </ol>
    Si no surge ningún contratiempo, ya se puuedes lanzar la aplicaicón y la recarga automática de archivos estáticos y de intercambio en caliente debería estar habilitada.
    </p></div>

## Anexo III.  FileTemplate HTML+Thymeleaf en IntelliJ & Eclipse.

<div style="text-align: justify">
    <p>
       En este anexo vamos a enseñar como crear un esquelo HTML+Thymeleaf en IntelliJ y Eclipse, para que cuando cremos un nuevo fichero, este añada automáticamente la dependencia de Thymeleaf y los fragmentos de código que insertamos de manera reiterada cada vez que creamos un nuevo documento html.<br>El esquelo o plantilla tendrá el siguiente aspecto:
    </p></div> 

```html
<!DOCTYPE html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
<head>
  <meta charset="UTF-8">
  <title>#[[$Title$]]#</title>
</head>
<body>
#[[$END$]]#
</body>
</html>
```

### Anexo III.A. FileTemplate en Eclipse.

<div style="text-align: justify">
    <p>
       Basicamente, consta de realizar los siguientes pasos:
       <ol>
       	<li>Vamos a "<i>Windows</i>">"<i>Preferencias</i>"</li> 
        <div align="center">
           <img src="..\img\anexos\anexo_3\anexo_3_b\img1.png" style="zoom:100%;" />
        </div>
        <li>Buscamos la opción "<i>Templates</i>" del Editor de HTML FIles Web, es decir, nos dirimos a <i>Web>HTML FIles>Editor>Templates.</i></li>
        <div align="center">
           	<img src="..\img\anexos\anexo_3\anexo_3_b\img2.png" style="zoom:100%;" />
        </div>
        <li>Le damos al botón "New" y rellenamos los campos de la venata que nos aperece (es muy intutivo)</li>
        <div align="center">
        	<img src="..\img\anexos\anexo_3\anexo_3_b\img3.png" style="zoom:80%;" />
        </div>
       </ol>
    </p>
</div> 

> :notebook: **Nota**: Es importante que tenga el contexto "New HTML" y si queremos añadir una varibale, podemos darle al botón "Intert Variable...".

### Anexo III.B. FileTemplate en IntelliJ.

> :alarm_clock: **Configure**: Ctrl+Alt+S o **Settings/Preferences | Editor | File and Code Templates**

<div style="text-align: justify">
    <p>
       Basicamente, consta de ir File>Setting y buscmaos "<i>file template</i>", le damos a al botón "+", y lo demás es muy intuitivo.
    </p></div> 

![](..\img\anexos\anexo_3\anexo_3_a\img1.png)

> :notebook: **Nota**: Para interpretar los campos `#[[$Title$]]` y `#[[$END$]]`, es necesario activar la opción "**Enable Live Templates**".

<div style="text-align: justify">
    <p>
       Una vez creado, le deamos botón derecho sobre el dirctorio o paquete donde vamos añadir el nuevo fichero y seleccioanmos nuestra plantilla.
    </p></div> 

![](..\img\anexos\anexo_3\anexo_3_a\img2.png)

## Anexo IV. Flujo de una petición HTTP en Spring MVC.

<div style="text-align: justify">
    <p>
       Es importante describir el flujo de procesamiento típico para una petición HTTP en Spring MVC (<i>tambien es válido para Spring Boot</i>).<br> Para no hacer una descripción la vamos a simplicar y no vamos a incluir ciertos elmentos, aunque ni decir tiene, se veran a lo largo del curso.<br>
       Sabemos que Spring es una implementación del patrón de diseño "front controller", que también implementan otros frameworks MVC, como por ejemplo, el clásico Struts, Symfony, etc.<br>
    </p>
	<div align="center">
           <img src="..\img\anexos\anexo_4\img1.jpg" style="zoom:100%;" />
        </div>
    <p>
        En la imagen superior, podemos ver:
        <ul>
            <li>Todas las peticiones HTTP se canalizan a través del front controller, y es que en casi todos los frameworks MVC que siguen este patrón, el front controller no es más que un servlet cuya implementación es propia del framework, así, y dado el caso de Spring, el front controller por defecto es la clase DispatcherServlet.</li>
            <li>El front controller es el encargado de averigaur, normalmente a partir de la URL, a qué Controller hay que llamar para servir la petición, apoyandonse para tal fin, el uso de un HandlerMapping.</li>
            <li>El Controller, es el que ejecuta la lógica de negocio, ya que obtiene los resultados y los devuelve al servlet, encapsulados en un objeto del tipo Model, junto a otros datos, como el nombre lógico de la vista a mostrar (<i>normalmente devolviendo un String, como en JSF</i>).</li>
            <li>Mediante el uso de un ViewResolver, se encargará de averiguar el nombre físico de la vista que se corresponde con el nombre lógico del paso anterior.</li>
            <li>Por último, el front controller (el DispatcherServlet) redirige la petición hacia la vista, que muestra los resultados de la operación realizada.</li>
    </ul>
    </p>
</div> 

### Anexo IV.A. Enviar un parámetro a la vista.

<div style="text-align: justify">
    <p>
        Vemoas un ejemplo de como leer una parametro en una enviado de un controaldor meidante los objetos: <b>Model</b>, <b>ModelMap</b>, <b>ModelAndView</b> a vista con Thymeleaf integrado.
    </p>
</div> 

#### Anexo IV.A.i. La vista.

<div style="text-align: justify">
    <p>
        En thymeleaf el palametro es recibido meidante la opicon "<i>th:text="${name_param}"</i>" dentro de una etiqueta, o "<i>[[${name_para,}]]</i>"
    </p>
</div> 

```html
<!DOCTYPE HTML>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <title>Title</title>
</head>
<body>
	<div th:text="${message}">Web Application. Passed parameter : </div>
	<div th:text="${message}">Web Application. Passed parameter : [[${message}]] </div>
    <div>Web Application. Passed parameter :  [[${message}]] </div>
</body>
</html>
```

#### Anexo IVA.ii. Objeto Model.

```java
@GetMapping("/showViewPage")
public String passParametersWithModel(Model model) {
    Map<String, String> map = new HashMap<>();
    map.put("spring", "mvc");
    model.addAttribute("message", "Baeldung");
    model.mergeAttributes(map);
    return "viewPage";
}
```

#### Anexo IVA.iii. Objeto ModelMap.

```java
@GetMapping("/printViewPage")
public String passParametersWithModelMap(ModelMap map) {
    map.addAttribute("welcomeMessage", "welcome");
    map.addAttribute("message", "Baeldung");
    return "viewPage";
}
```

##### Anexo IVA.iv. Objeto ModelAndView.

```java
@GetMapping("/goToViewPage")
public ModelAndView passParametersWithModelAndView() {
    ModelAndView modelAndView = new ModelAndView("viewPage");
    modelAndView.addObject("message", "Baeldung");
    return modelAndView;
}
```

## Anexo V. Estructuras Thymeleaf.

Para 

### Anexo V.A. Texto.

<div style="text-align: justify">
    <p>
        En thymeleaf el valor de un parámetro o atributo recibido del controlador se puede obtener mediante la opción "<i>th:text="${name_param}"</i>" de una etiqueta, o "<i>[[${name_para,}]]</i>" dentro del valor de una etiqueta html.<br>
        Por ejemplo:
</div> 

* En el controlador:

```java
package com.example.thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.sql.Timestamp;

@Controller
public class ThymelafTextController {

    /**
     * Handler, que envia varios atributos a la vista
     * Método HTTP: Get
     * Recurso: /ParamThymeleaf
     * @param model
     * @return String
     *
     * Example: http://localhost:8080/ParamThymeleaf
     */
    @GetMapping(value="/ParamThymeleaf",name = "sendParameterModel")
    public String sendParameterModel(Model model){
        model.addAttribute("mensaje","Esto es un mensaje desde el controlador");
        model.addAttribute("date",new Timestamp(System.currentTimeMillis()));
        model.addAttribute("email","andresruizpenuela@gmail.com");
        return "textThymeleaf";
    }
}
```

* En la vista:

```html
<!DOCTYPE HTML>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <title>Title</title>
</head>
<body>
	<div th:text="${message}">¡Bienvenido a nuestra tienda de comestibles!</div>
	<div th:text="${date}">Web Application. Passed parameter : [[${message}]] </div>
    <div>Web Application. Passed parameter :  [[${message}]] </div>
</body>
</html>
```

<div style="text-align: justify">
    <p>
       Por otro lado, con Thymeleaf podeos <b>externalizar texto</b>, este proceso, permite extraer fragmentos de código de los archivos de plantilla para que puedan guardarse en archivos separados (<i>normalmente archivos .properties</i>) y así oderlos remplazar fácilmente por textos equivalentes escritos en otros idiomas (<i>este proceso es conocido como internacionalización o simplemente i18n</i>).<br>
        <b>Los fragmentos de texto exteriorizados suelen denominarse "mensajes",</b> y siempre tienen una clave que los identifica, especificando que un texto debe corresponder a un mensaje específico con la sintaxis #{...}:<br>
</div> 

```html
<etiqueta th:text="'text_optional'+#{atributo}"></etiqueta>
<etiqueta data-th-text="'text_optional'+#{mensaje}"></etiqueta>
<etiqueta> [[#{mensaje}]] </etiqueta>
<!-- texto con parametro -->
<etiqueta th:text="'Text wich PARAM: '+#{atributo(${attr1},${attr2},...)}"></etiqueta>
<etiqueta th:text="'Text wich PARAM: '+#{atributo(param1,param2,...)}"></etiqueta>
<etiqueta> [[#{mensaje(${attr1},${attr2},...)]]</etiqueta>
<etiqueta> [[#{mensaje(param1,param2,...)]]</etiqueta>
```

> :notebook: **Nota**: El uso de externe

<div style="text-align: justify">
    <p>
        Lo que podemos ver aquí son, de hecho, dos características diferentes del dialecto estándar de Thymeleaf:<br>
        <ul>
            <li>Los atributos de texto th, que evalúan su expresión de valor y establecen el resultado como el cuerpo de la etiqueta de host, reemplazando efectivamente el "¡Bienvenido a nuestra tienda de comestibles!" texto que vemos en el código.</li>
            <li>La expresión #{home.welcome}, especificada en la sintaxis de expresiones estándar, indicando que el texto a ser utilizado por los atributos de texto debe ser el mensaje con la clave correspondiente a cualquier home.welcome local con el que estemos procesando la plantilla.</li>
    	</ul>
    	A continuaicón, mostraemos un ejemplo:
    </p>
</div>

```html
<!DOCTYPE html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Thyemeleaf</title>
</head>
<body>
    <h1>Thyemeleaf</h1>
    <div style="border-style: solid;border-color:black">
        <h2>Mostrar atributos del controlador en la vista</h2>
        <!-- atributos -->
        <h3>Get atributos: </h3>
        <ul>
            <li th:text="${mensaje}"></li>
            <li>[[${mensaje}]]</li>
        </ul>
        <h3>Get atributos vacíos o no pasados: </h3>
        <ul>
            <li th:text="${mensajeVoid}"></li>
            <li>[[${mensajeVoid}]]</li>
            <li th:text="${message}"></li>
            <li>[[${message}]]</li>
        </ul>
        <h3>Get atributos + texto: </h3>
        <ul>
            <li th:text="'Date: '+${date}">Este contenido no se muestra</li>
            <li th:text="'Date: '+${date}"></li>
            <!-- Si no existe el atributo o esta vacio -->
            <li>Date: [[${mensajeVoid}]]</li>
            <li th:text="${mensajeVoid}">Aunque este vacio no aparece th:text lo machaca</li>
            <li th:text="${message}">Aunque este exista no aparece th:text lo machaca</li>
            <li>Mesaje vacio: [[${mensajeVoid}]]</li>
            <li th:text="${message}"></li>
            <li>Mensaje de un attributo que no existe: [[${message}]]</li>
            <!-- Dentro de otras etiqeutas -->
            <li> Now you are looking at a <span th:text="'working web application'">template file</span>. </li>
            <li>
                Hola bienvenido [[${email}]], esto es un documento web que lee los parametros recibidos del controlador.<br>
                Este parámetro esta vacío o no se ha recibido, param1: [[${emails}]], param2: <span th:text="${emails}"></span>.
            </li>
        </ul>
    </div>
    <br>
    <h2>Extenderizando texto</h2>
    <!-- mensajes de messages.properties -->
    <div style="border-style: solid;border-color:black">
    <p th:text="#{messages.welcome}">Welcome to our grocery store!</p>
    <p data-th-text="#{messages.welcome}">Welcome to our grocery store!</p>
    <p th:*="#{messages.welcome}">Welcome to our grocery store!</p>
    </div>
    <br>
    <!-- mensjaes de messages/textMessages.properties -->
    <div style="border-style: solid;border-color:black">
        <p th:text="#{textMessages.salut}">Welcome to our grocery store!</p>
        <p data-th-text="#{textMessages.edad}">Welcome to our grocery store!</p>
        <p th:*="#{textMessages.salut}">Welcome to our grocery store!</p>
        <p th:text="#{textMessages.salut}">Welcome to our grocery store! - [[#{textMessages.salut}]]</p>
        <!-- texto con parametro -->
        <p th:text="'Text wich PARAM: '+#{welcome.messageWitchParam(${email})}"></p>
        <p>
            Text wich PARAM - [[#{welcome.messageWitchParam(${email})}]]
        </p>
    </div>
    <!-- Errores
    <p th:text="${messages.welcome}">Welcome to our grocery store!</p>
    <p th:text="#{email}">Welcome to our grocery store!</p>
    -->
</body>
</html>
```

<div style="text-align: justify">
    <p>
        Siendo el efecto visual que tendríamos en el navegor:
    </p>
</div>

![](C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\anexo_5\img_1.png)

### Anexo V.B. If-Else.

<div style="text-align: justify">
    <p>
        Una de las entajas de usar Thymeleaf, es el posiblidad de añadir estrucutras de control, en docuentos web.<br> Y como es de costubmre, la priemra estructura de control que veremos, será la fomosa "if-else", con la cual, a través de una condición podemos tomar un camino u otro, siendo las condiicones soportardas:
        <ul>
			<li>gt (>), lt (<) </li>
			<li>ge (>=), le (<=) </li>
			<li>not (!) </li>
			<li>eq (==), neq/ne (!=) </li>
         </ul>
    Por otro lado, tenemos des operadones:
    <ul>
			<li>Operador Elvis (?:)</li>
         </ul>
    </p>
</div>

```html
<etiqueta> th:text="${condicion} ? sentencias-true : sentences-false" </etiqueta> 
```

<div style="text-align: justify">
    <p>
    <ul>
			<li>Operador por defecto de thymeleaf th:if / th:unless</li>
         </ul>
    </p>
</div>

```html
<etiqueta th:if="${status}">La variable "status" es true.</etiqueta>
<etiqueta th:unless="${status}">La variable "status" es false.</etiqueta>
```

<div style="text-align: justify">
    <p>
        Para usarlo, solo vastiaría con añadir un al documento web el uso de thymeleaf y la dependncia de la misma en nuestro proyecto, sin necesidad de agregar nada opcional al controlador, es decir, un ejemplo sencillo sería:
    </p>
</div>

* En el controlador.

```java
package com.example.thymeleaf.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.LinkedList;
import java.util.List;

@Controller
public class ThymleafIfController {
    Logger logger =  LoggerFactory.getLogger(ThymleafIfController.class);
    /**
     * Handler, que envia una lista de String + otros atributos al vista
     * Método HTTP: Get
     * Recurso: /IfThymeleaf
     * @param model
     * @return String ifThymeleaf-v1
     *
     * Example: http://localhost:8080/IfThymeleaf
     */
    @GetMapping(value="/IfThymeleaf",name = "ifThymeleafModel")
    public String exampleIfThymeleaf(Model model){
        logger.info("[exampleIfThymeleaf] Procesando peticion /IfThymeleaf");

        /* Lista de ofertas */
        List<String> list = new LinkedList<String>();
        list.add("Ingeniero de Sistemas");
        list.add("Auxiliar de contabilidad");
        list.add("Programador Junior");
        list.add("Comercial");

        /* Add Atributos a la vista */
        model.addAttribute("email","andresruizpenuela@gmail.com");
        model.addAttribute("status",true);
        model.addAttribute("ofertas",list);

        /* Finalizamos */
        return "ifThymeleaf/ifThymeleaf-v1";
    }
}
```

* En la vista.

```html
<!DOCTYPE html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8">
        <title>Thymeleaf</title>
        <style>
            article{
                border-style: groove;
                margin: 20px;
            }
        </style>
    </head>
    <body>
        <h1>Thyemeleaf-v1</h1>
        <h2>-.Ejemplo sencilla de if en Thyemelaf.-</h2>
        <hr>
        <div th:with="condition=${potentially_complex_expression}" th:remove="tag">
            <h2 th:if="${status}">Hello! ${email} </h2>
            <span th:unless="${status}" >No login</span>
        </div>
        <hr>
        <div>
			<h4 style="margin: 0px;padding: 0px;">Ejemplo de un uso: </h4>
			<hr>
			<p th:text="'¿Lista vacia? '+(${ofertas.isEmpty()} ? true)"/>  </p>
			<p th:text="'¿Lista no vacia? '+(not ${ofertas.isEmpty()} ? true)"/>  </p>
			<p th:text="${ofertas.size()>2} ? 'Mas de dos elementos'" /> </p>
        </div>
    </body>
</html>
```

<div style="text-align: justify">
    <p>
        Un ejemplo del uso de este operador más detallamadamente, sería el siguiente:
    </p>
</div>

```html
<!DOCTYPE html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8">
        <title>Thymeleaf</title>
        <style>
            article{
                border-style: groove;
                margin: 20px;
            }
        </style>
    </head>
    <body>
        <h1>Thyemeleaf-v1</h1>
        <h2>-.Ejemplo sencilla de if en Thyemelaf.-</h2>
        <hr>
        <!-- Operador Elvis -->
        <section style="border-style: double;">
            <h3 style="padding: 0px;margin: 0px;"> Operador Elvis </h3>
            <!-- Operador Elvis "if -->
            <article>
                <h4>Operador Elvis "if"</h4>
                <mark>Sintaxis</mark> <br>
                <!-- <code>[[${condicion} ? sentences-true]]</code><br> -->
                <code> th:text="${condicion} ? sentencias-true" </code> <br>
                <code>&#91;&#91;&#36;&#123;condicion&#125; &#63; sentencias-true&#93;&#93;"> </code><br>
                <div style="border-style: solid;}; margin: 5px;">
                    <h4 style="margin: 0px;padding: 0px;">Ejemplo de un uso: </h4>
                    <hr>
                    <p th:text="'¿Lista vacia? '+(${ofertas.isEmpty()} ? true)"/>  </p>
                    <p th:text="'¿Lista no vacia? '+(not ${ofertas.isEmpty()} ? true)"/>  </p>
                    <p th:text="${ofertas.size()>2} ? 'Mas de dos elementos'" /> </p>
                </div>
            </article>
            <!-- Operador Elvis "else" -->
            <article>
                <h4>Operador Elvis "else"</h4>
                <mark>Sintaxis</mark> <br>
                <code> th:text="${condicion} ?: sentences-false" </code> <br>
                <!-- <code>[[${condicion} ?: sentences-false]]</code><br> -->
                <code>&#91;&#91;&#36;&#123;condicion&#125; &#63;&#58; sentencias-false&#93;&#93;"> </code><br>
                <div style="border-style: solid;}; margin: 5px;">
                    <h4 style="margin: 0px;padding: 0px;">Ejemplo de un uso: </h4>
                    <hr>
                    <p th:text="'¿Lista vacia? '+(${ofertas.isEmpty()} ?: false)"/>  </p>
                    <p th:text="'¿Lista no vacia? '+(not ${ofertas.isEmpty()} ?: true)"/>  </p>
                    <p th:text="${ofertas.size()>2} ?: 'Menos de dos elementos'" /> </p>
                </div>
            </article>
            <!-- Operador Elvis "if-else" -->
            <article>
                <h4>Operador Elvis "if-else"</h4>
                <mark>Sintaxis</mark> <br>
                <code> th:text="${condicion} ? sentencias-true : sentences-false" </code> <br>
                <!-- <code>[[${condicion} ? sentencias-true : sentences-false]]</code><br> -->
                <code>&#91;&#91;&#36;&#123;condicion&#125; &#63; sentencias-true &#58; sentencias-false&#93;&#93;"> </code><br>
                <div style="border-style: solid;}; margin: 5px;">
                    <h4 style="margin: 0px;padding: 0px;">Ejemplo de un uso: </h4>
                    <hr>
                    <p>¿Lista vacia? [[${ofertas.isEmpty()} ? true : false]]</p>
                    <p th:text="${ofertas.size()>2} ?'Mas de dos elementos': 'Menos de dos elementos'" /> </p>
                </div>
            </article>
        </section>
        <hr>
        <!-- Operador th:if & th:unless -->
        <section style="border-style: double;">
            <h3 style="padding: 0px;margin: 0px;"> Operador th:if & th:unless </h3>
            <p>
                Condiciones soportadas:
            <ul>
                <li>gt (>), lt (<)/li>
                <li>ge (>=), le (<=)</li>
                <li>not (!)</li>
                <li>eq (==), neq/ne (!=)</li>
            </ul>
            </p>
            <!-- Operador "th:if" -->
            <article>
                <h4>Operador "th:if"</h4>
                <mark>Sintaxis</mark> <br>
                <code> th:if="${condition}" </code> <br>
                <div style="border-style: solid;}; margin: 5px;">
                    <h4 style="margin: 0px;padding: 0px;">Ejemplo de un uso: </h4>
                    <hr>
                    <span th:if="${ofertas.size() gt 2}">Lista con mas de dos elementos.</span>
                    <span th:if="${ofertas.size() < ofertas.size()}">Lista con mas de dos elementos.</span>
                    <br>
                    <p>¿Lista vacia? <span th:if="${ofertas.isEmpty() }">Lista vacia.</span></p>
                    <p>¿Lista vacia? <span th:if="not ${ofertas.isEmpty() }">Lista no vacia.</span></p>
                </div>
            </article>
            <!-- Operador "th:unless" -->
            <article>
                <h4>Operador "th:unless"</h4>
                <mark>Sintaxis</mark> <br>
                <code> th:unless="${condition}" </code> <br>
                <div style="border-style: solid;}; margin: 5px;">
                    <h4 style="margin: 0px;padding: 0px;">Ejemplo de un uso: </h4>
                    <hr>
                    <span th:unless="${ofertas.size() gt 2}">Lista con menos de dos elementos.</span>
                    <span th:unless="${ofertas.size() > 2}">Lista con mas de dos elementos.</span>
                    <br>
                    <p>¿Lista vacia? <span th:unless="${ofertas.isEmpty() }">Lista no vacia.</span></p>
                    <p>¿Lista vacia? <span th:unless="not ${ofertas.isEmpty() }">Lista vacia.</span></p>
                </div>
            </article>
            <!-- Operador "th:if & th:unless" -->
            <article>
                <h4>Operador "th:if & th:unless"</h4>
                <mark>Sintaxis</mark> <br>
                <code> th:if="${condition}" sentence </code><br>
                <code> th:unless="${condition}" sentence</code><br>
                <div style="border-style: solid;}; margin: 5px;">
                    <h4 style="margin: 0px;padding: 0px;">Ejemplo de un uso: </h4>
                    <hr>
                    <hr>
                    <span th:if="${status}">La variable "status" es true.</span>
                    <span th:unless="${status}">La variable "status" es false.</span>
                    <br>
                    <p>¿Eres andresruizpenuela@gmail.com"?
                        <span th:if="${email}=='andresruizpenuela@gmail.com'"> Correcto usted es [[${email}]]</span>
                        <span th:unless="${email}=='andresruizpenuela@gmail.com'"> Lo siento, usted es [[${email}]]</span>
                    </p>
                </div>
            </article>
        </section>
    </body>
</html>
```

<div style="text-align: justify">
    <p>
        Siendo el reusldado visutla de este código, el siguinte:
    </p>
</div>

![](C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\anexo_5\img_2.png)

### Anexo V.C. Switch-Case.

<div style="text-align: justify">
    <p>
       Otra estrucutra de control, y como alternativa a "if-else", es la ya conocida "switch-case", la cual, se utiliza midiante la opicón <b>th:swith/th:case</b>, y al igual que en la estructura de control if-else, no hace falta añadir ninguna acción extra en el controlador.
    </p>
</div>

```html
<etiqueta th:switch="atributo">
  <etiqueta th:case="condicion">Caso A</etiqueta>
  <etiqueta th:case="condicion">Caso B</etiqueta>
  ...
  <etiqueta th:case="condicion">Caso N</etiqueta>
  <etiqueta th:case="*">Caso por defectog</etiqueta>
</etiqueta>
```

<div style="text-align: justify">
    <p>
       Además, al igual que en la estructura de control if-else, no hace falta añadir ninguna acción extra en el controlador, para ello veamos un ejemplo sencillo.
    </p>
</div>

* En el controaldor.

```java
package com.example.thymeleaf.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.LinkedList;
import java.util.List;

@Controller
public class ThymeleafSwitchCaseController {
    Logger logger = LoggerFactory.getLogger(ThymeleafSwitchCaseController.class);
    /**
     * Handler, que envia una lista de String + otros atributos al vista
     * Método HTTP: Get
     * Recurso: /SwitchCaseThymeleaf
     * @param model
     * @return String switchCaseThymeleaf-v1"
     *
     * Example: http://localhost:8080/SwitchCaseThymeleaf
     */
    @GetMapping(value="/SwitchCaseThymeleaf",name = "SwitchCaseThymeleaf")
    public String exampleSwitchCaseThymeleaf(Model model){
        logger.info("[exampleSwitchCaseThymeleaf] Procesando peticion /SwitchCaseThymeleaf");

        /* Lista de ofertas */
        List<String> list = new LinkedList<String>();
        list.add("Ingeniero de Sistemas");
        list.add("Auxiliar de contabilidad");
        list.add("Programador Junior");
        list.add("Comercial");

        /* Add Atributos a la vista */
        model.addAttribute("ofertas",list);

        /* Finalizamos */
        //templates/switchCaseThymeleaf/switchCaseThymeleaf-v1.html
        return "switchCaseThymeleaf/switchCaseThymeleaf-v1";

    }
}
```

* En la vista.

```html
<!DOCTYPE html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Thymeleaf</title>
    <style>
        article{
            border-style: groove;
            margin: 20px;
        }
    </style>
</head>
<body>
    <h1>Thyemeleaf-v1</h1>
    <h2>-.Ejemplo sencillo de switch-case en Thyemelaf.-</h2>
    <!-- Operador Elvis -->
    <section style="border-style: double;">
        <h3 style="padding: 0px;margin: 0px;"> Operador Elvis </h3>
        <!-- Operador "th:switch" & "th:case" -->
        <article>
            <h4>Operador "switch-case"</h4>
            <mark>Sintaxis</mark> <br>
            <!-- <code>[[${condicion} ? sentences-true]]</code><br> -->
            <code>
                th:switch="${option}"<br>
                th:case="condition_A_1,condition_A_2,...,condition_A_N"<br>
                sentences_condition_A<br>
                th:case="condition_B"<br>
                sentences_condition_B<br>
                ...<br>
                th:case="*"<br>
                sentences_condition_defaul<br>
            </code> <br>
            <div style="border-style: solid;}; margin: 5px;">
                <h4 style="margin: 0px;padding: 0px;">Ejemplo de un uso: </h4>
                <hr>
                <div th:switch="${ofertas.size()}">
                    <div th:case="0">
                        [[${ofertas[0]}]]
                    </div>
                    <div th:case="1">
                        [[${ofertas[1]}]]
                    </div>
                    <div th:case="*">
                        Contenido:<br>
                        <span th:each="oferta:${ofertas}"> [[${oferta}]]<br></span>
                    </div>
                </div>
            </div>
        </article>
    </section>
</body>
</html>
```

<div style="text-align: justify">
    <p>
       El resultado visual seria:
    </p>
</div>

![](C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\anexo_5\img_3.png)

### Anexo V.D. EACH.

<div style="text-align: justify">
    <p>
       En Thymeleaf las iteracioens se pueden realizar con las expresión <b>th:each</b>, que emula a la estrucutra de control "for" de Java, con ella odemos iterar sobre los diferntes tipos de datos, tales como:
       <ul>
           <li>Listas - List</li> 
           <li>Mapas - Map</li>
           <li>Iterables - Iterable</li>
       </ul>
    La sintaxis sería la siguiente:
    </p>
</div>

```html
<!-- Opción A-->
<etiqueta th:each="variable:atributo" th:text="variable"></etiqueta>

<!-- Opción B-->
<etiqueta th:each="variable:atributo">
	<etiqueta th:text="variable" />
</etiqueta>
```

<div style="text-align: justify">
    <p>
       Además, tampoco hace falta añadir ninguna funcionalidad extra en el controlador, puediendo ver un ejemplo sencillo:
    </p>
</div>

* En el controlador.

```java
package com.example.thymeleaf.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.LinkedList;
import java.util.List;

@Controller
public class ThymeleafEachController {

    Logger logger = LoggerFactory.getLogger(ThymeleafEachController.class);
    /**
     * Handler, que envia una lista de String + otros atributos al vista
     * Método HTTP: Get
     * Recurso: /SwitchCaseThymeleaf
     * @param model
     * @return String switchCaseThymeleaf-v1"
     *
     * Example: http://localhost:8080/EachThymeleaf
     */
    @GetMapping(value="/EachThymeleaf",name = "SwitchCaseThymeleaf")
    public String exampleEachThymeleaf(Model model){
        logger.info("[exampleEachThymeleaf] Procesando peticion /EachThymeleaf");

        /* Lista de ofertas */
        List<String> list = new LinkedList<String>();
        list.add("Ingeniero de Sistemas");
        list.add("Auxiliar de contabilidad");
        list.add("Programador Junior");
        list.add("Comercial");

        /* Add Atributos a la vista */
        model.addAttribute("ofertas",list);

        /* Finalizamos */
        //templates/switchCaseThymeleaf/switchCaseThymeleaf-v1.html
        return "eachThymeleaf/eachThymeleaf-v1";

    }
}
```

* En la vista.

```html
<!DOCTYPE html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Thymeleaf</title>
    <style>
        table, td {
            border:1px solid black;
        }
        table {
            width:100%;
        }
        td {
            padding:5px;
        }
    </style>
</head>
<body>
    <h1>Thyemeleaf-v1</h1>
    <h2>-.Ejemplo sencillo de each en Thyemelaf.-</h2>
    <div>
        <p>Tabla de empleos</p>
        <table>
            <caption>Lista de empleos</caption>
            <thead>
                <tr>
                    <th>Nombre de la oferta</th>
                </tr>
            </thead>

            <tbody th:each="oferta:${ofertas}">
                <tr>
                    <td th:text="${oferta}"></td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>
```

<div style="text-align: justify">
    <p>
       Siendo el resultado del ejemplo anterior:
    </p>
</div>

![](..\img\anexos\anexo_5\img_4.png)

### Anexo V.E. Template o Layout.

<div style="text-align: justify">
    <p>
       Hay tres formas básicas de incluir contenido de un fragmento:
		<ul>
            <li>insert (insertar): inserta contenido dentro de la etiqueta</li>
            <li>reemplazar (replacereplace): reemplaza la etiqueta actual con la etiqueta que define el fragmento</li>
            <li>include (incluir): está obsoleto, pero aún puede aparecer en un código heredado</li>
		</ul>
    Para definir un codigo como framgneto, se utiliza el atributo "<b>th:fragment</b>" y luego se invomca con las tres opciones nombradas anteriroremnte.
    </p>
</div>

```html
<!-- Contenido marcado como fragmento -->
<etiqueta th:fragment="name_fragment">
    ....
</etiqueta>
<!-- Invocación -->
<etiqueta th:insert="ruta del html :: name del fragmento" />
<etiqueta th:remplace="ruta del html :: name del fragmento" />
<etiqueta th:include="ruta del html :: name del fragmento" />
```

<div style="text-align: justify">
    <p>
      Auemás, no hace falta añadir por defecto ningun elemento o funcionalidad adicional, Un ejemplo, podría ser siguiente.
    </p>
</div>

* Ubicación de los ficheros.

![](..\img\anexos\anexo_5\img_5.png)

* Controllador.

```html
package com.example.thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.sql.Timestamp;

@Controller
public class ThymelafFramgentController {

    /**
     * Handler, QUE
     * Método HTTP: Get
     * Recurso: /FragmentThymeleaf
     * @param model
     * @return String
     *
     * Example: http://localhost:8080/FragmentThymeleaf
     */
    @GetMapping(value="/FragmentThymeleaf",name = "sendParameterModel")
    public String useFragmentThymeleaf(Model model){

        return "layoutThymeleaf/index";
    }
}

```

* Vistas.

  * Aspecto de un fragmento - `footer.html`

    ```
    <!DOCTYPE html>
    <html lang="es" xmlns:th="http://www.thymeleaf.org">
    <body>
    <footer th:fragment="footer">
        <p>&copy; 2021 arp.com</p>
    </footer>
    </body>
    </html>
    ```

  * Carga de fragmentos - `index.html`

    ```html
    <!DOCTYPE html>
    <html lang="es" xmlns:th="http://www.thymeleaf.org">
    <!--
    <head th:insert="layoutThymeleaf/common/head/fm-head.html :: head"> </head>
    -->
    <head th:replace="layoutThymeleaf/common/head/fm-head.html :: head"></head>
    <body>
    
    </body>
    <!-- START NAV-->
    <nav th:replace="layoutThymeleaf/common/nav/fm-nav :: nav">
    </nav>
    <!-- END NAV-->
    
    <!-- START TEXT-->
    <h1>HELLO WORLD!!</h1>
    <!-- END TEXT-->
    
    <!-- START FOOTER-->
    <footer th:insert="layoutThymeleaf/common/footer/fm-footer :: footer"></footer>
    <!-- START FOOTER-->
    </html>
    ```
<div style="text-align: justify">
    <p>
       El efecto visual quedaría de la siguiente manera:
    </p>
</div>
![](C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\anexo_5\img_6.png)

## Anexo VI. projectlombok

### 

[![img](https://cleventy.com/wp-content/uploads/2020/05/lombokproject-250x89.png)](https://cleventy.com/wp-content/uploads/2020/05/lombokproject.png)

Utilizamos [projectlombok](https://projectlombok.org/) para **implementar de una manera más sencilla algunos métodos básicos y comunes** en el desarrollo, como por ejemplo:

1. *getters* y *setters*, para la obtención y asignación de valores de atributos
2. constructores y *builders*, para la creación de objetos
3. acceso a *logs*, para la depuración de código

De esta manera, vemos que, siguiendo el ejemplo de [Usuario](https://github.com/cleventy/springboilerplate/blob/master/src/main/java/com/cleventy/springboilerplate/business/entities/user/User.java), esta clase no explicita ninguno de estos métodos que sí son utilizados en otras clases (servicios). De esta forma, en el [servicio de usuarios](https://github.com/cleventy/springboilerplate/blob/master/src/main/java/com/cleventy/springboilerplate/business/services/userservice/UserServiceImpl.java) vemos que accedemos a métodos para obtener valores de [usuario](https://github.com/cleventy/springboilerplate/blob/master/src/main/java/com/cleventy/springboilerplate/business/entities/user/User.java) por ejemplo en:

```java
private static UserDetailsCO toUserDetailsCO(User user) {
  return new UserDetailsCO(user.getId(), user.getUsername(), 
//    user.getEmail(), user.getName(), user.getRole(), user.getState());

}
```

O a constructores builders en:

```java
User user = User
  .builder()
  .username(userDetailsForm.getUsername())
  .password(**this**.passwordEncoder.encode(userDetailsForm.getPassword()))
  .email(userDetailsForm.getEmail())
  .name(userDetailsForm.getName())
  .role(userDetailsForm.getRole())
  .state(userDetailsForm.getState())
  .registerDate(DateUtil.getNow())
  .build();
```

A pesar de que esos métodos parecen no existir en la clase [Usuario](https://github.com/cleventy/springboilerplate/blob/master/src/main/java/com/cleventy/springboilerplate/business/entities/user/User.java). Esto es porque esos métodos son generados de manera automática (al poner las anotaciones correspondientes) por el projectlombok.

Además, vemos por ejemplo el [controlador de autenticación web](https://github.com/cleventy/springboilerplate/blob/master/src/main/java/com/cleventy/springboilerplate/web/controller/web/WebAuthenticationController.java) una anotación *@Slf4j* (también gracias a projectlombok) que lo que permite es utilizar de manera cómoda métodos de logging como el que está en ese mismo controlador:

```java
log.debug("web login");
```

Por último, resaltar que es necesario realizar una configuración especial para integrar el projectlombok con Eclipse, para lo cual deberemos seguir [esta guía](https://projectlombok.org/setup/eclipse).

## Anexo VII. Boostsrap

![](..\img\anexos\anexo_7\img.jpeg)

Dependiendo de la forma que hayas elegido para descargar Bootstrap, verás una estructura de directorios u otra. En este anexo se muestran los detalles de cada una de ellas.

### Anexo VII.A. Contenidos de la versión compilada de Bootstrap

Después de descomprimir el archivo que te has descargado con la versión compilada de Bootstrap, verás la siguiente estructura de archivos y directorios:

```
bootstrap/
├── css/
│   ├── bootstrap-grid.css
│   ├── bootstrap-grid.css.map
│   ├── bootstrap-grid.min.css
│   ├── bootstrap-grid.min.css.map
│   ├── bootstrap-reboot.css
│   ├── bootstrap-reboot.css.map
│   ├── bootstrap-reboot.min.css
│   ├── bootstrap-reboot.min.css.map
│   ├── bootstrap.css
│   ├── bootstrap.css.map
│   ├── bootstrap.min.css
│   └── bootstrap.min.css.map
└── js/
    ├── bootstrap.bundle.js
    ├── bootstrap.bundle.js.map
    ├── bootstrap.bundle.min.js
    ├── bootstrap.bundle.min.js.map
    ├── bootstrap.js
    ├── bootstrap.js.map
    ├── bootstrap.min.js
    └── bootstrap.min.js.map
```

Estos archivos son la forma más sencilla de utilizar Bootstrap en cualquier proyecto web. Para cada archivo se ofrecen dos variantes: los archivos compilados (cuyo nombres son `bootstrap.*`) y los archivos compilados + comprimidos (cuyos nombres son `bootstrap.min.*`). También se incluyen los *[source maps](https://developers.google.com/web/tools/chrome-devtools/javascript/source-maps)* (cuyos nombres son `bootstrap.*.map`) para utilizarlos en los navegadores que los soporten.

Los archivos JavaScript (llamados `bootstrap.bundle.js` para la versión normal y `bootstrap.bundle.min.js` para la versión minimizada) includen la librería [Popper](https://popper.js.org/) pero no la librería [jQuery](https://jquery.com/).

#### Anexo VII.A.i. Archivos CSS

Bootstrap ofrece diferentes archivos CSS compilados para que puedas elegir el que más te convenga.

| Archivo CSS            | Layout          | Contenido   | Componentes | Utilidades  |
| ---------------------- | --------------- | ----------- | ----------- | ----------- |
| `bootstrap.css`        | Incluido        | Incluido    | Incluido    | Incluido    |
| `bootstrap-grid.css`   | Solo la rejilla | No incluido | No incluido | Solo Flex   |
| `bootstrap-reboot.css` | No incluido     | Solo Reboot | No incluido | No incluido |

#### Anexo VII.A.ii. Archivos JavaScript

Igualmente, Bootstrap ofrece diferentes archivos JavaScript.

| Archivo JavaScript    | Popper.js   | jQuery      |
| --------------------- | ----------- | ----------- |
| `bootstrap.bundle.js` | Incluido    | No incluido |
| `bootstrap.js`        | No incluido | No incluido |

### Anexo VII.B. Integración.

Bootstrap puede integrarse:

* Mediante CDN.
* Incluyendo los ficheros comprimidos.
* Incluyendo los ficheros fuentes.
* Mediante gestores de contenido.

#### Anexo VII.B. Integración de Bootstrap CDN.

Usando el servicio de [BootstrapCDN](https://www.bootstrapcdn.com/), puedes incluir Bootstrap en tus proyectos añadiendo la siguiente línea en todas tus páginas:

```html
<head>
    <!-- ... -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
```

La URL concreta y el valor del atributo `integrity` dependen de cada versión de Bootstrap. Visita el sitio BootstrapCDN para obtener siempre la versión más reciente.

Además, muchos de los componentes de Bootstrap requieren JavaScript para funcionar. En concreto, requieren [jQuery](https://jquery.com/), [Popper.js](https://popper.js.org/) y los propios plugins creados por Bootstrap. Para enlazarlos, añade las siguientes líneas justo antes de la etiqueta `</body>` de cierre.

Aunque recomendamos el uso de [jQuery's Slim](https://blog.jquery.com/2016/06/09/jquery-3-0-final-released/), la versión completa de jQuery también funciona:

```html
<!-- ... -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
```

El orden en el que se enlazan los archivos JavaScript tiene que ser exactamente ese: primero jQuery, luego Popper y luego Bootstrap. Las URLs correctas para cada versión de esas librerías las puedes encontrar visitando cada uno de los sitios que las alojan.

> :notebook:**Nota**
>
> Si utilizas nuestro archivo `bootstrap.bundle.js` (o `bootstrap.bundle.min.js`) la librería Popper ya está incluida, pero jQuery no.

```html
<!doctype html>
<html lang="es">
  <head>
    <!-- Etiquetas <meta> obligatorias para Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Enlazando el CSS de Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>¡Hola Mundo!</title>
  </head>
  <body>
    <h1>¡Hola Mundo!</h1>

    <!-- Opcional: enlazando el JavaScript de Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
```

#### Anexo VII.B. Integración mediante ficheros comprimidos.

Vamos a la web oficial de bootstrap [getbootstrap.com](https://getbootstrap.com/docs/5.0/getting-started/download/), para descargar un archivo ZIP comprimido con los archivos CSS y JavaScript listos para usar. Los archivos se entregan compilados y minimizados, pero no incluyen ni documentación, ni el código fuente, ni las dependencias JavaScript como jQuery y Popper.js.

```html
<!doctype html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
  <head>
    <!-- Etiquetas <meta> obligatorias para Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Enlazando el CSS de Bootstrap -->
	<link th:href="@{/bootstrap-5.0.0-beta3-dist/css/bootstrap.css}" rel="stylesheet" />
    <title>Spring Boot Thymeleaf - Bootstrap WebJars</title>
  </head>
  <body>
    <h1>Hola Mundo!</h1>

    <!-- Opcional: enlazando el JavaScript de Bootstrap -->
    <!-- 
    1º jquery-3.3.1.slim.min.js
    2º popper.min.js
    3º bootstrap.min.js
    Nota:
    Si utilizas nuestro archivo bootstrap.bundle.js (o bootstrap.bundle.min.js) la librer�a Popper ya est� incluida, pero jQuery no. -->
    <script th:src="@{/bootstrap-5.0.0-beta3-dist/jquery/3.0.0/jquery-3.0.0.min.js}"></script>
    <script th:src="@{/bootstrap-5.0.0-beta3-dist/js/bootstrap.bundle.js}"></script>  
  </body>
</html>
```

![](..\img\anexos\anexo_7\img1.png)

#### Anexo VII.B. Integración mediante ficheros sources.

Vamos a la web oficial de bootstrap [getbootstrap.com](https://getbootstrap.com/docs/5.0/getting-started/download/), para descubrir la forma en la que puedes descargar el código fuente original de los archivos CSS y JavaScript de Bootstrap.

Para integrar esos archivos en tu aplicación, necesitarás herramientas adicionales como un compilador Sass, la librería [Autoprefixer](https://github.com/postcss/autoprefixer), etc.



Anexo VII.B. Integración de Bootstrap CDN.

## Anexo VIII. Spring Boot + MySQl.

<div style="text-align: justify">
    <p>
       En este tutorial te mostrare como conectar a una base de datos <b>MYSQL</b>. Obviamente como <b>pre-requisito</b> debes de tener instalado tu servidor local de MYSQL, te recomiendo XAMPP el cual cuanta con APACHE, MySQL, TOMCAT, ...<br>
       Para conectarlo con tu projecto de Spring Boot y utilizando maven, hay que agregar el siguiente código en tu pom.xml y configurar el application.properties para apuntar a tu base de datos.
    </p>
</div> 

### Anexo VIII.A. Fichero "porm.xml"

```xml

<!-- Configuracion del ORM de spring boot para persistencia -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>

<!-- Conector/libreria de MYSQL para java -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
```

### Anexo VIII.B. Fichero "application.properties".

```properties
#Data source 
#Indica el driver/lib para conectar java a mysql
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver

#Url donde esta el servicio de tu mysql y el nombre de la base de datos
spring.datasource.url=jdbc:mysql://localhost:3306/mydatabase

#Usuario y contrasena para tu base de datos descrita en la linea anterior
spring.datasource.username=root
spring.datasource.password=root

#[opcional]Imprime en tu consola las instrucciones hechas en tu base de datos.
spring.jpa.show-sql = true
```

En este tutorial te mostrare como conectar a una base de datos **MYSQL**. Obviamente como **pre-requisito** debes de tener instalado tu servidor local de MYSQL.

## Anexo IX. Spring Boot Securty Inicion de Sesión con MySQL.

<div style="text-align: justify">
    <p>
       En este Anexo se describe el proceso de como crear un inicio de sesión seguro en Spring Boot, por otro lado el usuario quedrá definido por un rol de tipo "USER" o de tipo "ADMIN".<br>
       Lo pirmera será crear nuestro proyecto con las siguientes dependencias;
    </p>
</div> 

| Dependencia | Descripción                                                  |
| ----------- | ------------------------------------------------------------ |
| Web         | Tecnología necesaria para elaborarun proyecto Web con Spring |
| Thymeleaf   | Motor de plantillas para generar la vista.                   |
| Devtools    | Conjunto de herramientas para desarrollo                     |
| JPA         | Java Persistente API, tecnología estándar de Java para gestionar  entidades persistentes |
| MySQL       | Driver para establecer conexión con la BBDD                  |

![Loggin_1](..\img\anexos\anexo_8\img1.png)

<div style="text-align: justify">
    <p>
       Lo siguiente será añadir la ocnfiguración de nuestro proyecto, para ello añadimos el siguiente código al fichero "application.properties":
    </p>
</div> 

```
#Server
server.port= 8090

#Data source 
#Indica el driver/lib para conectar java a mysql
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver

#Url donde esta el servicio de tu mysql y el nombre de la base de datos
spring.datasource.url=jdbc:mysql://localhost:3306/test

#Usuario y contrasena para tu base de datos descrita en la linea anterior
spring.datasource.username=root
spring.datasource.password=root

#[opcional]Imprime en tu consola las instrucciones hechas en tu base de datos.
spring.jpa.show-sql = true
```



## Anexo X. Plantilla HTML 5 con Thymeleaf

```html
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<!-- Plantilla de sitio de tres columnas en HTML5 -->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="author" content="JMSR">
  <meta name="description" content="Plantilla para documento HTML 5">
  <meta name="keywords" content="HTML, CSS">
  
  <title>Plantilla para documento HTML 5</title>
  
  <link rel="icon" type="image/png" href="favicon.ico">
  <link rel="stylesheet" type="text/css" href="base.css">
  <style>
    *
    {
      margin: 0;
      padding: 0;
    }
    #espacioSuperior
    {
      background-color: #E6E6E6;
    }
    #contenedorPagina
    {
      margin: 0 auto;
      border: 1px solid #9900CC;
      height: 85%;
    }
    
    #encabezado
    {
      height: 50px;
      background-color: #FF3300;
    }
    #aLaIzquierda
    {
      width: 20%;
      height: 500px;
      float: left;
      background-color: #0066FF;
    }
    #contenidoPrincipal
    {
      width: 60%;
      float: left;
      background-color: #FFFFFF;
    }
    #aLaDerecha
    {
      width: 20%;
      height: 500px;
      float: right;
      background-color: #006600;
    }
    #piePagina
    {
      clear: both;
      height: 25px;
      background-color: #CC6699;
    }
  </style>
</head>
<body>
  <div id="espacioSuperior">
    <h1>Plantilla para documento HTML 5</h1>
  </div>
  <div id="contenedorPagina">
    <header id="encabezado">
      <p>#encabezado</p>
    </header>
    <aside id="aLaIzquierda">
      <p>#aLaIzquierda</p>
    </aside>
    <section id="contenidoPrincipal">
      <p>#contenidoPrincipal</p>
    </section>
    <aside id="aLaDerecha">
      <p>#aLaDerecha</p>
    </aside>
    <footer id="piePagina">
      <p>#piePagina</p>
    </footer>
  </div>
</body>
</html>
```

## Anexo XI.  TinyMCE

<div style="text-align: justify">
    <p>
        <a href="https://www.tiny.cloud/get-tiny/">TinyMCE</a> es un editor de texto WYSIWYG para HTML de código abierto que funciona completamente en JavaScript y se distribuye gratuitamente bajo licencia LGPL Al ser basado en JavaScript TinyMCE es independiente de la plataforma y se ejecuta en el navegador de internet.
    </p>
</div> 

![](..\img\module_1\11FormularioHTML\TinyMCE.png)

<div style="text-align: justify">
    <p>
        Se puede usar meidante cdn en nuestro proyecto, tal que así:
    </p>
</div> 

```html
<!DOCTYPE html>
<html>
<head>
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>tinymce.init({selector:'textarea'});</script>
</head>
<body>
  <textarea>Next, use our Get Started docs to setup Tiny!</textarea>
</body>
</html>
```

<div style="text-align: justify">
    <p>
        O descargardo el framework y alojando los ficheros en nuestra ruta estática:
    </p>
</div> 

![](..\img\module_1\11FormularioHTML\tinymce-use-0.png)

```
<body>
....
	<div class="form-group">
		<label for="detalles"><strong>Detalles</strong></label>
		<textarea class="form-control" th:field="*{detalles}"
			name="detalles" id="detalles" rows="7"></textarea>							
	</div>
....
	<script>
	//Usamos un edito de texto
	tinymce.init({
		selector: '#detalles',
		plugins: "textcolor, table lists code",
		toolbar: " undo redo | bold italic | alignleft aligncenter alignright alignjustify \n\
                    | bullist numlist outdent indent | forecolor backcolor table code"
	});
	<\script>
<\body>
```



![](..\img\module_1\11FormularioHTML\tinymce-use-1.png)



Capítulo 1:

[1] [Spring Boot Thymeleaf y su configuración](https://www.arquitecturajava.com/spring-boot-thymeleaf-y-su-configuracion/)

[2] [Spring Boot Starter Web](https://www.javatpoint.com/spring-boot-starter-web)

[3] [Spring Boot DevTools y recarga de aplicaciones](https://www.arquitecturajava.com/spring-boot-devtools-y-recarga-de-aplicaciones/#:~:text=Spring%20Boot%20DevTools%20es%20la,que%20nos%20devuelve%20un%20mensaje.)

[4] [Spring Boot Starter Test](https://www.javatpoint.com/spring-boot-starter-test)

Capítulo 2:

[1] [Thymeleaf](www.thymeleaf.org/)

Capítulo 8:

[1] [Controladores](http://acodigo.blogspot.com/2017/03/spring-mvc-controladores.html)

Anexos:

* Para habilitar el reinicio automático en IntelliJ:

[1] [INTELLiJ IDEA Fix Spring Boot DevTools not working](https://www.youtube.com/watch?v=Dpk-RUsZBug&t=437s)

[2] [Spring boot devtools - Static content reloading does not work in IntelliJ](https://stackoverflow.com/questions/35895730/spring-boot-devtools-static-content-reloading-does-not-work-in-intellij)

[3] [Intellij IDEA – Spring boot reload static file is not working](https://mkyong.com/spring-boot/intellij-idea-spring-boot-template-reload-is-not-working/)

* Para añadir un Template File en IntelliJ:

[1] [File templates - WebStorms](https://www.jetbrains.com/help/webstorm/using-file-and-code-templates.html)

* Flujo de una petición HTTP en Spring.

[1] [Introducción a MVC en Spring](http://www.jtech.ua.es/j2ee/publico/spring-2012-13/sesion03-apuntes.html#:~:text=Spring%20es%20una%20implementación%20del,a%20través%20del%20front%20controller.)

General:

[1] [Aprende a mejorar la presentación de tus publicaciones de Steemit con MARKDOWN Y HTML](https://hive.blog/spanish/@atmoxphera/aprende-a-mejorar-la-presentacion-de-tus-publicaciones-de-steemit-con-markdown-y-html-parte-1-html)

[2] [Spring Boot + Boostrap + Thymeleaf + Markdown-editor](https://frontbackend.com/thymeleaf/spring-boot-bootstrap-thymeleaf-markdown-editor)

Boostrap

[1] [Bootstrap 4, el manual oficial](https://uniwebsidad.com/libros/bootstrap-4/capitulo-1)

Externalizar Configruación

[1] [Externalizar Configuración](https://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/html/boot-features-external-config.html)

