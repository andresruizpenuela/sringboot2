package net.andres.empleados.controller;

import net.andres.empleados.configuration.MyLogger;
import net.andres.empleados.configuration.MyProperties;
import net.andres.empleados.model.Vacante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Controller
public class HomeController {
    /* START DEPENDENDCIAS PROJECT */
    @Autowired
    MyProperties properties;
    /* END DEPENDENDCIAS PROJECT */

    /* START ATRIBUTE */
    private static MyLogger logger;     //Logger


    /* END ATRIBUTE */

    /* START METHOD INIT CLASS */
    @PostConstruct
    public void init(){
        logger = new MyLogger(HomeController.class);
    }
    /* START METHOD INIT CLASS */

    /*
     * http://localhost:8080/
     * */
    @GetMapping(value = "/")
    public String getHomePage(Model model){
        return "home";
    }

    /*
    * http://localhost:8080/listado
    * */
    @GetMapping(value = "/listado")
    public String mostrarListado(Model model){
        //Lista de ofertas
        List<String> lista = new LinkedList<String>();

        // Inicializmaos la lista de ofertas
        lista.add("Ingeniero de Sistemas");
        lista.add("Auxiliar de Contabilidad");
        lista.add("Vendedor");
        lista.add("Arquitecto");

        // Enviamos la lista de ofertas a la vista
        model.addAttribute("headTable","ofertas");
        model.addAttribute("empleos",lista);

        // Devolvemos la vista
        return "listado";
    }

    /*
     * http://localhost:8080/mostrarDetalle
     * */
    @GetMapping("mostrarDetalle")
    public String mostrarDetalleVacante(Model model){
        //model.addAttribute("vacante",vacantes.stream().findFirst().orElse(null));
        Vacante vacante = new Vacante(1,
                "Ingeniero de comunicaciones",
                "Se solicita un ingeniero para dar soperte a Internet.",
                new Date(),
                9750.0,
                1
        );
        logger.info("mostrarDetalle",vacante.toString());
        model.addAttribute("vacante",vacante);
        return "detalle";
    }

    /*
     * http://localhost:8080/mostrarTabla
     * */
    @GetMapping("mostrarTabla")
    public String mostrarTabla(Model model){
        // Lista de ofertas/vacantes
        List<Vacante> lista = getVacante();

        // Mensaje en consola
        logger.info("mostrarDetalle",""+lista.stream().findFirst().orElse(null));
        logger.info("mostrarDetalle", "image ="+lista.get(0).getImg());
        // Enviamos la lista a la vista
        model.addAttribute("vacantes",lista);
        return "tabla";
    }

    /*
     * Método que devuelve una lista de objetos de tipo Vacante
     * @return
     */
    private List<Vacante> getVacante(){
        // Parámetros locales
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
        List<Vacante> lista = new LinkedList<Vacante>();

        try{
            // Creamos la oferta de Trabajo 1.
            Vacante vacante1 = new Vacante();
            vacante1.setId(1);
            vacante1.setName("Ingnerio Civil");
            vacante1.setDescription("Solcitimaso Ing. Civil para diseñar puente peatonal.");
            vacante1.setDate(sdf.parse("08-02-2020"));
            vacante1.setSalary(8500.0);
            vacante1.setOutstanding(1);
            vacante1.setImg("empresa1.png");

            // Creamos la oferta de Trabajo 2.
            Vacante vacante2 = new Vacante();
            vacante2.setId(2);
            vacante2.setName("Contador Público");
            vacante2.setDescription("Empresa importante solicita contador con 5 años de experencia titulado.");
            vacante2.setDate(sdf.parse("09-02-2020"));
            vacante2.setSalary(1200.0);
            vacante2.setOutstanding(0);
            vacante2.setImg("empresa2.png");

            // Creamos la oferta de Trabajo 3.
            Vacante vacante3 = new Vacante();
            vacante3.setId(3);
            vacante3.setName("Ingeniero Eléctrico");
            vacante3.setDescription("Empresa internacional solicata Ingeniero mecánico para mantenimiento de la instalación eleéctrica.");
            vacante3.setDate(sdf.parse("10-02-2020"));
            vacante3.setSalary(10500.0);
            vacante3.setOutstanding(0);
            vacante3.setImg("empresa3.png");

            // Creamos la oferta de Trabajo 4.
            Vacante vacante4 = new Vacante();
            vacante4.setId(4);
            vacante4.setName("Diseñaro Gráfico");
            vacante4.setDescription("Solicitamos Diseñaro Gráfico titulado para diseñar publicidad de la empresa.");
            vacante4.setDate(sdf.parse("11-02-2020"));
            vacante4.setSalary(7500.0);
            vacante4.setOutstanding(1);

            /*
             * Agegamos las intnaicas del objeto Vacante a la lista ...
             */
            lista.add(vacante1);
            lista.add(vacante2);
            lista.add(vacante3);
            lista.add(vacante4);

            // Mensaje de consola
            logger.info("getVacante","lista de vacantes inicializada correctamente.");

        }catch (Exception e){

            // Mensaje de consola
            logger.error("getVacante","error al inicializar la lista de vacantes: \n"+e);
        }

        return lista;
    }
}
