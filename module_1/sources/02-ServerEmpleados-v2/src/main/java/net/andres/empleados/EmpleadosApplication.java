package net.andres.empleados;

import net.andres.empleados.configuration.MyLogger;
import net.andres.empleados.configuration.MyProperties;
import net.andres.empleados.controller.HomeController;
import net.andres.empleados.model.Vacante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

@SpringBootApplication
public class EmpleadosApplication {
    /* START DEPENDENDCIAS PROJECT */
    @Autowired
    private static MyProperties properties;
    /* END DEPENDENDCIAS PROJECT */

    //Logger
    private static final MyLogger logger = new MyLogger(EmpleadosApplication.class);


    public static void main(String[] args) {
        SpringApplication.run(EmpleadosApplication.class, args);

        /* LOGGER INIT() */
        logger.info("main",
                properties.NAMEAPP
                        +" "+properties.VERSION
                        +" "+properties.COPY
                        +" Inicializada"+properties.PORTAPP);
        /* LOGGER INIT() */

    }

}
