package net.andres.empleados.model;

import java.util.Date;
import java.util.Objects;

public class Vacante {
    private Integer id;
    private String name;
    private String description;
    private Date date;
    private Double salary;
    /* START CONSTRUCTORES */
    public Vacante() { }

    public Vacante(Integer id, String name, String description, Date date, Double salary) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.salary = salary;
    }
    /* END CONSTRUCTORES*/

    /* START GETTERS & SETTERS */

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getSalary() {
        return salary;
    }
    public void setSalary(Double salary) {
        this.salary = salary;
    }
    /* END GETTERS & SETTERS */

    /* START METHODS GENERIC */
    @Override
    public String toString() {
        return "Vacante{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", salary=" + salary +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
    /* END METHODS GENERIC */
    
}
