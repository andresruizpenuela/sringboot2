package com.aruizpen.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeContreller {
	
	@GetMapping("/")
	public String getHomePage(){
		return "home";
	}
}
