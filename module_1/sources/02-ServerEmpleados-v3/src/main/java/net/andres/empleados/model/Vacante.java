package net.andres.empleados.model;

import java.util.Date;
import java.util.Objects;

public class Vacante {
    private Integer id;
    private String name;
    private String description;
    private Date date;
    private Double salary;
    private Integer outstanding; //destacado
    /* START CONSTRUCTORES */
    public Vacante() { }

    public Vacante(Integer id, String name, String description, Date date, Double salary, Integer outstanding) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.salary = salary;
        this.outstanding=outstanding;
    }
    /* END CONSTRUCTORES*/

    /* START GETTERS & SETTERS */
    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public Double getSalary() { return salary; }

    public void setSalary(Double salary) { this.salary = salary; }

    public Integer getOutstanding(){ return this.outstanding; }

    public void setOutstanding(Integer outstading){ this.outstanding = outstading; }
    /* END GETTERS & SETTERS */

    /* START METHODS GENERIC */
    @Override
    public String toString() {
        return "Vacante{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", salary=" + salary +
                ", outstading=" + outstanding +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
    /* END METHODS GENERIC */

}
