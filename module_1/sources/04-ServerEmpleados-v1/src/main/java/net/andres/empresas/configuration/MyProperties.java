package net.andres.empresas.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;

@Configuration
@Description("Clase para obtener las propiedades del generias del proyecto")
public class MyProperties {
    /* START DEFINITION PROPERTIES */
    public static String AUTHOR;
    public static String COPY;
    public static String VERSION;
    public static String NAMEAPP;
    public static int PORTAPP;
    /* END DEFINITION PROPERTIES */

    /* START METHODS SETTERS */
    @Value("${author}")
    private void setAuthor(String value) { MyProperties.AUTHOR = value; }
    @Value("${copy}")
    private void setCopy(String value) { MyProperties.COPY = value; }
    @Value("${versionApp}")
    private void setVersion(String value) { MyProperties.VERSION = value; }
    @Value("${spring.application.name}")
    private void setNameApp(String value) { MyProperties.NAMEAPP = value;}
    @Value("${server.port}")
    private void setPortApp(int value) { MyProperties.PORTAPP = value; }
    /* START METHODS SETTERS*/

}
