package net.andres.empresas.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyLogger {
    /* START ATRIBUTES OF CLASS */
        // Define the logger object for this class
    private static Logger logger;
        // Define the name of class
    private static String nameClass;
    /* END ATRIBUTES OF CLASS */


    /* START CONSTRUCTOR LOGGER */
    public MyLogger(Class theClass){
        logger = LoggerFactory.getLogger(theClass);
        nameClass = theClass.getName();
    }
    /* START CONSTRUCTOR LOGGER GENERIC */

    /* START METHODS LOGGER GENERIC */
    public void info(String nameMethod,String msg) {
        // Define the header of message logger
        String headMenssage = "["+nameClass+"] - "
                +"["+nameMethod+"]";
        // Print logger message
        this.logger.info(headMenssage+"::"+msg);
    }
    public void warn(String nameMethod,String msg) {
        // Define the header of message logger
        String headMenssage = "["+nameClass+"] - "
                +"["+nameMethod+"]";
        // Print logger message
        this.logger.warn(headMenssage+"::"+msg);
    }
    public void error(String nameMethod,String msg) {
        // Define the header of message logger
        String headMenssage = "["+nameClass+"] - "
                +"["+nameMethod+"]";
        // Print logger message
        this.logger.error(headMenssage+"::"+msg);
    }
    public void debug(String nameMethod,String msg) {
        // Define the header of message logger
        String headMenssage = "["+nameClass+"] - "
                +"["+nameMethod+"]";
        // Print logger message
        this.logger.error(headMenssage+"::"+msg);
    }
    /* END METHODS LOGGER GENERIC */
}
