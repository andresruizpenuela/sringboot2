package net.andres.empresas.controller;

import net.andres.empresas.configuration.MyLogger;
import net.andres.empresas.configuration.MyProperties;
import net.andres.empresas.model.Vacante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Controller
public class HomeController {
    /* START DEPENDENCIAS */
    @Autowired
    MyProperties properties;
    /* END DEPENDENCIAS */

    /* START ATRIBUTTES */
    private MyLogger logger = new MyLogger(HomeController.class);
    /* END ATRIBUTTES */

    /*
    * Método que devuelve una vista con thyemeleaf
    * Methodo HTTP: GET
    * Resource: /
    * Return: home
    * Example call: http://localhost:9090/
    * */
    @GetMapping("/")
    public String mostrarHome(Model model) {
        /* Elementos que compone una oferta ejemplo */
        String nombre = "Auxiliar de Contabilidad";
        Date fechaPub = new Date();
        double salario = 9000.0;
        boolean vigente = true;
        String description = "Se busca auxiliar de contabiladad titualo con al menos 3 años de experencia en el sector";

        /* START ADD ATRTIBUTES TO VIEW */
        /* Datos genercos sobre la aplicación web */
        model.addAttribute("title",properties.NAMEAPP);
        model.addAttribute("version",properties.VERSION);
        model.addAttribute("copy",properties.COPY);

        /* Datos de bienvencida */
        model.addAttribute("mensaje", "Bienvenidos a Empleos App");
        model.addAttribute("fecha", new Date());

        /* Datos de una oferta web */
        model.addAttribute("nombre", nombre);
        model.addAttribute("fecha", fechaPub);
        model.addAttribute("descripcion", description);
        model.addAttribute("salario", salario);
        model.addAttribute("vigente", vigente);
        /* END ADD ATRTIBUTES TO VIEW */

        return "home";
    }

    /*
     * Método que una devuelve una lista de String a una vista con thyemelef
     * Methodo HTTP: GET
     * Resource: /listado
     * Return: listado
     * Example call: http://localhost:9090/listado
     * */
    @GetMapping("/listado")
    public String mostrarListado(Model model) {
        List<String> lista = new LinkedList<>();
        lista.add("Ingeniero  de Sistemas");
        lista.add("Auxiliar de Contabilidad");
        lista.add("Vendedor");
        lista.add("Arquitecto");

        /* Datos genercos sobre la aplicación web */
        model.addAttribute("title",properties.NAMEAPP);
        model.addAttribute("version",properties.VERSION);
        model.addAttribute("copy",properties.COPY);

        /* Lista de titutlos de ofertas*/
        model.addAttribute("empleos", lista);

        return "listado";
    }

    /*
     * Método que una devuelve una oferta completa a una vista con thyemelef
     * Methodo HTTP: GET
     * Resource: /detalle
     * Return: detalle
     * Example call: http://localhost:9090/detalle
     * */
    @GetMapping("/detalle")
    public String mostrarDetalle(Model model) {
        /* Obtenemos la oferta */
        Vacante vacante = new Vacante();
        vacante.setNombre("Ingeniero de comunicaciones");
        vacante.setDescripcion("Se solicita ingeniero para dar soporte a intranet");
        vacante.setFecha(new Date());
        vacante.setSalario(9700.0);

        /* Datos genercos sobre la aplicación web */
        model.addAttribute("title",properties.NAMEAPP);
        model.addAttribute("version",properties.VERSION);
        model.addAttribute("copy",properties.COPY);


        /* Lista de titutlos de ofertas*/
        model.addAttribute("vacante", vacante);
        return "detalle";
    }

    /*
     * Método que una devuelve una lista de vacantes comletas a una vista con thyemelef
     * Methodo HTTP: GET
     * Resource: /tabla
     * Return: tabla
     * Example call: http://localhost:9090/dtabla
            * */
    @GetMapping("/tabla")
    public String mostrarTabla(Model model) {
        List<Vacante> lista = getVacantes();

        /* Datos genercos sobre la aplicación web */
        model.addAttribute("title",properties.NAMEAPP);
        model.addAttribute("version",properties.VERSION);
        model.addAttribute("copy",properties.COPY);

        /* Lista de ofertas*/
        model.addAttribute("vacantes", lista);

        return "tabla";
    }

    /**
     * Método que regresa una lista de objetos de tipo Vacante
     * @return List<Vacante>
     */
    private List<Vacante> getVacantes() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        List<Vacante> lista = new LinkedList<Vacante>();
        try {
            // Creamos la oferta de Trabajo 1.
            Vacante vacante1 = new Vacante();
            vacante1.setId(1);
            vacante1.setNombre("Ingeniero Civil"); // Titulo de la vacante
            vacante1.setDescripcion("Solicitamos Ing. Civil para diseñar puente peatonal.");
            vacante1.setFecha(sdf.parse("08-02-2019"));
            vacante1.setSalario(8500.0);
            vacante1.setDestacado(1);
            vacante1.setImagen("empresa1.png");

            // Creamos la oferta de Trabajo 2.
            Vacante vacante2 = new Vacante();
            vacante2.setId(2);
            vacante2.setNombre("Contador Publico");
            vacante2.setDescripcion("Empresa importante solicita contador con 5 años de experiencia titulado.");
            vacante2.setFecha(sdf.parse("09-02-2019"));
            vacante2.setSalario(12000.0);
            vacante2.setDestacado(0);
            vacante2.setImagen("empresa2.png");

            // Creamos la oferta de Trabajo 3.
            Vacante vacante3 = new Vacante();
            vacante3.setId(3);
            vacante3.setNombre("Ingeniero Eléctrico");
            vacante3.setDescripcion("Empresa internacional solicita Ingeniero mecánico para mantenimiento de la instalación eléctrica.");
            vacante3.setFecha(sdf.parse("10-02-2019"));
            vacante3.setSalario(10500.0);
            vacante3.setDestacado(0);

            // Creamos la oferta de Trabajo 4.
            Vacante vacante4 = new Vacante();
            vacante4.setId(4);
            vacante4.setNombre("Diseñador Gráfico");
            vacante4.setDescripcion("Solicitamos Diseñador Gráfico titulado para diseñar publicidad de la empresa.");
            vacante4.setFecha(sdf.parse("11-02-2019"));
            vacante4.setSalario(7500.0);
            vacante4.setDestacado(1);
            vacante4.setImagen("empresa3.png");

            /**
             * Agregamos los 4 objetos de tipo Vacante a la lista ...
             */
            lista.add(vacante1);
            lista.add(vacante2);
            lista.add(vacante3);
            lista.add(vacante4);

        } catch (ParseException e) {
            //System.out.println("Error: " + e.getMessage());
            logger.error("getVacantes","Error: " + e.getMessage());
        }

        return lista;

    }



}
