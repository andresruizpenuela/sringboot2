package net.andres.empresas.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value="/categorias")
public class CategoriasController {

    /*
     * Método que una devuelve una lista de categorias a una vista con thyemelef
     * Methodo HTTP: GET
     * Resource: /index
     * Return: index
     * Example call: http://localhost:9090/categorias/index
     * */
    // @GetMapping("/index")
    @RequestMapping(value="/index", method= RequestMethod.GET)
    public String mostrarIndex(Model model) {
        return "categorias-view/listCategorias";
    }

    /*
     * Método que crea un formulario para crear una categoria
     * Methodo HTTP: GET
     * Resource: /index
     * Return: index
     * Example call: http://localhost:9090/categorias/create
     * */
    // @GetMapping("/create")
    @RequestMapping(value="/create", method=RequestMethod.GET)
    public String crear() {
        return "categorias-view/formCategoria";
    }

    /*
     *  Método que crea una categoria nueva categorias
     * Methodo HTTP: GET
     * Resource: /save
     * Return: save
     * Example call: http://localhost:9090/categorias/save
     * */
    // @PostMapping("/save")
    @RequestMapping(value="/save", method=RequestMethod.POST)
    public String guardar(@RequestParam("nombre") String nombre, @RequestParam("descripcion") String descripcion) {
        System.out.println("Categoria:" + nombre);
        System.out.println("Descripcion: " + descripcion);

        return "categorias-view/listCategorias";
    }
}
