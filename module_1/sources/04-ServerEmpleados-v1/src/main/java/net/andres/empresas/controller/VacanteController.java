package net.andres.empresas.controller;

import net.andres.empresas.model.Vacante;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@Controller
@RequestMapping("/vacantes")
public class VacanteController {
    /*
     * Método que dado el id como parametro de entrada elimianra una vacante
     * Methodo HTTP: GET
     * Resource: /delete
     * Param: id-Integger
     * Return: mensaje
     * Example call: http://localhost:9090/detalle
     * */
    @GetMapping("/delete")
    public String eliminar(@RequestParam("id") int idVacante, Model model) {
        System.out.println("Borrando vacante con id: " + idVacante);
        model.addAttribute("id", idVacante);
        return "vacante-view/mensaje";
    }

    /*
     * Método que meustra el detalle detalle de una oferta solicitada
     * Methodo HTTP: GET
     * Resource: /delete
     * Return: mensaje
     * Example call: http://localhost:9090/detalle/{id}
     * */
    @GetMapping("/view/{id}")
    public String verDetalle(@PathVariable("id") int idVacante, Model model) {
        System.out.println("IdVacante: " + idVacante);
        model.addAttribute("idVacante", idVacante);
        // Buscar los detalles de la vacante en la BD...
        return "vacante-view/detalle";
    }

}
