package net.andres.model.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.sql.Timestamp;

@Controller
public class HomeController {
    @GetMapping("/")
    public String mostrarHome(Model model){
        /* Defenimos las varaibles */
        String nombre = "Auxiliar de contabilidad";
        Timestamp fechaPublicacion = new Timestamp(System.currentTimeMillis());
        double salario = 9000.0;
        boolean vigente = true;

        /* La agregamos al Model*/
        model.addAttribute("titleWeb","Spring Boot + Thymeleaf");
        model.addAttribute("name",nombre);
        model.addAttribute("date",fechaPublicacion);
        model.addAttribute("salary",salario);
        model.addAttribute("status",vigente);
        return "home";

    }
}
