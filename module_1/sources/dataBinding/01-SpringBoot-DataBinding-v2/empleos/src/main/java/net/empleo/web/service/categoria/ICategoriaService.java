package net.empleo.web.service.categoria;

import java.util.List;

import net.empleo.web.model.Categoria;

public interface ICategoriaService {

	void guardar(Categoria categoria);
	List<Categoria> buscarTodas();
	Categoria buscarPorId(Integer idCategoria);	

}
