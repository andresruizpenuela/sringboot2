package net.empleo.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import net.empleo.web.model.Vacante;
import net.empleo.web.service.categoria.ICategoriaService;
import net.empleo.web.service.vacante.IVacanteService;

@Controller
@RequestMapping(value="/vacantes")
public class VacantesController {
	
	@Autowired
	private IVacanteService serviceVacantes;
	
	@Autowired
	private ICategoriaService serviceCategorias; 
	
	@GetMapping("/create")
	public String crear(Vacante vacante, Model model) {
		model.addAttribute("categorias", serviceCategorias.buscarTodas() );
		model.addAttribute("vacante", serviceVacantes.buscarPorId(1) );
		return "vacantes/FormVacantes";
	}
	
	@PostMapping("/save")
	public String guardar(@RequestParam("nombre") String nombre, //test
			@RequestParam("descripcion") String descripcion,  //test
			@RequestParam("estatus") String estatus, //
			@RequestParam("fecha") String fecha, //test
			@RequestParam(value = "destacado",defaultValue = "0" ) int destacado, ///radio-button -> value 1,0
			@RequestParam("salario") double salario, //number
			@RequestParam("detalles") String detalles) {
		System.out.println("Nombre Vacante: " + nombre);
		System.out.println("Descripcion: " + descripcion);
		System.out.println("Estatus: " + estatus);
		System.out.println("Fecha Publicación: " + fecha);
		System.out.println("Destacado: " + destacado);
		System.out.println("Salario Ofrecido: " + salario);
		System.out.println("detalles: " + detalles);
		return "vacantes/listVacantes"; 
	}
	
	@GetMapping(value = "view/{id}")
	public String detalle(@PathVariable("id") int idVacnate, Model model) {
		Vacante vacante = serviceVacantes.buscarPorId(idVacnate);
		model.addAttribute("vacante",vacante);
		//model.addAttribute("vacante",null);
		return "vacantes/Detalle";
	}
	
	@GetMapping("/delete")
	public String eliminar(@RequestParam("id") int idVacante, Model model) {
		System.out.println("Borrando vacante con id: " + idVacante);
		model.addAttribute("id", idVacante);
		return "vacantes/Mensaje";
	}

}
