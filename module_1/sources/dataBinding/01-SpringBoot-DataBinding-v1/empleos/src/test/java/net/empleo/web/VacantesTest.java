package net.empleo.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import net.empleo.web.model.Vacante;
import net.empleo.web.service.vacante.IVacanteService;


@SpringBootTest
public class VacantesTest {

	@Autowired
	IVacanteService vacanteService;
	
	@Test
	void contextLoads() {
		Vacante v1 = new Vacante();
		Vacante v2 = new Vacante();
		
		System.out.println(v1.toString());
		System.out.println(v2.toString());
		System.out.println("Lista ordenada por defecto (orden de inserccion)");
		vacanteService.buscarTodas().forEach(System.out::println);
		
		System.out.println("Lista ordenada por fecha descendente");
		vacanteService.listaPorTiempoDes().forEach(System.out::println);
		
	}

}
