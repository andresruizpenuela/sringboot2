function validar(obj) {
	var textFechaError = document.getElementById('error-fecha');
  patron = /^\d{2}\/\d{2}\/\d{4}$/;
	
  if (!patron.test(obj.value)) {
    alert('Error');
	textFechaError.style.display = 'block';
  }else{
	textFechaError.style.display = 'none';
  }
}