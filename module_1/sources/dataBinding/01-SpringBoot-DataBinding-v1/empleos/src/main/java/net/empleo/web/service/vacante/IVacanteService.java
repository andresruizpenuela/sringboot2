package net.empleo.web.service.vacante;

import java.util.List;
import net.empleo.web.model.Vacante;

public interface IVacanteService {
	
	public List<Vacante> buscarTodas();
	public Vacante buscarPorId(Integer idVacante);
	public void guardar(Vacante vacante);
	public List listaPorTiempoDes();
	public List listaPorTiempoAs();
}
