package com.example.thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.sql.Timestamp;

@Controller
public class ThymelafTextController {

    /**
     * Handler, que envia varios atributos a la vista
     * Método HTTP: Get
     * Recurso: /ParamThymeleaf
     * @param model
     * @return String
     *
     * Example: http://localhost:8080/ParamThymeleaf
     */
    @GetMapping(value="/ParamThymeleaf",name = "sendParameterModel")
    public String sendParameterModel(Model model){
        model.addAttribute("mensajeVoid",null);
        model.addAttribute("mensaje","Esto es un mensaje desde el controlador");
        model.addAttribute("date",new Timestamp(System.currentTimeMillis()));
        model.addAttribute("email","andresruizpenuela@gmail.com");
        return "textThymeleaf";
    }
}
