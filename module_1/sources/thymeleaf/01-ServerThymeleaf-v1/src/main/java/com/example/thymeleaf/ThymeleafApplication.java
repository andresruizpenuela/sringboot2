package com.example.thymeleaf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Description;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.Locale;

@SpringBootApplication
public class ThymeleafApplication {
    Logger logger = LoggerFactory.getLogger(ThymeleafApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ThymeleafApplication.class, args);
    }





}
