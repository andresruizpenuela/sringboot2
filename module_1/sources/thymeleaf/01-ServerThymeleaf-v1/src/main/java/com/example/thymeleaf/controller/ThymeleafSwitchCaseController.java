package com.example.thymeleaf.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.LinkedList;
import java.util.List;

@Controller
public class ThymeleafSwitchCaseController {
    Logger logger = LoggerFactory.getLogger(ThymeleafSwitchCaseController.class);
    /**
     * Handler, que envia una lista de String + otros atributos al vista
     * Método HTTP: Get
     * Recurso: /SwitchCaseThymeleaf
     * @param model
     * @return String switchCaseThymeleaf-v1"
     *
     * Example: http://localhost:8080/SwitchCaseThymeleaf
     */
    @GetMapping(value="/SwitchCaseThymeleaf",name = "SwitchCaseThymeleaf")
    public String exampleSwitchCaseThymeleaf(Model model){
        logger.info("[exampleSwitchCaseThymeleaf] Procesando peticion /SwitchCaseThymeleaf");

        /* Lista de ofertas */
        List<String> list = new LinkedList<String>();
        list.add("Ingeniero de Sistemas");
        list.add("Auxiliar de contabilidad");
        list.add("Programador Junior");
        list.add("Comercial");

        /* Add Atributos a la vista */
        model.addAttribute("ofertas",list);

        /* Finalizamos */
        //templates/switchCaseThymeleaf/switchCaseThymeleaf-v1.html
        return "switchCaseThymeleaf/switchCaseThymeleaf-v1";

    }
}
