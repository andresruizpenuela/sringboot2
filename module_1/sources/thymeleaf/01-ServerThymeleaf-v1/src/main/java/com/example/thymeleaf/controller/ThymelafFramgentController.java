package com.example.thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.sql.Timestamp;

@Controller
public class ThymelafFramgentController {

    /**
     * Handler, QUE
     * Método HTTP: Get
     * Recurso: /FragmentThymeleaf
     * @param model
     * @return String
     *
     * Example: http://localhost:8080/FragmentThymeleaf
     */
    @GetMapping(value="/FragmentThymeleaf",name = "sendParameterModel")
    public String useFragmentThymeleaf(Model model){

        return "layoutThymeleaf/index";
    }
}
