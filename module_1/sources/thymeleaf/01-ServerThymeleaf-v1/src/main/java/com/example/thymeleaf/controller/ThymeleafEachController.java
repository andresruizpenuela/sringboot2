package com.example.thymeleaf.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.LinkedList;
import java.util.List;

@Controller
public class ThymeleafEachController {

    Logger logger = LoggerFactory.getLogger(ThymeleafEachController.class);
    /**
     * Handler, que envia una lista de String + otros atributos al vista
     * Método HTTP: Get
     * Recurso: /SwitchCaseThymeleaf
     * @param model
     * @return String switchCaseThymeleaf-v1"
     *
     * Example: http://localhost:8080/EachThymeleaf
     */
    @GetMapping(value="/EachThymeleaf",name = "SwitchCaseThymeleaf")
    public String exampleEachThymeleaf(Model model){
        logger.info("[exampleEachThymeleaf] Procesando peticion /EachThymeleaf");

        /* Lista de ofertas */
        List<String> list = new LinkedList<String>();
        list.add("Ingeniero de Sistemas");
        list.add("Auxiliar de contabilidad");
        list.add("Programador Junior");
        list.add("Comercial");

        /* Add Atributos a la vista */
        model.addAttribute("ofertas",list);

        /* Finalizamos */
        //templates/switchCaseThymeleaf/switchCaseThymeleaf-v1.html
        return "eachThymeleaf/eachThymeleaf-v1";

    }
}
