package com.example.thymeleaf.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.LinkedList;
import java.util.List;

@Controller
public class ThymleafIfController {
    Logger logger =  LoggerFactory.getLogger(ThymleafIfController.class);
    /**
     * Handler, que envia una lista de String + otros atributos al vista
     * Método HTTP: Get
     * Recurso: /IfThymeleaf
     * @param model
     * @return String ifThymeleaf-v1
     *
     * Example: http://localhost:8080/IfThymeleaf
     */
    @GetMapping(value="/IfThymeleaf",name = "ifThymeleafModel")
    public String exampleIfThymeleaf(Model model){
        logger.info("[exampleIfThymeleaf] Procesando peticion /IfThymeleaf");

        /* Lista de ofertas */
        List<String> list = new LinkedList<String>();
        list.add("Ingeniero de Sistemas");
        list.add("Auxiliar de contabilidad");
        list.add("Programador Junior");
        list.add("Comercial");

        /* Add Atributos a la vista */
        model.addAttribute("email","andresruizpenuela@gmail.com");
        model.addAttribute("status",true);
        model.addAttribute("ofertas",list);

        /* Finalizamos */
        return "ifThymeleaf/ifThymeleaf-v1";
    }
}
