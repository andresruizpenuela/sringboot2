package com.externalizar.thymelaf.multilenguaje.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    /*
     * http://localhost:8080/home-pag
     */
    @GetMapping("/home-page")
    public String getHomePage(){
        return "home-page";
    }

    /*
     * http://localhost:8080/home
     */
    @GetMapping("/home")
    public String getHome(){
        return "home";
    }
}
