package com.externalizar.thymelaf.multilenguaje.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class MessagesResolverConfiguration {

    /* MENSSAGES CONFIGURATION */
    @Bean
    @Description("Spring Messages Resolver")
    public ResourceBundleMessageSource messageSource() {
        //logger.info("[messageSource2] - "+Locale.getDefault());
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        //messageSource.setBasename("messages");
        messageSource.addBasenames("i18n/common/messages","i18n/nav/messages-nav");
        return messageSource;
    }
    /* END MENSSAGES CONFIGURATION */
}
