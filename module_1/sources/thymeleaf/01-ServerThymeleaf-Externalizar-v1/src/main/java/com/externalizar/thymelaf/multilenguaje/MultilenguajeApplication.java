package com.externalizar.thymelaf.multilenguaje;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultilenguajeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultilenguajeApplication.class, args);
    }

}
