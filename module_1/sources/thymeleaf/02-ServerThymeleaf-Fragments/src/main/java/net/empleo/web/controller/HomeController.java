package net.empleo.web.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.empleo.web.model.Vacante;
import net.empleo.web.service.IVacanteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;


@Controller
public class HomeController {
	/********INIT READ APPLICATION.PRORTIES ********/
	 @Value("${server.port}")
	 private String PORTAPP;
	/********END READ APPLICATION.PRORTIES ********/
	 
	 /******** INIT CREATE OBJET LOGGER ********/
	 private final String NAMECLASS = this.getClass().toString();
	 final Logger log = LoggerFactory.getLogger(this.getClass());
	 /******** END CREATE OBJET LOGGER ********/
	
	 
	 @Autowired
	 private IVacanteService vacanteService;
	 
	 String resultado = "";
	 @GetMapping("/")
	 public String getHomeView(Model model) {
		log.debug(NAMECLASS+": getHomeView - "+this.PORTAPP);
		
		/*
		String nombre = "Auxiliar de Contabilidad";
		Date fechaPub = new Date();
		double salario = 9000.0;
		boolean vigente = true;
			
		model.addAttribute("nombre", nombre);
		model.addAttribute("fecha", fechaPub);
		model.addAttribute("salario", salario);
		model.addAttribute("vigente", vigente);
		*/
		//model.addAttribute("vacantes", null);
		//System.out.println(vacanteService.buscarTodas());
		log.info(NAMECLASS +": "+vacanteService.buscarTodas().toString());
		List<Vacante> lista = vacanteService.buscarTodas();
		model.addAttribute("vacantes", lista);
		return "Home";
	 }
	 
	 @GetMapping("/tabla")
 	 public String mostrarTabla(Model model) {
		List<Vacante> lista = vacanteService.buscarTodas();
		model.addAttribute("vacantes", lista);
			
		return "Tabla";
	 }
	
	
	 
	  /** METODOS QUE DEVUELVEN JSON **/
	  @RequestMapping(value="/allOferts", method=RequestMethod.GET)
	  //@RequestMapping(value="/allOfertsJson", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<Object> getAllOferts(Model model) {
		log.info(NAMECLASS+": getHomeView - "+this.PORTAPP);
				//Mostramos lista por el debug
		vacanteService.buscarTodas().forEach(v->System.out.println(v.toString()));
		vacanteService.buscarTodas().forEach(v->log.info(v.toString()));

		//Debug
		Consumer<Vacante> debugConsumer = (v)->{
			log.info(NAMECLASS+": getAllOferts - "+v.toString());
			};
		vacanteService.buscarTodas().forEach( v -> debugConsumer.accept(v));
		
		//Deuvleve un String
		//return new ResponseEntity("This is a String", HttpStatus.OK);
		
		
		//Mostramos lista ordenada - Devulve un Array de Ofertas JSON automaticamente
		return new ResponseEntity(vacanteService.buscarTodas(), HttpStatus.OK);
	 }
	  
	  @RequestMapping(value="/allOfertsOrderDefault", method=RequestMethod.GET)
	  //@RequestMapping(value="/allOfertsJson", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<Object> getAllOfertsOrderDefaults(Model model) {
		log.info(NAMECLASS+": getHomeView - "+this.PORTAPP);
				//Mostramos lista por el debug
		vacanteService.buscarTodas().forEach(v->System.out.println(v.toString()));
		vacanteService.buscarTodas().forEach(v->log.info(v.toString()));
		
		//Debug
		Consumer<Vacante> debugConsumer = (v)->{
			log.info(NAMECLASS+": getAllOferts - "+v.toString());
			};
		vacanteService.buscarTodas().forEach( v -> debugConsumer.accept(v));
			
		//Cargamos las ofertas y las oferdenamos
		List<Vacante> vacantesOrderDefault = vacanteService.buscarTodas();
		Collections.sort(vacantesOrderDefault);
		
		//Mostramos lista ordenada - Devulve un Array de Ofertas JSON automaticamente
		return new ResponseEntity(vacantesOrderDefault, HttpStatus.OK);
	 }
	  
	  @RequestMapping(value="/allOfertsOrderDate", method=RequestMethod.GET)
	  //@RequestMapping(value="/allOfertsJson", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<Object> getAllOfertsOrderDate(Model model) {
		log.info(NAMECLASS+": getHomeView - "+this.PORTAPP);
				//Mostramos lista por el debug
		vacanteService.buscarTodas().forEach(v->System.out.println(v.toString()));
		vacanteService.buscarTodas().forEach(v->log.info(v.toString()));
		
		//Debug
		Consumer<Vacante> debugConsumer = (v)->{
			log.info(NAMECLASS+": getAllOferts - "+v.toString());
			};
		vacanteService.buscarTodas().forEach( v -> debugConsumer.accept(v));
			
		//Cargamos las ofertas y las oferdenamos
		List<Vacante> vacantesOrderDefault = vacanteService.buscarTodas();
		
		
		//Cargamos las ofertas y las oferdenamos por fecha
		List<Vacante> vacantesOrderDate = vacanteService.buscarTodas();
		
		
		// Modo imperativo
		Comparator<Vacante> byDate = new Comparator<Vacante>() {
			@Override
			public int compare(Vacante o1, Vacante o2) {
				return o1.getTime().compareTo(o2.getTime());
			}
		};
		Collections.sort(vacantesOrderDate, byDate);
		
		//Modo declarativo
		//Collections.sort(vacantesOrderDate, (x, y) -> x.getTime().compareTo(y.getTime()));
		
		//Mostramos lista ordenada - Devulve un Array de Ofertas JSON automaticamente
		return new ResponseEntity(vacantesOrderDate, HttpStatus.OK);
	 }
	 
}
