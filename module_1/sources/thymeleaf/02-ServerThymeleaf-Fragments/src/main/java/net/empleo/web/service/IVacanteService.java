package net.empleo.web.service;

import java.util.List;

import net.empleo.web.model.Vacante;

public interface IVacanteService {
	//Obtiene todas las vacantes diponibles
	List<Vacante> buscarTodas();
	//Busca la vacante dada un identificador
	Vacante buscarPorId(Integer idVacante);
	//Eño,oma ña vacamte dadp im identificador
	boolean borrarVacante(Integer idVacante);
}
