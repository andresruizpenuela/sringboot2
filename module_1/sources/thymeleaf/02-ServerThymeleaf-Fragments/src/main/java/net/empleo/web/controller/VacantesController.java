package net.empleo.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.ui.Model;

import net.empleo.web.model.Vacante;
import net.empleo.web.service.IVacanteService;


@Configuration
@RequestMapping("/vacantes")
public class VacantesController {

	/******** INIT CREATE OBJET LOGGER ********/
	 private final String NAMECLASS = this.getClass().toString();
	 final Logger log = LoggerFactory.getLogger(this.getClass());
	 /******** END CREATE OBJET LOGGER ********/
	 
	@Autowired
	IVacanteService daoVacantes;
	//http://localhost:9090/vacantes/delete?id=1
	@GetMapping("/delete")
	public String eliminar(@RequestParam("id") int idVacante, Model model) {
		log.info(NAMECLASS+"Se borra la oferta con id: "+idVacante);
		model.addAttribute("id",idVacante);
		daoVacantes.borrarVacante(idVacante);
		return "Mensajes";
	}
	
	//http://localhost:9090/vacantes/view/2
	 @GetMapping("view/{id}")
	 public String verDetalle(@PathVariable("id") int idVacnate, Model model) {
		 Vacante res = daoVacantes.buscarPorId(idVacnate);
		 if(res==null)
			 log.info(NAMECLASS+" "+null);
		 else
			 log.info(NAMECLASS+" "+res.toString());
		 
		 //model.addAttribute("vacante",null);
		 model.addAttribute("vacante",res);
		 return "Detalle";
	 }
	 
	 
	
}
