package net.empleo.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="categorias")
public class CategoriasController {
	
	private final String NAMECLASS = this.getClass().toString();
	final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value="/create",method = RequestMethod.GET)
	public String crear() {
		return "categorias/FormCategoria";
	}
	
	// @PostMapping("/save")
	@RequestMapping(value="/save",method = RequestMethod.POST)
	public String guardar(@RequestParam(value="nombre") String name, @RequestParam(value = "descripcion", required = false) String desc) {
		log.info(NAMECLASS+" [guardar] - name="+name+", description= "+desc);
		return "categorias/ListCategorias";
	}

}
