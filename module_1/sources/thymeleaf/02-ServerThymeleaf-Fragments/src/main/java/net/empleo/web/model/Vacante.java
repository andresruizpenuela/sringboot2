package net.empleo.web.model;

import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

public class Vacante implements Comparable<Vacante> {
	private Integer id;
	private String name;
	private String description;
	private Date time;
	private Double salario;
	private Integer importat;
	private String nameImage;
	
	public Vacante() {
		this.nameImage="no-image.png";
	}

	/** Getters and Setters **/
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public Integer getImportat() {
		return importat;
	}

	public void setImportat(Integer importat) {
		this.importat = importat;
	}

	public String getNameImage() {
		return nameImage;
	}

	public void setNameImage(String nameImage) {
		this.nameImage = nameImage;
	}

	@Override
	public int hashCode() {
		return Objects.hash(description, id, importat, name, nameImage, salario, time);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vacante other = (Vacante) obj;
		return Objects.equals(description, other.description) && Objects.equals(id, other.id)
				&& Objects.equals(importat, other.importat) && Objects.equals(name, other.name)
				&& Objects.equals(nameImage, other.nameImage) && Objects.equals(salario, other.salario)
				&& Objects.equals(time, other.time);
	}

	@Override
	public String toString() {
		return "Vacante [id=" + id + ", name=" + name + ", description=" + description + ", time=" + time + ", salario="
				+ salario + ", importat=" + importat + ", nameImage=" + nameImage + "]";
	}

	//Compara este objecto con otro objeto, para establecer una ordenación, comparacion por defecto de la clase
	//Objeto a comparar -> o
	//Objeto compardo -> this
	// -1 -> si El nombre de 'o' es mayor que el nombre de this o nombre iguales pero 'o' tiene mas tiemppo
	//  1 -> si El nombre de 'o' es menor que el nombre de this o nombre iguales pero 'o' tiene menos tiempo
	//  0 -> si Eln ombre y el teimpo de 'o' y 'this' son iguales 
	@Override
	public int compareTo(Vacante o) {
		int salida;
		//ComparteTo de la clase String
		int result = o.getName().compareToIgnoreCase(this.name);
		
		if(result==0) {
			//Mismo nombre, vemos la antiguedad de la oferta
			
			//ComparteTo de la clase Date
			result = o.getTime().compareTo(this.time);
			if(result>0) {
				//'o' es mas viejo
				salida = -1;
			}else if(result < 0 ) {
				//'o' mas joven
				salida = 1;
				}else {
					//Igual de viajoes
					salida = 0;
				}
			
		}else if(result > 0){
			//'o' es mayor que this
			salida = -1;
		} else {
			//'o' es menor que this
			salida = 1;
		}
		// TODO Auto-generated method stub
		
		return salida;
	}
}
