package net.empleo.web.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import net.empleo.web.model.Vacante;

@Service
public class VacanteImpService implements IVacanteService {
	
	/***************** INIT LOAD DATA *****************/
	List<Vacante> vacantes; //Default is null
	
	@PostConstruct
	public void loadData() {
		// Creamos la oferta de Trabajo 1.
		Vacante vacante1 = nuevaVacante(1, 								//Id 
			"Ingeniero Civil", 										//name
			"Solicitamos Ing. Civil para diseñar puente peatona",	//Description
			"08-02-2019",											//time
			14000.0, 												//salario
			1, 														//importat
			"logo1.png"												//nameImge
		);	
		
		// Creamos la oferta de Trabajo 2.
		Vacante vacante2 = nuevaVacante(2, 								//Id 
				"Contador Publico",										//name
				"Empresa importante solicita contador con 5 años de experiencia titulado.",	//Description
				"09-02-2019",											//time
				12000.0, 												//salario
				0, 														//importat
				"logo2.png"												//nameImge
			);

		// Creamos la oferta de Trabajo 3.
		Vacante vacante3 = nuevaVacante(3, 								//Id 
				"Ingeniero Eléctrico",										//name
				"Empresa internacional solicita Ingeniero mecánico para mantenimiento de la instalación eléctrica.",	//Description
				"10-02-2019",											//time
				10050.0, 												//salario
				0, 														//importat
				null											//nameImge
			);		

		// Creamos la oferta de Trabajo 4.
		Vacante vacante4 = nuevaVacante(4, 								//Id 
				"Diseñador Gráfico",										//name
				"Solicitamos Diseñador Gráfico titulado para diseñar publicidad de la empresa.",	//Description
				"11-02-2019",											//time
				7500.0, 												//salario
				1, 														//importat
				"logo3.png"												//nameImge
			);			
		vacantes= new ArrayList<Vacante>();
		vacantes.add(vacante1);
		vacantes.add(vacante2);
		vacantes.add(vacante3);
		vacantes.add(vacante4);
	}
	
	private Vacante nuevaVacante(int id,String name, String description,String time, double salario,int destacado,String nameImg) {
		Vacante vacante = new Vacante();
		vacante.setId(id);
		vacante.setName(name);
		vacante.setDescription(description);
		try {
			//Convierte un String a fehca
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			vacante.setTime(sdf.parse(time));
			//Date date = sdf.parse(time);
			//System.err.println("date- "+date+" sdf- "+sdf.format(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vacante.setTime(null);
		}
		vacante.setSalario(salario);
		vacante.setImportat(destacado);
		if(nameImg!=null) vacante.setNameImage(nameImg);
		
		return vacante;
	}
	/***************** END LOAD DATA *****************/

	
	@Override
	public List<Vacante> buscarTodas() {
		// TODO Auto-generated method stub
		return this.vacantes;
	}

	@Override
	public Vacante buscarPorId(Integer idVacante) {
		// TODO Auto-generated method stub
		Vacante v = vacantes.stream()
				.filter( vacante->vacante.getId()==idVacante).findAny().orElse(null); //Filtramos por id;
		return v;
	}

	/*
	 * @return true if any elements was removed
	 */
	@Override
	public boolean borrarVacante(Integer idVacante) {
		// TODO Auto-generated method stub
		return vacantes.removeIf(vacante->vacante.getId()==idVacante);
	}

}
