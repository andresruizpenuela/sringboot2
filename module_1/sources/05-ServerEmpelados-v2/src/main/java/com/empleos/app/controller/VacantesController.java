package com.empleos.app.controller;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.empleos.app.model.VacanteDto;
import com.empleos.app.service.IVacanteService;

@Controller
@RequestMapping("/vacantes")
public class VacantesController {
	
	@Autowired
	private IVacanteService serviceVacantes; //Inyección de dependencias
	
	@GetMapping("/view")
	public String verDetalleAll(Model model) {	
		
		List<VacanteDto> vacante = serviceVacantes.buscarTodas();
		
		System.out.println("Vacante: " + vacante);
		model.addAttribute("vacante", vacante);
		
		// Buscar los detalles de la vacante en la BD...		
		return "vacantes/lista";
	}
	
	// http://localhost:8080/vacantes/viewAvaility
	@GetMapping("/viewAvaility")
	public String verDisponibles(Model model) {	
		
		List<VacanteDto> vacantes = serviceVacantes.buscarTodas();
		//List<VacanteDto> vacantes = null;//para probar el caso en el que no exista vacantes en el sistema
		List<String> namesVacantes = null;
		if(vacantes!=null && !vacantes.isEmpty()) {
			namesVacantes = vacantes.stream().map(VacanteDto->VacanteDto.getNombre()).collect(Collectors.toList());
			System.out.println("namesVacantes: " + namesVacantes.toString());
		}
		
		
		
		model.addAttribute("namesVacantes", namesVacantes);
		
		// Buscar los detalles de la vacante en la BD...		
		return "vacantes/listado";
	}

	/* Muestra los datos de una vacante dada
	 * url: http://localhost:8080/vacantes/view/{id}
	 * param: 
	 * 	@id identificador de la vacante
	 * return:
	 *  @vacantedto datos de una vante
	 *  
	 */
	@GetMapping("/view/{id}") //
	public String verDetalle(@PathVariable("id") int idVacante, Model model) {	
		
		VacanteDto vacante = serviceVacantes.buscarPorId(idVacante);
		
		System.out.println("Vacante: " + vacante);
		model.addAttribute("vacante", vacante);
		
		// Buscar los detalles de la vacante en la BD...		
		return "vacantes/detalle";
	}
	
	
	/* Borra de la lista una vacante dada
	 * url: http://localhost:8080/vacantes/delete?id={idVacante}
	 * param: 
	 * 	@id identificador de la vacante
	 * return:
	 *  @string  mensaje con el id de la vantante y resutlado de la eiminacion
	 *  
	 */
	@GetMapping("/delete")
	public String deleteVacante(@RequestParam("id") int idVacante, Model model) {	
		
		if( serviceVacantes.borrarVacante(idVacante) ) {
			System.out.println("Vacante: " + idVacante + " eliminada");
			model.addAttribute("result", "Vacante: " + idVacante + " eliminada");
		}else {
			System.out.println("Vacante: " + idVacante + " no eliminada");
			model.addAttribute("result", "Vacante: " + idVacante + " no eliminada");
		}
		
		return "vacantes/delete";
	}

}
