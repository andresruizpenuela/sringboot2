package com.empleos.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/categorias")
public class CategoriasController {
	
	//http://localhost:8080/categorias/index
	// @GetMapping("/index")
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public String mostrarIndex(Model model) {
		return "categorias/listCategorias";
	}
	
	//http://localhost:8080/categorias/create
	// @GetMapping("/create")
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String crear() {
		return "categorias/formCategoria";
	}
	
	
	//DELTE: http://localhost:8080/categorias/delete
	// @DeleteMapping("/delete")
	@RequestMapping(value="/delete", method=RequestMethod.DELETE)
	public ModelAndView borrar(ModelMap model) {
		Boolean result = false;
		model.addAttribute("attribute", "Borrad: "+result);
		return new ModelAndView("forward:/categorias/index", model); //return "categorias/listCategorias";
	}
	
	//POST: http://localhost:8080/categorias/create
	// @PostMapping("/save")
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public String guardar(@RequestParam("nombre") String nombre,@RequestParam("descripcion") String descripcion) {
		System.out.println("Categoria:" + nombre);
		System.out.println("Descripcion: " + descripcion);
		
		return "categorias/listCategorias";
	}
}
