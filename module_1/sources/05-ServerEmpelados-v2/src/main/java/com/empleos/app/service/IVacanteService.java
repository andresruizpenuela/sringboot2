package com.empleos.app.service;

import java.util.List;
import com.empleos.app.model.VacanteDto;

public interface IVacanteService {
	List<VacanteDto> buscarTodas();
	VacanteDto buscarPorId(Integer idVacante);
	boolean borrarVacante(Integer idVacante);
	
}
