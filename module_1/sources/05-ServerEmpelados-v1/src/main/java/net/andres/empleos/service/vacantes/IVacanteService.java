package net.andres.empleos.service.vacantes;

import net.andres.empleos.entitity.Vacante;

import java.util.List;

public interface IVacanteService {

    List<Vacante> getLitVacantes();
    Vacante getVacanteFromID(int id);
    List<Vacante> deleteVacante(int id);
}
