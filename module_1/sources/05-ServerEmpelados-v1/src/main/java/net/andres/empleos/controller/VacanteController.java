package net.andres.empleos.controller;

import net.andres.empleos.entitity.Vacante;
import net.andres.empleos.service.vacantes.IVacanteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping(value="vacantes")
public class VacanteController {
    @Autowired
    IVacanteService vservice;

    @RequestMapping(value="allVacantes",method = RequestMethod.GET)
    public String allVacantes(Model model){
        List<Vacante> lista = vservice.getLitVacantes();
        model.addAttribute("ofertas", lista);
        return "vacantes-view/showVacantes";
    }

    @RequestMapping(value="getVacante/{id}",method = RequestMethod.GET)
    public String getVacnate(@PathVariable("id") int id, Model model){
        List<Vacante> lista = vservice.getLitVacantes();
        Vacante vacante = vservice.getVacanteFromID(id);

        model.addAttribute("ofertas", lista);
        model.addAttribute("oferta",vacante);
        return "vacantes-view/showVacantes";
    }

    @RequestMapping(value="deleteVacante/{id}",method = RequestMethod.GET)
    public String deleteVacnate(@PathVariable("id") int id, Model model){
        vservice.deleteVacante(id);
        List<Vacante> lista = vservice.getLitVacantes();
        model.addAttribute("ofertas", lista);
        return "vacantes-view/showVacantes";
    }
}
