package net.andres.empleos.service.vacantes;

import net.andres.empleos.entitity.Vacante;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class VacanteServiceImpl implements IVacanteService{
    private Logger logger = LoggerFactory.getLogger(VacanteServiceImpl.class);
    List<Vacante> listVacantes;

    @PostConstruct
    public void init(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        listVacantes = new LinkedList<Vacante>();
        try {
            // Creamos la oferta de Trabajo 1.
            Vacante vacante1 = new Vacante();
            vacante1.setId(1);
            vacante1.setNombre("Ingeniero Civil"); // Titulo de la vacante
            vacante1.setDescripcion("Solicitamos Ing. Civil para diseñar puente peatonal.");
            vacante1.setFecha(sdf.parse("08-02-2019"));
            vacante1.setSalario(8500.0);
            vacante1.setDestacado(1);
            vacante1.setImagen("empresa1.png");

            // Creamos la oferta de Trabajo 2.
            Vacante vacante2 = new Vacante();
            vacante2.setId(2);
            vacante2.setNombre("Contador Publico");
            vacante2.setDescripcion("Empresa importante solicita contador con 5 años de experiencia titulado.");
            vacante2.setFecha(sdf.parse("09-02-2019"));
            vacante2.setSalario(12000.0);
            vacante2.setDestacado(0);
            vacante2.setImagen("empresa2.png");

            // Creamos la oferta de Trabajo 3.
            Vacante vacante3 = new Vacante();
            vacante3.setId(3);
            vacante3.setNombre("Ingeniero Eléctrico");
            vacante3.setDescripcion("Empresa internacional solicita Ingeniero mecánico para mantenimiento de la instalación eléctrica.");
            vacante3.setFecha(sdf.parse("10-02-2019"));
            vacante3.setSalario(10500.0);
            vacante3.setDestacado(0);

            // Creamos la oferta de Trabajo 4.
            Vacante vacante4 = new Vacante();
            vacante4.setId(4);
            vacante4.setNombre("Diseñador Gráfico");
            vacante4.setDescripcion("Solicitamos Diseñador Gráfico titulado para diseñar publicidad de la empresa.");
            vacante4.setFecha(sdf.parse("11-02-2019"));
            vacante4.setSalario(7500.0);
            vacante4.setDestacado(1);
            vacante4.setImagen("empresa3.png");

            /**
             * Agregamos los 4 objetos de tipo Vacante a la lista ...
             */
            listVacantes.add(vacante1);
            listVacantes.add(vacante2);
            listVacantes.add(vacante3);
            listVacantes.add(vacante4);

        } catch (ParseException e) {
            //System.out.println("Error: " + e.getMessage());
            logger.error("[getVacantes] Error: " + e.getMessage());
        }
    }

    @Override
    public List<Vacante> getLitVacantes() {
        return listVacantes;
    }

    @Override
    public Vacante getVacanteFromID(int id) {

        return listVacantes.stream().filter(v->v.getId()==id).findFirst().orElse(null);
    }

    @Override
    public List<Vacante> deleteVacante(int id){
        listVacantes.removeIf(v->v.getId()==id);
        return listVacantes;
    }
}
