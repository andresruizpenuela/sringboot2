# Sring Boot 2 & Spring MVC

_Curso de Spring Boot 2 & Spring MVC-Desarrollo web profesional (2021)_

![Spring vs Spring Boot vs Spring MVC](.\img\module_0\img1.jpg)

[TOC]

## Abstract

<div style="text-align: justify">
<p>
    <strong>Spring Boot</strong> es un sub-proyecto de Spring Framework que busca facilitar la creaicón de poryectos web con <b>Spring Framework</b> ya que elimina la necesidad de crear largos y compljeos archivos de configuración XML.<br>Las características principales de <b>Spring Boot</b> son:
    <ul>
        <li>Provee <b>configuraciones por defecto</b> para la mayoría de las tenologías usadas (<i>Spring MVC, Spring Data JPA &amp; Hibernate, Sping Rest</i>), lo que ya no es necesario realizar esas configuraicones tediosas y propensas a errores.</li>
        <li>Facilia la administración de todas las dependencias (<i>archivos JAR y versiones compatibles</i>)</li>
        <li>Proporciona un modelo de propagación parecido a las aplicaciones java tradicionales que se inician en el método main.</li>
    </ul>
    Anteriormente, el proceso típicio para desarrollar una aplicación de Spring:
    <ol>
        <li value="1">Seleccionar Dependencias necesarias con Maven (<i>deben ser compatibles</i>). </li>
        <li>Crear nuestra aplicación.</li>
        <li> Realizar el Deployment en el servidor.</li>
    </ol>
</p>
<p style="color:#FF0000;">
    <strong>Spring Boot</strong> nace con la intencion de simplificar los pasos 1 y 3, para poder centrarnos en el desarrollo de nuestra aplicación.
</p>
<p style="color:green;">
   ¿Cómo se simplifican el paso 1 y 3?
</p>
<p>
    <ul>
        <li>Permite crear apliaicones <b>Stand-Alone</b> con Spring</li>
        <ul>
            <li>Una aplicación <b>Stand-Alone</b>, es una aplicación independiente, la cual, no requiere un servidor Web.</li>
            <li>Este tipo de apliaiocnes corre desde la línea de comandos (<i>cmd, shell,...</i>), y por tanto necesita tener una aplicación "main": <code>$ java -jar mywebapp.jar</code></li>
        </ul>
        <li>Inluye un servidor web Apaceh Tomcat Embebido (<i>se puede cambiar por "Jetty" o "Undertow"</i>)</li>
        <ul>
            <li>No necesita realizar el Deployment de archivos WAR.</li>
        </ul>
        <li>Se rquiere una configuraicón minima debiado a:</li>
        <ul>
            <li>No es necesaroi más archivos XML</li>
            <li>Las configuraciones para la mayoría de las tecnologías ya se incluye con valores or defecto en base a los parámetros y valores más usadso por la mayoría de los usuarios que usan Sring.</li>
        </ul>
        <li>Incluye características listas para entornos de producción:</li>
        <ul>
        	<li>Revisión de funcionalidad.</li>
            <li>Métricas de la aplicación.</li>
        </ul>
	</ul>
</p>
</div>



## Estructura del curso

<div style="text-align: justify">
<p>
    El curso estará difivido en 5 módulos:
    <ul>
        <li> <b style="color:red">Módulo 1:</b> Desarrollar aplicaciones web con <b>Spring MVC y Thymeleaf.</b></li>
        <li> <b style="color:red">Módulo 2:</b> Persitencia de Datos con <b>Spring Data y JPA.</b></li>
        <li> <b style="color:red">Módulo 3:</b> Ingegración de <b>Spring MVC y Spring Data JPA</b> en una aplicación web.</li>
        <li> <b style="color:red">Módulo 4:</b> <b>Spring Security</b> (<i>seguridad basada en usuarios y roles</i>)</li>
        <li> <b style="color:red">Módulo 5:</b> Desarrollar <b>Restful Web Services</b> con <b>Spring Boot</b>.</li>
    </ul>
    Además, se creara una pagina web completamente web funcional para publicar ofertas de trabajo por Internet, donde el administrador de la web crean ofertas y un usuario se registra.
</p>
</div>




