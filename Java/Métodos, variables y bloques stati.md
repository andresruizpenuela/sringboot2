# Métodos, variables y bloques static en Java con ejemplos

Hola que tal, bienvenido a este nuevo artículo en donde aprenderás que son los **Métodos, variables y bloques static en Java,** así mismo realizaré algunos ejemplos con la finalidad que se entiendan de mejor manera.

Que cosas pueden ser static en Java?, básicamente la palabra **static** se puede aplicar a clases internas, a métodos, a variables y a bloques de  código, a continuación voy ha describir que significa cada uno y como  implementarlo con ejemplos. 

Empecemos primero con las variables y los métodos static.

### Artículos relacionados: [Cómo integrar Bootstrap con JSP y crear un proyecto Web en Java 8](https://www.ecodeup.com/como-integrar-bootstrap-con-jsp-y-crear-un-proyecto-web-en-java8/)

## ¿Qué es una variable static en Java?

Una  variable estática (static) en Java es una variable que pertenece a la  clase en que fue declarada y se inicializa solo una vez al inicio de la  ejecución del programa, la característica principal de este tipo de  variables es que se puede acceder directamente con el nombre de la clase sin necesidad de crear un objeto, a continuación otros detalles que  tiene una variable static en Java.

- Es una variable que pertenece a la clase y no al objeto.
- Las  variables static se inicializan solo una vez, al inicio de la ejecución. Estas variables se inicializarán primero, antes de la inicialización de cualquier variable de instancia.

## ¿Qué es un Método static en Java?

Un método **static** en Java es un método que pertenece a la clase y no al objeto. Un  método static solo puede acceder a variables o tipos de datos declarados como static.

- Un método static sólo puede acceder a datos static. No puede acceder a datos no static (variables de instancia).
- Un método static puede llamar solo a otros métodos static y no puede invocar un método no static a partir de él.
- Un  método static se puede acceder directamente por el nombre de la clase y  no se necesita crear un objeto para acceder al método (aunque se puede  hacerlo).
- Un método static no puede hacer referencia a “this” o “super”.

## Ejemplos de como crear **Métodos, variables y bloques static en Java**

Ahora vamos a crear un proyecto en Eclipse con la siguiente estructura:

![img](https://www.ecodeup.com/wp-content/uploads/2018/01/Proyecto-metodos-variables-bloques-static.jpg)

Y luego copiar el código que se muestra abajo en la clase Ejecutor:

```java
package com.ecodeup.estatic;
 
public class Ejecutor {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Estudiante e1 = new Estudiante();
		e1.mostrarInfo();
		Estudiante e2 = new Estudiante();
		e2.mostrarInfo();
		// accediendo a la variable b static de la clase creada
		e1.b++; // incrementa en 1
		// Accediendo directamente desde la clase, sin instanciar un objeto
		Estudiante.b++; // incrementa en 1
		// como se puede ver la variable b static es compartida por todas las
		// instancia de la clase
		e2.mostrarInfo();
	}
}
 
class Estudiante {
	int a; // se inicializa en cero por cada objeto creado
	static int b; // se inicializa en cero cuando la clase es cargada y no por
					// cada objeto creado
 
	Estudiante() {
		// Constructor que incremente en 1 la variable static b
		b++;
	}
 
	public void mostrarInfo() {
		System.out.println("Valor de a = " + a);
		System.out.println("Valor de b = " + b);
	}
	// en un método static solo puede declar o llamar variables static
	// se puede descomentar para ver el error
	// public static void incrementar(){
	// a++;
	// }
}
```

Si ejecutamos el proyecto podemos ver como la variable **b** es compartida por todas las instancias de la clase **Estudiante**, es por eso que en la línea 16 al acceder al método **mostrarInfo()** con el objeto e2, la variable **b** almacena el incremento tanto de la línea 11 que la ejecuta el objeto e1, como de la línea 13 que se la realiza accediendo desde la misma clase.

También  si descomentamos las líneas 36, 37, 38 se puede ver que desde un método  static no se puede llamar a una variable de instancia y que en el mismo  IDE nos marca un error.

## Bloques static en Java

El  bloque static es un bloque de instrucción dentro de una clase Java que  se ejecuta cuando una clase se carga por primera vez en la JVM.  Básicamente un bloque  static inicializa variables de tipo static dentro de una clase, al igual que los constructores ayudan a inicializar las variables de instancia,  un bloque static inicializa las variables de tipo static en una clase.

Para ver un ejemplo de como crear un bloque static vamos a crear una nueva clase con el nombre de Bloque.java y copiar el siguiente código.

```java
package com.ecodeup.estatic;

public class Bloque {
	static int a;
	static int b;
	static {
		a = 10;
		b = 20;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Valor de a = " + a);
		System.out.println("Valor de b = " + b);
	}
}
```

Como  podemos ver en este caso las variables static a y b se inicializan  automáticamente puesto que el bloque static junto con las variables  static **es lo primero que carga la clase cuando se ejecuta**. 

Finalmente es importante recordar que en un bloque static sólo se puede  inicializar variables de tipo static no variables de instancia.

Espero que este artículo haya sido de ayuda, cualquier inquietud me la puedes dejar en los comentarios.

## Conclusión

* Cuando tenemos variables de tipo static estás son compartidas  por todos los objetos de esa clase. Por lo genera este tipo de clases no se instancian objetos si no más bien se usa de la forma Clase.variable.
* Es cierto que una variable static pertenece a la clase, una instancia si puede acceder a ella. Por lo que en ambos casos es válido:  Clase.variablestatic, o instancia.variablestatic.