import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;



public class App {
    List<Vacante> vacantes; //Default is null
    
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World! This code is a example to use stream");

        Vacante v1 = new Vacante("Ingeniero técncio",12500.0,0);
        Vacante v2 = new Vacante("Diseñador gráfico",21000.0,1);
        Vacante v3 = new Vacante("Arquitecto",7500.0,0);

        System.out.println("v1= "+v1.toString());
        System.out.println("v2= "+v2.toString());
        System.out.println("v3= "+v3.toString());

        //Creamos una lista
        List<Vacante> vacantes = Arrays.asList(v1,v2,v3);

        //Recorremos la lista
        System.out.println("""
            ************************
            RECPRRER UNA LISTA
            ************************
            """);
            //Modo tradicional - for
        System.out.println("Modo tradicional - for");
        for(int i=0;i<vacantes.size();i++){
            System.out.println(vacantes.get(i));
        }
            //Modo tradicional - forEach
        System.out.println("Modo tradicional - forEach");
        for(Vacante vacante : vacantes){
            System.out.println(vacante.getName());
        }
        
            //Usando expresiones Lambda
        System.out.println("Usando lambada - forEach");
        vacantes.forEach(vacante->{
            System.out.println(vacante.getName());
        });
        
        System.out.println("Usando lambada sobre unal ista de enteros direcamente- forEach");
        Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).forEach(
				System.out::println);

        //Recorremos la lista
        System.out.println("""
            ************************
            Buscar los salarios mayors de 10000
            ************************
            """);
        List<Vacante> vacantesMayores10K = vacantes.stream().filter(vacante->
                vacante.getSalario()>1000.0
            ).collect( Collectors.toList());

        vacantesMayores10K.forEach(
            v->{
                System.out.println(v.getName());
            }
        );
    }
}
