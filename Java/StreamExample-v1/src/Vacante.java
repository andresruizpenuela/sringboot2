
import java.util.Date;
import java.util.Objects;

public class Vacante {
	private static int cont =-1; //contador, atributo de clase
	private Integer id =0; //autoincremto
	private String name;
	private String description;
	private Date time;
	private Double salario;
	private Integer importat;
	private String nameImage;
	
	public Vacante() {
        this.id = ++cont; //id auto-increment
		this.nameImage="no-image.png";
	}

	public Vacante(String name,double salario,int importat) {
        this.id = ++cont; //id auto-increment
		this.name = name;
		this.salario = salario;
		this.importat = importat;
		this.nameImage="no-image.png";
	}

	/** Getters and Setters **/
	public void setId(int id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public Integer getImportat() {
		return importat;
	}

	public void setImportat(Integer importat) {
		this.importat = importat;
	}

	public String getNameImage() {
		return nameImage;
	}

	public void setNameImage(String nameImage) {
		this.nameImage = nameImage;
	}

	@Override
	public int hashCode() {
		return Objects.hash(description, id, importat, name, nameImage, salario, time);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vacante other = (Vacante) obj;
		return Objects.equals(description, other.description) && Objects.equals(id, other.id)
				&& Objects.equals(importat, other.importat) && Objects.equals(name, other.name)
				&& Objects.equals(nameImage, other.nameImage) && Objects.equals(salario, other.salario)
				&& Objects.equals(time, other.time);
	}

	@Override
	public String toString() {
		return "Vacante [id=" + id + ", name=" + name + ", description=" + description + ", time=" + time + ", salario="
				+ salario + ", importat=" + importat + ", nameImage=" + nameImage + "]";
	}
}

