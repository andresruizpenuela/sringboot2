package ejemplo02;
/**
 * Vamos a ver un ejemplo donde nos vamos a encontrar con la principal desventaja de las lambdas, y es que solo nos funcionará si
 * tenemos como mínimo:
 *  1º Un método sin defaul default.
 *  2º El resto de metodos deberán estar 
 * De lo contrario el compilador no identificrá que metodoe de la interfaz esta siendo invocada.
 * 
 * Si queremos realizar el mismo ejercicio de la calculadora pero, esta vez, con múltiples interfaces 
 * (suma, resta, división…).
 * 
 * Main01 -> Manera clasica
 * Main02 -> Manera con lambda
 */
public class Main02 {
	interface Calc {
		default int suma(int a, int b){return a + b; };
		int resta(int a, int b);
		default int multiplicacion(int a, int b){return a * b; };
		default int division(int a, int b){return (b!=0)?a / b:0; };
	}

	public static void main(String[] args) {
		Calc calc = (a, b) -> {return a+b;};

		System.out.println(calc.suma(1,3));
        System.out.println(calc.resta(1,3));
        System.out.println(calc.division(1,3));
        System.out.println(calc.multiplicacion(1,3));

	}
}