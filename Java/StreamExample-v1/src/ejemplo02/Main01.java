package ejemplo02;
/**
 * Vamos a ver un ejemplo donde nos vamos a encontrar con la principal desventaja de las lambdas, y es que solo nos funcionará si
 * tenemos como mínimo:
 *  1º Un método default.
 *  2º Un método con cuerpto de retorno.
 * De lo contrario el compilador no identificrá que metodoe de la interfaz esta siendo invocada
 * 
 * Si queremos realizar el mismo ejercicio de la calculadora pero, esta vez, con múltiples interfaces 
 * (suma, resta, división…).
 * 
 * Main01 -> Manera clasica
 * Main02 -> Manera con lambda
 */
public class Main01 {
	interface Calc {
		void suma(int a, int b);
		void resta(int a, int b);
		void multiplicacion(int a, int b);
		void division(int a, int b);
	}

	public static void main(String[] args) {
		Calc calc = new Calc() {
			@Override
			public void suma(int a, int b) {
				System.out.println("Suma:");
				System.out.println("\tResult: " + (a+b));
			}

			@Override
			public void resta(int a, int b) {
				System.out.println("Resta:");
				System.out.println("\tResult: " + (a-b));
			}

			@Override
			public void multiplicacion(int a, int b) {
				System.out.println("Multiplicacion:");
				System.out.println("\tResult: " + (a*b));
			}

			@Override
			public void division(int a, int b) {
				System.out.println("Division:");
				if(b!=0) {
					System.out.println("\tResult: " + (a/b));
				}else {
					System.err.println("\tError!");
				}
			}
		};
		calc.suma(1,3);
		calc.resta(1,3);
		calc.division(1,3);
		calc.multiplicacion(1,3);
	}
}