package ejemplo00.colecciones;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
/*
* SET -> Coleecion de elememtnos 
*/
public class SetUse {
    
    public static void main(String[] args){

        //
        Cliente cl1 = new Cliente("Andres", "00001",20000);
        Cliente cl2 = new Cliente("Antonio Bandres", "00001",1000);
        Cliente cl3 = new Cliente("Ramon", "00001",50000);
        Cliente cl4 = new Cliente("Baltasar", "00001",32100.50);
        Cliente cl5 = new Cliente("Andres", "00001",20000); //C5 esta repetido en cl1


        //Para escoger la mejor colección que mejor se adapta a nuestgro progrmad debemos responder las siguientes pergutnas:
        // 1º ¿Puede existir elementos repetidos?
        // 2º ¿Se va ha borrar y escribir muchas veces ?
        // 3º ¿Se va ha leer muchas veces?
        
        //En ese caso usamos usamos hAshSet -> Coleecion no ordenada no repetiada, eficinete y de acceso no aleatorio
        //implmentacion de una interfaz sobre una clase //Poliformismo y principio de sustitución
        Set<Cliente> cuentas = new  HashSet<Cliente>();

        cuentas.add(cl1);
        cuentas.add(cl2);
        cuentas.add(cl1); //Repetido, no lo agrega por que el mismo objeto y por tanto equal sera true aunque este no este redefino
        
        // Nota:
        // La importancion del Equals y HasCode en las clases propias.
        // Equals -> Permite comprobar sis dos objetos son iguales a partir del hascode y de las caracterisitcas de la clase
        // HasCode -> Nos indica la posición en memoria de un objeto.
        // Por defecto es un metodo herado de la clase Objet y por tanto debe ser rescrito en todas las clases propias sobre
        // todo cuadno las almacenmos en colecciones, ya que
            //Si el metodo equals y hashCode no esta redefinido en nuestras clases propias
            //pues el método equals que tira por defecto es del objecto tipo Object y este no conece las caracterísitcas de nuestas
            //clases, dira que son distintos siempre y ucando se compare objetos diferente
            //Es decir, si no redefinimos los métodos "hasCode" y "equals":
            //         cl1.equals(cl1) -> true mismo objeto
            //         cL1 = cl5 -> cl1.equals(cl5) -> false porque son objetos disitnos y el hasCode es difernte,  por eso cl5.equal(cl1) da true [mismo objeto]
            
        cuentas.add(cl5); //Repetido con cl1, no deberia ser agregado porque aunque son dos objetos difentes tiene el mismo valor que cl1, 
                          //pero si el equals no esta redifino en la clase propia, comparar con el equals de la clase Object, la cual, no 
                          //conoce las caracterisitcas como nombre, salario, ... de nuestra clase y por tatno dara falso.
                          //cl5.equals(c1)  dara false si no redefinios el equasl
                          
        System.out.println("cl5 equals cl1 -"+cl5.equals(cl1));
        
        //Comporbar si contiente un elemento
        System.out.println("Continete el elemando andres? "+cuentas.contains("abdres"));
        System.out.println("Continete el elemando cl1? "+cuentas.contains(cl1));

        //Recorrer un elemento
        System.out.println("*******\nIterate\n******");
        //Tradicional  - Iterador
        Iterator<Cliente> it = cuentas.iterator(); //Necesario para recorrer
        while(it.hasNext()){
            System.out.println(it.next());
            System.out.println(it.next().getNombre());
        }

        System.out.println("*******\nForEach\n******");
        //Tradicional - FOrEch
        for (Cliente cliente : cuentas) {
            System.out.println(cliente.toString());
        }

        System.out.println("*******\nLambda\n******");
        //Lambda
        cuentas.forEach(c->System.out.println(c.toString()));

        //Eliminar un elmento
        System.out.println("*******\nEliminamos el cliente Andres\n******");
        //Da este error: java.util.ConcurrentModificationException >> Con forEach no podmeos eliminar un elmento mientras se recorre
        //la opicon mejor usar iterador
        try{
            for (Cliente cliente : cuentas) {
                if(cliente.getNombre().equals("Andres")){
                    cuentas.remove(cliente);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            //Iterator Permite borrar un elemento
            Iterator<Cliente> it2 = cuentas.iterator(); //Necesario para recorrer
            while(it.hasNext()){
                String nombre = it2.next().getNombre();
                if(nombre.equals("Andres")){
                    it2.remove();
                }
            }
        }
        
        //Visualizamos
        cuentas.forEach(c->System.out.println(c.toString()));

    }
}
