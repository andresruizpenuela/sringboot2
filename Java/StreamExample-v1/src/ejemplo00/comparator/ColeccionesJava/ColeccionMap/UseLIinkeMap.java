package ejemplo00.comparator.ColeccionesJava.ColeccionMap;

import java.util.LinkedHashMap;
import java.util.Map;

public class UseLIinkeMap {

    public static void main(String[] args) {
        
        Map<Integer,Oferta> nombres = new LinkedHashMap<Integer,Oferta>();

        nombres.put(1, new Oferta("Andres"));
        nombres.put(3, new Oferta("Zan"));
        nombres.put(2, new Oferta("Pedro"));

        System.out.println(nombres.toString());

        //Obtneemos los key en un Set
        System.out.println(nombres.keySet());

        //Convierte el map en un Set
        System.out.println(nombres.entrySet());
        
    }
    
}


class Oferta implements Comparable<Oferta>{
    private String nombre;
    public Oferta(String n){
        this.nombre = n;
    }
    

    @Override
    public String toString() {
        return "Oferta [nombre=" + nombre + "]";
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Oferta other = (Oferta) obj;
        if (nombre == null) {
            if (other.nombre != null)
                return false;
        } else if (!nombre.equals(other.nombre))
            return false;
        return true;
    }


    /**
     * @return String return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Override
    public int compareTo(Oferta o) {
        // TODO Auto-generated method stub
        return this.nombre.compareTo(o.getNombre());
    }

}