package ejemplo00.comparator.ColeccionesJava.ColeccionMap;

import java.util.*;

public class UseHashTable {
 
    public static void main(String[] args) {

        System.out.println("HasTable elementos - ");
        Map<String,Object> ht = new Hashtable<>();
        
        ht.put("nombre", "Andres");
        ht.put("potencia", "220");
        ht.put("edad", "29");

        System.out.println("HasTable elementos - ");
        System.out.println(ht.toString());
        
        //Recorremos los elmentos meidnate un enumeracion
        Enumeration enumeration = ((Hashtable<String, Object>) ht).elements();
        while (enumeration.hasMoreElements()) {
            System.out.println(""+"hashtable valores: " + enumeration.nextElement());
        }

        //Si queremos saber cuales son las claves de la Hashtable Java usamos el método .keys().
        Enumeration llaves = ((Hashtable<String, Object>) ht).keys();
        while (llaves.hasMoreElements()) {
        System.out.println(""+"hashtable llaves: " + llaves.nextElement());
        }
    }
}
