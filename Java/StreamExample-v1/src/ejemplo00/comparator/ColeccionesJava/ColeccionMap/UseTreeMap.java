package ejemplo00.comparator.ColeccionesJava.ColeccionMap;

import java.util.Map;
import java.util.TreeMap;

public class UseTreeMap {
    
    public static void main(String[] args) {
        Map<String, Oferta> ofertas = new TreeMap<String,Oferta>();

        ofertas.put("01", new Oferta("Maracrrores 50%"));
        ofertas.put("03", new Oferta("Fideos de Sopa 20%"));
        ofertas.put("02", new Oferta("Solomillo de Cerdo 15%"));

        System.out.println(ofertas.toString());
    }
}

class Oferta{
    private String nombre;
    public Oferta(String n){
        this.nombre = n;
    }
    

    @Override
    public String toString() {
        return "Oferta [nombre=" + nombre + "]";
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Oferta other = (Oferta) obj;
        if (nombre == null) {
            if (other.nombre != null)
                return false;
        } else if (!nombre.equals(other.nombre))
            return false;
        return true;
    }


    /**
     * @return String return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}