package ejemplo00.comparator.ColeccionesJava.ColeccionMap;

import java.util.HashMap;
import java.util.Map;

public class UseHashMap {

    public static void main(String[] args) {
        Map<String,Object> hasMap = new HashMap<String, Object>();
       
        hasMap.put("nombre", "Andres");
        hasMap.put("Potencia", "220");
        hasMap.put("edad", "19");

        System.out.println("Coleccion de datos [clave=valor] creada");
        System.out.println(hasMap.toString());

        System.out.println("Coleccion de datos [clave=valor] Clave repetida - Solapa el valor de 'nombre'");
        hasMap.put("nombre", "Andres 2");
        System.out.println(hasMap.toString());
    }
    
}
