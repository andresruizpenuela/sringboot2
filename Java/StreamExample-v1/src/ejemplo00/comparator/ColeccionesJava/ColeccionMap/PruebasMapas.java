package ejemplo00.comparator.ColeccionesJava.ColeccionMap;

import java.util.HashMap;
import java.util.Map;

public class PruebasMapas {
   
    public static void main(String[] args) {
        
        Map<String,Oferta> ofertas = new HashMap<String,Oferta>();

        ofertas.put("001", new Oferta("Oferta 1"));
        ofertas.put("003", new Oferta("Oferta 3"));
        ofertas.put("002", new Oferta("Oferta 2"));

        System.out.println("ofertas= \n"+ofertas.toString());

        //Sustituir oferta 3 
        ofertas.put("003", new Oferta("Oferta 3 Mejorada"));
        System.out.println("ofertas mejoradas= \n"+ofertas.toString());

        //Eliminar oferta 2
        ofertas.remove("002");
        System.out.println("ofertas sin oferta 2= \n"+ofertas.toString());


        //Set- Entry: Mostrmoas la coelicón Set de Ofertas
        System.out.println("Coleccion Set: "+ofertas.entrySet());
        
        //Recorremos la coleccion con un ForEach
        for(Map.Entry<String,Oferta> entrada: ofertas.entrySet()){
            System.out.println("Key: "+entrada.getKey()+", Valor: "+entrada.getValue());
        }

    }
}

class Oferta{
    private String nombre;
    public Oferta(String n){
        this.nombre = n;
    }
    

    @Override
    public String toString() {
        return "Oferta [nombre=" + nombre + "]";
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Oferta other = (Oferta) obj;
        if (nombre == null) {
            if (other.nombre != null)
                return false;
        } else if (!nombre.equals(other.nombre))
            return false;
        return true;
    }


    /**
     * @return String return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
