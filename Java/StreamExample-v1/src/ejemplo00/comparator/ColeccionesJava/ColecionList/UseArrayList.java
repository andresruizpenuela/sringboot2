package ejemplo00.comparator.ColeccionesJava.ColecionList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UseArrayList {
    
    public static void main(String[] args) {
        
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("a");
        arrayList.add("i");
        arrayList.add("e");
        arrayList.add("u");
        arrayList.add("o");
        

        System.out.println("ArrayList");
        arrayList.forEach(System.out::println);

    
        System.out.println("ArrayList ordenado");
        Collections.sort(arrayList);
        System.out.println(arrayList.getClass());
        arrayList.forEach(System.out::println);
    }
}
