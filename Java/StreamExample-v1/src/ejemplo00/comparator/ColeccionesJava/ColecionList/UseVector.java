package ejemplo00.comparator.ColeccionesJava.ColecionList;

import java.util.List;
import java.util.Vector;

public class UseVector {
    public static void main(String[] args) {
        //List<int> v = new Vector<int>(); //No se permite datos primitivos coom elmento de una colección, auqn este si puede formar parte de un objeto coche.edad donde edad es un entero
        List<Integer> vector = new Vector<Integer>();
        //NO conitente el elemtno elementAt

        vector.add(1);
        vector.add(333);
        vector.add(7);
        vector.add(91);

        System.out.println("Elmentos de la lista Vector");
        vector.forEach(System.out::println);

        Vector<Integer> v = new Vector<Integer>();
        v.add(1);
        v.add(333);
        v.add(7);
        v.add(91);

        System.out.println("Elmentos del  Vector");
        v.forEach(System.out::println);
        System.out.println("Elemento 1: "+v.elementAt(1));
        System.out.println("Elemento 333: "+v.elementAt(0));
    }
}
