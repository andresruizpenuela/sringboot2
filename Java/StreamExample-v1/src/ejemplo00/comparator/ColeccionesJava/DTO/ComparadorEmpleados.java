package ejemplo00.comparator.ColeccionesJava.DTO;

import java.util.Comparator;

public class ComparadorEmpleados implements Comparator<Empleado> {

    @Override
    public int compare(Empleado o1, Empleado o2) {
        // Compara por nombre
        return o1.getName().compareTo(o2.getName());
    }
    
}
