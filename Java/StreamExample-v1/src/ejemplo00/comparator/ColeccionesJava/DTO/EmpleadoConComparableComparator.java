package ejemplo00.comparator.ColeccionesJava.DTO;

import java.util.Comparator;

public class EmpleadoConComparableComparator implements Comparable<EmpleadoConComparableComparator>, Comparator<EmpleadoConComparableComparator> {
    private String name;
    private String surname;
    private static int count = -1;
    public int id;

    public EmpleadoConComparableComparator(){}; //Neceario para creaar un objeto de refencia;

    public EmpleadoConComparableComparator(String name, String surname){
        this.name = name;
        this.surname = surname;
        this.id = this.count++;
    }


    @Override
    public String toString() {
        return "Empleado [id=" + id + ", name=" + name + ", surname=" + surname + "]";
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }




    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (obj instanceof EmpleadoConComparableComparator)
            return false;
            EmpleadoConComparableComparator other = (EmpleadoConComparableComparator) obj;
        if (id != other.id)
            return false;
        return true;
    }

    /**
     * @return String return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return String return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return int return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }


    
    @Override
    public int compareTo(EmpleadoConComparableComparator o) {
        //Compara por nombre
        return this.name.compareTo(o.getName());
    }


    @Override
    public int compare(EmpleadoConComparableComparator o1, EmpleadoConComparableComparator o2) {
        // Compara por subnome
        return o1.getSurname().compareTo(o2.getSurname());
    }

}
