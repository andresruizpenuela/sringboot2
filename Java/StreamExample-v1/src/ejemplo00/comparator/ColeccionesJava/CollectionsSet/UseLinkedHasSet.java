package ejemplo00.comparator.ColeccionesJava.CollectionsSet;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;



public class UseLinkedHasSet {
    
    public static void main(String[] args) {
        
        Set<Integer> linkedHashSet = new LinkedHashSet<Integer>();

        linkedHashSet.add(20);
        linkedHashSet.add(30);
        linkedHashSet.add(10);
        linkedHashSet.add(50);
        linkedHashSet.add(40);

        System.out.println("Elements in the Set after add method: " + linkedHashSet);

        //Añadir una colecicón
        LinkedHashSet<Integer> l = new LinkedHashSet<Integer>();
        l.add(60);
        l.add(80);

        linkedHashSet.addAll(l);
        System.out.println("Elements in the Set after addAll method: " + linkedHashSet);

        //Borrar un elemento
        linkedHashSet.retainAll(l);
        System.out.println("Elements in the Set after retainAll method: " + linkedHashSet);

        //Converte un LinkedHashSete en un TreeSet, un HashSet y un Array
        Set<String> s = new LinkedHashSet<String>();
        s.add("Chennai");
        s.add("Bangalore");
        s.add("Delhi");
        s.add("Mumbai");

        System.out.println("LinkedHashSet: " + s);
    
        Set<String> t = new TreeSet<String>(s);
        System.out.println("TreeSet: " + t);
        
        Set<String> h = new HashSet<String>(s);
        System.out.println("HashSet: " + h);

        String[] a = new String[s.size()];
        s.toArray(a);
        
        System.out.println("Convert to array:");
        System.out.println(Arrays.toString(a));
    }
}
