package ejemplo00.comparator.ColeccionesJava.CollectionsSet;


import java.util.*;

import javax.swing.JOptionPane;

import ejemplo00.comparator.ColeccionesJava.DTO.ComparadorEmpleados;
import ejemplo00.comparator.ColeccionesJava.DTO.Empleado;
import ejemplo00.comparator.ColeccionesJava.DTO.EmpleadoConComparable;
import ejemplo00.comparator.ColeccionesJava.DTO.EmpleadoConComparableComparator;
import ejemplo00.comparator.ColeccionesJava.DTO.EmpleadoConComparator;

/**
 * Collecion 
 * Interfaz Set -> Coleccion de elemntos donde ninguno de ellos está repetido y pueden estar o no ordenados
 * Interfaz SortedSet -> Extiende de la iterfaz Set, y permite que los elementos esten ordenados por defecto en ascednete y dado la naturleza del valor o por un comprado especificado
 */
public class UseHashSet {
    public static void main(String[] args) {
        int seleccion = -1;
        while(seleccion==-1){
            seleccion = JOptionPane.showOptionDialog(
                null,
                "Seleccione opcion", 
                "Selector de opciones",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,    // null para icono por defecto.
                new Object[] { "HashSet", "TreeSet" },   // null para YES, NO y CANCEL
                "opcion 1");

            if(seleccion==-1)
                seleccion = JOptionPane.showOptionDialog(
                    null,
                    "Salir?", 
                    "Selector de opciones",
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,    // null para icono por defecto.
                    new Object[] { "Si", "No" },   // null para YES, NO y CANCEL
                    "opcion 1");
                
        }
        
        if (seleccion != -1){
            System.out.println("seleccionada opcion " + (seleccion + 1));

            switch(seleccion+1){
                case 1:
                    useHashSet();
                break;
                case 2:
                    useSorteedSet();
                break;
            };
        }
    }


    public static void useHashSet(){
        //Coleccion no acceso aleatorio ni ordenada
        Set<Empleado> hasSet = new HashSet<Empleado>();
		
        hasSet.add(new Empleado("Andres","Ruiz"));
        hasSet.add(new Empleado("Pedro","Luis"));
        hasSet.add(new Empleado("Ramon","Ruiz"));

        Iterator it = hasSet.iterator();

        System.out.println("Coleccion hasSet creada con los elementos: ");
        while(it.hasNext()){
            System.out.println(it.next());
        }
        
    }

    public static void useSorteedSet(){
        //Coleecion de arboles de acceso no 
        
        //Clase sin implemtenar "Comparable" y "Compartor"
        //Set<Empleado> treedSett1 = new TreeSet<Empleado>();
        //treedSett.add(new Empleado("Andres","Ruiz")); //No tiene implementado o sobrescitor el meoto compareTo() "Comparable", por lo que no se pueden el
        
        //Clase con la interfaz Comparable implementada (comparable-> compara this con un objeta de la misma clase)
        Set<EmpleadoConComparable> treedSett2 = new TreeSet<EmpleadoConComparable>();
        treedSett2.add(new EmpleadoConComparable("Andres","Luis"));
        treedSett2.add(new EmpleadoConComparable("Pedro","Luis"));
        treedSett2.add(new EmpleadoConComparable("Ramon","Ruiz"));

        Iterator it2 = treedSett2.iterator();

        System.out.println("Coleccion treedSett2 creada con los elementos que implementa la clase Comparable: ");
        while(it2.hasNext()){
            System.out.println(it2.next());
        }

        /*********************************************************************************************************************/
        //Clase con la interfaz Comparator implementada (compara dos objetos de la misma clase)
        
        //Set<EmpleadoConComparator> treedSett3 = new TreeSet<EmpleadoConComparator>();
        //treedSett3.add(new EmpleadoConComparator("Andres","Luis"));//No tiene implementado o sobrescitor el meoto compareTo() "Comparable", por lo que no se pueden el

        /*********************************************************************************************************************/
        //Clase con la interfaz Comparator  y Comparable implementada
        //Permite ordenar los elementos de un TreeSet de la forma que indica el el método "Compare" del objeto referenciado en el constructo y no del método comparteTo.
       
        //Ordena por defecto según indica el método compareTo
        Set<EmpleadoConComparableComparator> treedSett4_a = new TreeSet<EmpleadoConComparableComparator>( );
        treedSett4_a.add(new EmpleadoConComparableComparator("Andres","Luis"));
        treedSett4_a.add(new EmpleadoConComparableComparator("Mark","Polo"));
        treedSett4_a.add(new EmpleadoConComparableComparator("Pedro","Albarez"));
        treedSett4_a.add(new EmpleadoConComparableComparator("Ramon","Ruiz"));

        Iterator it4_a = treedSett4_a.iterator();

        System.out.println("Coleccion treedSett4_a ordenador por un 'compareTo': ");
        while(it4_a.hasNext()){
            System.out.println(it4_a.next());
        }

        //Ordenamos según indica el metodo compare de la propia clase
        EmpleadoConComparableComparator comparadorEmpleadoConComparableComparator = new EmpleadoConComparableComparator(); //Rerencia

        Set<EmpleadoConComparableComparator> treedSett4 = new TreeSet<EmpleadoConComparableComparator>(
            comparadorEmpleadoConComparableComparator //Especifimcoas que ordene treedSett4 como el metoodo "compare" implementado
        );
        treedSett4.add(new EmpleadoConComparableComparator("Andres","Luis"));
        treedSett4.add(new EmpleadoConComparableComparator("Mark","Polo"));
        treedSett4.add(new EmpleadoConComparableComparator("Pedro","Albarez"));
        treedSett4.add(new EmpleadoConComparableComparator("Ramon","Ruiz"));
        
        Iterator it4 = treedSett4.iterator();

        System.out.println("Coleccion treedSett4 ordenador por un 'compare' de la propia clase: ");
        while(it4.hasNext()){
            System.out.println(it4.next());
        }

        /*********************************************************************************************************************/
        //TreeSet de una clase con Compare en una clase que no tiente comparteTo (este remplaza cualquier compare de la propia case si
        //existiera)
        ComparadorEmpleados bySurname = new ComparadorEmpleados();

        TreeSet<Empleado> treedSett5 = new TreeSet<Empleado>(bySurname);
        treedSett5.add(new Empleado("Andres","Luis"));
        treedSett5.add(new Empleado("Mark","Polo"));
        treedSett5.add(new Empleado("Pedro","Albarez"));
        treedSett5.add(new Empleado("Ramon","Ruiz"));
        
        Iterator it5 = treedSett5.iterator();

        System.out.println("Coleccion treedSett5 que no tiene implementada compareTo': ");
        while(it5.hasNext()){
            System.out.println(it5.next());
        }

        //Con clase anonima - modo iperativo (ahorra codigo)
        TreeSet<Empleado> treedSett6 = new TreeSet<Empleado>(new Comparator<Empleado>(){

            @Override
            public int compare(Empleado o1, Empleado o2) {
                // Por surnaem
                return o1.getSurname().compareTo(o2.getSurname());
            }
            
        });

        //Con clase anonima - modo declarativo o lambda (ahorra codigo)
        Comparator<Empleado> compareBySurnmae = (Empleado o1,Empleado o2)->{
            return o1.getSurname().compareTo(o2.getSurname());
        };
        Set<Empleado> treedSett7 = new TreeSet<Empleado>(compareBySurnmae);

        treedSett7.add(new Empleado("Andres","Luis"));
        treedSett7.add(new Empleado("Mark","Polo"));
        treedSett7.add(new Empleado("Pedro","Albarez"));
        treedSett7.add(new Empleado("Ramon","Ruiz"));
        
        Iterator it7 = treedSett7.iterator();

        System.out.println("Coleccion treedSett6 que no tiene implmentado compareTo (clase anonima)': ");
        while(it7.hasNext()){
            System.out.println(it7.next());
        }

    }
}
