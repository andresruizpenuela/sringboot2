package ejemplo00.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Comparator01  {
    
    public static void main(String[] args){
        List<Persona> lista = new ArrayList<>();

        lista.add(new Persona("Andres",29));
        lista.add(new Persona("Baltasar",15));
        lista.add(new Persona("Juan",26));
        lista.add(new Persona("Carlos",15));

        //Lista no ordenada
        System.out.print("""
        **************************\n
        Lista no ordenada 
        \n**************************
        """);
        for (Persona persona : lista) {
            System.out.println(persona.toString());
        }


        //Ordena la lista de fomra aritemtico mediante una calse
        System.out.print("""
        **************************\n
        Lista ordenada por nombre
        \n**************************
        """);
        Collections.sort(lista, new NameComparator());

        System.out.println("");
        for (Persona persona : lista) {
            System.out.println(persona.toString());
        }

        //Ordenrar la lista por dedad sin clase adicional
        System.out.print("""
        **************************\n
        Lista ordeanda por edad
        \n**************************
        """);
        Collections.sort(lista, new Comparator<Persona>(){
            public int compare(Persona p1, Persona p2){
       
                if(p1.old>p2.old){
                    return 1;
                }else if(p1.old<p2.old){
                    return -1;
                }else{
                    return 0;
                }
            }
        } );
        
        for (Persona persona : lista) {
            System.out.println(persona.toString());
        }
    }

    

    
    //
    //Clase personal
    public static class Persona{
        private static int count =-1;
        public Integer id;
        public String name;
        public Integer old;
        public Persona(String name, int old){
            this.id = ++count;
            this.name = name;
            this.old = old;
        }

        public String toString(){
            return "Persona = [ id= "+id+",name= "+name+", old= "+old+" ]";
        }
    }

    /*Creamos una clase con el metodo de comparar */
    public static class NameComparator implements Comparator{
        @Override
        public int compare(Object obj1, Object obj2) {
            Persona per1 = (Persona) obj1;
            Persona per2 = (Persona) obj2;

            return per1.name.compareTo(per2.name);
        }
    }

}
