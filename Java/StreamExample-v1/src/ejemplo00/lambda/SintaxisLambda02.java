package ejemplo00.lambda;
/*
* Lambda - Implementicon de clases anonimas
*/
public class SintaxisLambda02 {
    /*
     * Sintaxis Labmda 01: Sin especidficar tipos
     */
    public double probarSitaxis01(){
        IOperacion operacion = (x,y)->(x+y)/2;

        return operacion.calcularPromedio(3, 3);
    }

     /*
     * Sintaxis Labmda 02: Especificando tipos de parametros de entrada
     */
    public double probarSitaxis02(){
        IOperacion operacion = (double x, double y)->(x+y)/2;

        return operacion.calcularPromedio(3, 3);
    }

     /*
     * Sintaxis Labmda 03: Encerrando el cuerpo entre llaves
     */
    public double probarSitaxis03(){
        IOperacion operacion = (x, y)->{ return (x+y)/2; };

        return operacion.calcularPromedio(3, 3);
    }

     /*
     * Sintaxis Labmda 04: Con parametros y el cuerpo entre llaves
     */
    public double probarSitaxis04(){
        IOperacion operacion = (double x,double y)->{ return (x+y)/2; };

        return operacion.calcularPromedio(3, 3);
    }

     /*
     * Sintaxis Labmda 05: Sin parametros
     */
    public double probarSitaxis05(){
        IOperacionSinParametros operacion = ()->{ return (3+3)/2; };

        return operacion.calcularPromedio();
    }

    public static void main(String[] args) {
        SintaxisLambda02 app = new SintaxisLambda02();

        System.out.println("Sintaxis 01: "+app.probarSitaxis01());
        System.out.println("Sintaxis 02: "+app.probarSitaxis02());
        System.out.println("Sintaxis 03: "+app.probarSitaxis03());
        System.out.println("Sintaxis 04: "+app.probarSitaxis04());

    }
}
