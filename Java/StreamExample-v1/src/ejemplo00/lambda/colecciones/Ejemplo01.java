package ejemplo00.lambda.colecciones;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

//Streams filter() and collect()
public class Ejemplo01 {
    public static void main(String[] args) {

        List<String> result1 = frutas.stream()           //// convert list to stream
            .filter( fruta -> fruta.equals("Sandia"))   // we like Sandia
            .collect(Collectors.toList());              // collect the output and convert streams to a List

        result1.forEach(System.out::println);            //output : sandia
        System.out.println("------------------------------------------------");


        Set<String> result2 = frutas.stream()
            .filter( fruta -> !fruta.equals("Sandia"))
            .collect(Collectors.toSet());
        
            result2.forEach(System.out::println);  //output : Manzana, Pera
        
    }   
}
