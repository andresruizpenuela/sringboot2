package ejemplo00.lambda.colecciones;

import java.util.Arrays;
import java.util.List;

//Streams filter(), findAny() and orElse()
public class Ejemplo02 {
  

        public static void main(String[] args) {
            List<String> frutas = Arrays.asList("Manzana","Sandia","Pera");
            String result0 = frutas.stream()                        // Convert to steam
                .filter( fruta -> fruta.equals("Sandia"))           // we want "Sandia" only
                .findAny()                                          // If 'findAny' then return found
                .orElse(null);                                      // If not found, return null
    
            System.out.println(result0);            //output : sandia
            System.out.println("------------------------------------------------");
        }
}
