package ejemplo00.lambda;

public interface IOperacionSinParametros {
    public double calcularPromedio();
}
