package ejemplo00.lambda.MetodosPorDefecto;

/*
* Un metodo por defecto, es un metodo cuya lógica ya esta definida en la porpia interfaz, tal que cuando
* se implemta la interfaz en una case, no tiene por que ser sobreesquita
*/
public interface PersonaA {
    public void caminar();

    default public void hablar(){
        System.out.println("Salduos! hablemos desde peronsa A");
    }
}
