package ejemplo00.lambda.MetodosPorDefecto;
/*
* Un metodo por defecto, es un metodo cuya lógica ya esta definida en la porpia interfaz, tal que cuando
* se implemta la interfaz en una case, no tiene por que ser sobreesquita.
*
* Los metoods default son útiles para definir un comportamiento común para todas las clases que implementa la interfaz sin tener que
* estrar rescribiendo el metodo.
* 
* Dos interfaces con metodos por defecto con el mismo nombre, se debe sobrescribir indicado la interfaz
*/
public class DefaultMethods implements PersonaA, PersonaB {
    @Override
    public void caminar() {
        System.out.println("Hola mundo");
    }

    //Indciamos cual es metodo de la interfaz a uitlizar, y sobrescribe el metodo 'hablar'
    @Override
    public void hablar() {
        PersonaA.super.hablar(); //iNDICMAOS QUE QUERMEOS UTILIZAR EL METODO POR DEFECTO HABLAR DE PERSONAA
    }

    //Cambiar el cuerpo de un metodo por defecto,
    @Override
    public void cantar() {
        //PersonaB.super.cantar();
        System.out.println("Cabmio el valor desde la clase que implmeta la interfaz");
    }



    public static void main(String[] args) {
        DefaultMethods app = new DefaultMethods();
        app.caminar();
        app.hablar();
        app.cantar();
    }
}
