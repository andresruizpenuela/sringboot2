package ejemplo00.lambda.MetodosPorDefecto;

public interface PersonaB {
    default public void cantar(){
        System.out.println("Hola! valor por defecto");
    }

    //Este metodo tmabine existe en PersonaA
    default public void hablar(){
        System.out.println("Salduos! hablemos desde peronsa B");
    }


}
