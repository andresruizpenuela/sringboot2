package ejemplo00.lambda;

public class SintaxisLambda01 {
    //Modo tradicional - imperativo
    public double probarSintaxisImperativo(){
        //Creamos una clase anónima y la instancimos en una interraz
        IOperacion operacion = new IOperacion(){
            @Override
            public double calcularPromedio(double x, double y) {
                return (x+y)/2;
            };
        };

        return operacion.calcularPromedio(3, 3);
    }

    //Mediante lambda - Modo declarativo
    public double probarSintaxisLambda(){
        IOperacion operacion = (double a,double b) -> (a+b)/2;

        return operacion.calcularPromedio(2,3);
    }

    public static void main(String[] args) {
        SintaxisLambda01 app = new SintaxisLambda01();

        System.out.println("Promedio - modo imperativo: "+app.probarSintaxisImperativo());
        System.out.println("Promedio - modo declartivo: "+app.probarSintaxisImperativo());
        
    }

    
}
