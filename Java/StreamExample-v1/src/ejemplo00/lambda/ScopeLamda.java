package ejemplo00.lambda;

public class ScopeLamda {

    //Variable Local -> Modo Imperativo
    double probarVariableLocalImperativo(double n1,double n2){
    
        final double n3 = 2;//Variable local

        //Lambda -> Implementaicon de clase anomima en modo impeatibo
        IOperacion op = new IOperacion(){
            
            @Override
            public double calcularPromedio(double x, double y) {
                //implica que n3 sea final (consatnte), por lo que puedo utilizar su valor pero no alterar   
                return (n1+n2)/n3;
            }
        };
        return op.calcularPromedio(n1, n2);
    }
    //Variable Local -> Modo Declarativo
    double probarVariableLocalDeclartivo(double n1,double n2){
    
        final double n3 = 2;//Variable local

        //Lambda -> Implementaicon de clase anomima en modo declatirvo
        //el suo de la variable local n3 dentro de una clase anonimoa implica que esta sea "final" (constante), por lo que se puede leer,
        //pero no editar
        IOperacion op = (x,y)-> (x+y)/n3;
        return op.calcularPromedio(n1, n2);
    }

    //atributos globaes o de clase
    private static double atributo1;
    private double atributo2;

    //Hacer referneica a un atributo de clase o global dentro de un lambda
    double probarVariableClase(double n1, double n2){
        IOperacion op = (x,y)->{
            atributo1 = (x+y)/2; //Puede ser alterado
            return atributo1;
        };

        return op.calcularPromedio(n1, n2);
    }

    //Hacer referneica a un atributo de clase o global dentro de un lambda
    double probarVariableGlobal(double n1, double n2){
        IOperacion op = (x,y)->{
            atributo2 = (x+y)/2; //Puede ser alterado
            return atributo2;
        };

        return op.calcularPromedio(n1, n2);
    }
    public static void main(String[] args) {
        ScopeLamda app = new ScopeLamda();

        //Lambda llama a una vairalbe local -> IMPLICA ue sea FINAL (solo se puede leer)
        System.out.println("Scope Variable local imperativo: "+app.probarVariableLocalImperativo(3, 3));
        System.out.println("Scope Variable local declarativo: "+app.probarVariableLocalDeclartivo(2, 2));

        //Lamda llama a una variable de clase (statica) o global -> Pueden ser editas, se interpretna como atributos de un obejto anonimo
        System.out.println("Scope Varible de clase (atributo estatico): "+app.probarVariableClase(7, 7));
        System.out.println("Scope Varible de clase (atributo): "+app.probarVariableGlobal(7, 8));
    }
}
