package ejemplo00.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SintaxisLambda {

    public static void main(String[] args){
        SintaxisLambda app = new SintaxisLambda();
        app.ordernar();
    }

    public void ordernar(){
        List<String> lista = new ArrayList<>();

        lista.add("MitoCode");
        lista.add("Code");
        lista.add("Mito");

        System.out.println("\n*************\nLista de fruta\n**********");
        for (String string : lista) {
           System.out.println(string);
        }

        //Modo imperativo
        Collections.sort(lista,new Comparator(){
            public int compare(Object o1, Object o2) {
                String s1 = (String) o1;
                String s2 = (String) o2;

                return s1.compareTo(s2);
            };
        });
        System.out.println("\n*************\nLista de fruta ordenada\n**********");
        for (String string : lista) {
           System.out.println(string);
        }

        /*********************************************************************/
        lista.add("AWK");
        System.out.println("\n*************\nLAgregamos AWK\n**********");
        for (String string : lista) {
           System.out.println(string);
        }

        //Modo declarativo
        Collections.sort(lista,(o1,o2 )->{
            String s1 = (String) o1;
            String s2 = (String) o2;

            return s1.compareTo(s2);
        });
        //Es lo mimos que
        //Collections.sort(lista,(Sring s1,String s2)-> s1.compareTo(s2));


        System.out.println("\n*************\nLista de fruta ordenada\n**********");
        for (String string : lista) {
           System.out.println(string);
        }
    }

    
}
