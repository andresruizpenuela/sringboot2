package ejemplo01;
/**
 * Vamos a hacer una interfaz llamada calculadora que tenga un método llamado suma que nos calculé la suma de dos números, 
 * de la manera clásica y con lambda.
 * 
 * Main01 -> Manera clasica
 * Main02 -> Manera con lambda
 */
public class Main01 {
    //Definimos la función con un solo método
    public interface Calc{
        public void suma(int a,int b);
    }

    public static void main(String[] args){

        //Implementamos la intefaz y definimos la lógica del método
        //Paradigma de la programacion imperativo -> Defimos como lo queremos
        Calc calc = new Calc(){
            @Override
            public void suma(int a, int b){
                System.out.println("\ta + b ="+a+" + "+b+" ="+(a+b));
            };
        };

        //Llamamdos al metodo
        calc.suma(1,2);

    }
}
