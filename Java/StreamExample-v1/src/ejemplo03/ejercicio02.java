package ejemplo03;

import java.util.ArrayList;

import java.util.function.BiFunction;
import java.util.function.Function;

/*
* Defina un método listTest (ArrayList al, Integer s), que requiere devolver el índice de la primera aparición de s en al, 
* o -1 si no ha aparecido s.
*
*/
public class ejercicio02 {
 
    public static void main(String[] args) {

        ArrayList<Integer> al = new ArrayList<>();
        al.add(1);
        al.add(2);
        al.add(3);
        al.add(5);
        al.add(3);

        System.out.println("Lista de elemetnos: "+al.toString());
        
        System.out.println("Posciones de elementos (Imperativo): ");
        System.out.println("Posicion del 3? "+listTest(al, 3));
        System.out.println("Posicion del 0? "+listTest(al, 0));
        System.out.println("Posicion del 5? "+listTest(al, 5)); 
        
        /*
         * Mediante Lambada
         */
        BiFunction<ArrayList<Integer>, Integer, Integer> listTestLambda = (lista,ele) -> lista.indexOf(ele);
        System.out.println("Posciones de elementos (Declarativo- BiFunction): ");
        System.out.println("Posicion del 3? "+listTestLambda.apply(al, 3));
        System.out.println("Posicion del 0? "+listTestLambda.apply(al, 0));
        System.out.println("Posicion del 5? "+listTestLambda.apply(al, 5)); 
        
        Function<Integer, Integer> listTestLambda2 = (ele) -> al.indexOf(ele);
        System.out.println("Posciones de elementos (Declarativo- Function mediante variables locales): ");
        System.out.println("Posicion del 3? "+listTestLambda2.apply(3));
        System.out.println("Posicion del 0? "+listTestLambda2.apply(0));
        System.out.println("Posicion del 5? "+listTestLambda2.apply(5)); 
        

        System.out.println("Posciones de elementos (Declarativo- Interfaz funcional): ");
        InterfaceListTetLamda iLamdaTest = (lista,eleme)->{
            return lista.indexOf(eleme);
        };
        System.out.println("Posicion del 3? "+iLamdaTest.listTestLambda3(al, 3));
        System.out.println("Posicion del 0? "+iLamdaTest.listTestLambda3(al, 0));
        System.out.println("Posicion del 5? "+iLamdaTest.listTestLambda3(al, 5));
    
    }

    public static int listTest(ArrayList<Integer> al, Integer s){
        /* 
        //Usando lambada sobre una coleccion
        Integer resul = (Integer) al.stream().filter(numero -> numero == s).findFirst().orElse(-1);
        return (resul==-1)?resul:al.indexOf(s);
        */
        
        return al.indexOf(s);
    }

    interface InterfaceListTetLamda{
        public Integer listTestLambda3(ArrayList<Integer> al, Integer s);
    }
}
