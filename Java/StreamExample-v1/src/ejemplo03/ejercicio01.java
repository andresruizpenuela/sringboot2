package ejemplo03;

import java.util.Set;
import java.util.TreeSet;

/*
 * Genere 10 números aleatorios que van del 1 al 100 y colóquelos en una matriz.
 * 
 * Coloque los números mayores o iguales a 10 en la matriz 
 * en un conjunto de listas e imprímalos en la consola.
 */
public class ejercicio01 {
    public static void main(String[] args) {
    
        int[][] matriz = new int[2][5];
        Set<Integer> matrizSet = new TreeSet<>();

        System.out.println("Filas= "+matriz.length);
        System.out.println("Columnas= "+matriz[0].length);
        iniciar(matriz);
        
        System.out.println("Mtriz generada: ");
        for (int[] is : matriz) {
            for (int is2 : is) {
                System.out.println("Elemento= "+is2);
                if(is2>10){
                    matrizSet.add(is2);
                }
            }
        }

        System.out.println("Matriz Ordenada sobtr una colección: ");
        matrizSet.forEach(System.out::println);

    }

    public static void iniciar(int[][] m){
        //
        for(int i=0;i<m.length;i++){
            for(int j=0;j<m[i].length;j++){
                m[i][j]=(int) ((Math.random()*100)+1);
            }
        }
        /*
        for(int i=0;i<m.length;i++){
            for(int j=0;j<m[i].length;j++){
                System.out.println(m[i][j]);
                 //m[i][j]
            }
        }
        */
    }   
}
