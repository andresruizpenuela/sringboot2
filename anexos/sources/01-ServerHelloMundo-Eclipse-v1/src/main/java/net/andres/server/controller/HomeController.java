package net.andres.server.controller;

import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String mostrarHome(Model model) {
		model.addAttribute("mensaje","Hola Mundo!!");
		model.addAttribute("fecha",new Date());
		return "home";
	}
}
