# API Stream de Java 8

## Abstrac.

<div style="text-align: justify;">
    La interfaz <a href="https://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html">Stream</a> que en Español quiere decir <b>flujo</b> o <b>secuencia</b>, y según la documentación oficial de Java, esta interfaz representa una secuencia de elementos, pero ¿qué clase de elementos?, elementos del tipo que indiquemos.<br>
    Por lo tanto, podemos decir que la interfaz <b>Stream</b> es genérica, es decir, que podemos recuperar tanto los elementos primitivos de tipo int, float, a tipos compuestos como string, arrays e incluso, los elmenentos que pertenence a una clase defenida por el desarrollador.<br>
    Ejemplo:
</div>

```java
Stream<Product> productStream = products.stream(); //flujo de elementos Product

// imprime la lista de productos
productStream.forEach(product -> System.out.println(product)); 

//esta linea es equivalente a la anterior- la clase productos debe tener toString definido
productStream.forEach(System.out::println); 
```

<div style="text-align: justify;">
    En este workshop realizaremos algunas operaciones que aplicamos comúnmente a una base de datos relacional usando sql, pero las haremos sobre listas en Java usando Stream, por lo tanto a continuación te presento una tabla donde se muestran algunos métodos de la interfaz <b>Stream</b> junto a una posible equivalencia en sql. No te detengas mucho en revisarla, mejor regresa a ella conforme vayas teniendo dudas a lo largo de este workshop.
</div>

| SQL      | Interfaz Stream                     |
| :------- | :---------------------------------- |
| from     | stream()                            |
| select   | map()                               |
| where    | filter() (antes de un collecting)   |
| order by | sorted()                            |
| distinct | distinct()                          |
| having   | filter() (después de un collecting) |
| join     | flatMap()                           |
| union    | concat().distinct()                 |
| offset   | skip()                              |
| limit    | limit()                             |
| group by | collect(groupingBy())               |
| count    | count()                             |

p

## 1. Consultas.

Considera la siguiente consulta **sql**,

```sql
    select name from products
```

El equivalente usando Java **Streams** es,

```java
List<Product> products;
...
Stream<String> streams = products.stream().map(Product::getName);
```

- Con el método `stream()` obtenemos una secuencia de elementos de tipo `Product`. Este es el **from**.
- Con el método `map` recuperamos solo el atributo *name*. Este es el **select**.

El punto clave es obtener un Stream mediante el método `stream()` y a partir de ahí ejecutar las operaciones como filtrados, agrupaciones, etc.

## 2. Filtros.

> Recuperar los nombres de productos que tengan una existencia en el almacen menor a 10 unidades.

En **sql**,

```sql
   select name from products where units_in_stock < 10
```

Con Java **Streams**,

```java
Stream<String> streams = products.stream().filter(p -> p.getUnitsInStock()<10).map(Product::getName);
streams.forEach(product -> System.out.println(product)); //imprime el resultado en consola
```

Es importante notar el órden en el que aparecen los métodos, primero se encuentra filter y después map. ¿Qué ocurre si colocamos primero a map y luego a filter?

```java
//ERROR DE COMPILACION
Stream<String> streams = products.stream().map(Product::getName).filter(p -> p.getUnitsInStock()<10);
```

Obtendremos un error de compilación. ¿Por qué? Porque el método map devuelve el nombre del producto que es un String y la clase String no tiene un método que se llame filter.

<u>El método `filter()` recibe un **predicado**</u>.  **Un predicado es solo una función que devuelve un valor boolean y la instrucción `p.getUnitsInStock()<10`  es una expresión booleana.**

Si nuestra lógica es más compleja podemos considerar usar un método en lugar de lamdas.

```java
...
Stream<String> streams = products.stream()
                            .filter(predicado()) //invocamos a un método de predicado
                            .map(Product::getName);
...

public Predicate<Product> predicado(){
    return new Predicate<Product>() {
        @Override
        public boolean test(Product product) {
            //aqui la logica requerida para devolver true o false
            return product.getUnitsInStock() < 10;
        }
    };
}
```

## 3. Ordenar.

> Obtener los nombres de productos que tengan una existencia menor a 10 unidades en el almacen pero ordenados de forma ascendente, es decir, de menor existencia a mayor existencia.

En **sql**

```java
   select name from products where units_in_stock < 10
   order by units_in_stock asc
```

Con Java **Streams**

```java
Stream<String> streams = products.stream()
                .filter(p -> p.getUnitsInStock()<10)
                .sorted(Comparator.comparingDouble(Product::getUnitsInStock))
                .map(Product::getName)
                ;
```

El método `sorted` recibe un `Comparator`. Ésta misma interfaz `Comparator` tiene algunos métodos que nos serán de gran ayuda

- `comparingInt()` Permite comparar elementos de tipo int
- `comparingDouble()` Permite comparar elementos de tipo double
- `comparingLong()` Permite comparar elementos de tipo long
- `thenComparing()` Permite anidar comparaciones. Útil cuándo deseamos ordenar por más de 1 atributo (ejemplo más adelante)

Lo mejor será revisar la documentación de la interfaz [Comparator](https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html)

Si deseamos ordenar en forma descendente necesitamos aplicar un reverso,

```java
Stream<String> streams = products.stream()
                .filter(p -> p.getUnitsInStock()<10)
                .sorted(Comparator.comparingDouble(Product::getUnitsInStock).reversed())
                .map(Product::getName)
                ;
```

Otra forma diferente de ordenar, es que nuestra clase Product implemente a la interfaz `Comparable`

```java
public class Product implements Comparable<Product>{
    ...
    // atributos, setters/getters
    ...

    @Override
    public int compareTo(Product p) {
        if(this.getUnitsInStock() < p.getUnitsInStock())
            return -1;
        else if(this.getUnitsInStock() > p.getUnitsInStock())
            return 1;
        else
            return 0;
    }
}
```

Ahora al método `sorted()` es invocado sin argumentos,

```java
Stream<String> streams = products.stream()
        .filter(p -> p.getUnitsInStock()<10)
        .sorted()
        .map(Product::getName)
        ;
```

¿Cómo ordenamos en forma descendente? Usando Comparator.reverseOrder()

```java
Stream<String> streams = products.stream()
        .filter(p -> p.getUnitsInStock()<10)
        .sorted(Comparator.reverseOrder())
        .map(Product::getName)
        ;
```

¿Y si queremos ordenar por unitsInStock de forma descendente y por nombre de producto de forma ascendente?

En **sql**,

```sql
select productName, unitsInStock from products
where unitsInStock < 10
order by unitsInStock desc, productName asc;
```

Con Java **Streams**

```java
Stream<String> streams = products.stream()
        .filter(p -> p.getUnitsInStock()<10)
        .sorted(
            Comparator //recordar que el método sorted recibe un Comparator.
                .comparing(Product::getUnitsInStock)
                .reversed() //invertimos el orden, será de mayor a menor
                .thenComparing(Product::getName) //una vez ordenado por unitsInStock, entonces ordenamos por nombre
        )
        .map(Product::getName)
        ;
```

Y si ahora queremos invertir las cosas y ordenar por unitsInStock de forma ascendente y por nombre de forma descendente?

En **sql**,

```sql
select productName, unitsInStock from products
where unitsInStock < 10
order by unitsInStock asc, productName desc;
```

Con Java Streams podríamos pensar en solo cambiar la posición del método `reversed()`

```java
Stream<String> streams = products.stream()
        .filter(p -> p.getUnitsInStock()<10)
        .sorted(
            Comparator
                .comparing(Product::getUnitsInStock) 
                .thenComparing(Product::getName) 
                .reversed()

        )
        .map(Product::getName)
        ;
```

Sin embargo esto no es correcto ya que estamos ordenando de forma descendente por ámbos atributos, unitsInStock y name.

La forma correcta es aplicar el reverse sólo al campo **name**,

```java
jStream<String> streams = products.stream()
        .filter(p -> p.getUnitsInStock()<10)
        .sorted(
                Comparator
                        .comparing(Product::getUnitsInStock) //ordenamos ascendente por unitsInStock 
                        .thenComparing( // despues ordenamos por otro campo
                            Collections.reverseOrder( // pero este segundo campo sera por orden descendente
                                Comparator.comparing(Product::getName) // el segundo campo a ordenar
                            )
                        )
        )
        .map(Product::getName)
        ;
```

Hasta este punto podemos resumir lo siguiente:

- El método `sorted()`recibe un `Comparator`
- La interfaz `Comparator` nos proporciona algúnos métodos que nos serán útiles para las ordenaciones.
- Existe una clase `Collections` que tiene un método `reverseOrder()` el cual devuelve un `Comparator` que impone el reverso de una ordenación.
- Hay que tener cuidado donde se aplican las operaciones como reversos ya que podríamos aplicarlos a toda la colección y no a los campos que deseamos.



## Notas:

Un **predicado** es solo una función que devuelve un valor boolean.

## List y ArrayList

<mark>**List** es una colección, y una colección puede ser interfaces y clase abstracta que nos permite identificar los objetos independientemente de la implementación. Es decir, son genéricas.<br>Mientras, un **ArrayList** es contenedor que contiene una implementación de la colección List.</mark>

Aquí puedes ver un ejemplo de las colecciones y sus relaciones en Java: https://en.proft.me/2013/11/3/java-collection-framework-cheat-sheet/

En base a esto, ***List\*** es una interfaz genérica que representa una colección ordenada de elementos que pueden repetirse.

Mientras, dos listas de propósito general serían las clases ***LinkedList\*** y ***ArrayList\*** y de propósito específico, ***Vector\*** y ***CopyOnWriteArrayList\***.

En tu duda, la ***ArrayList\*** es un array que se maneja como una clase y no tiene tamaño fijo. Es eficiente cuando se realizan muchos acceso y en este caso los elementos se añaden al final. Permite que se puedan eliminar elementos intermedios, pero eso provocará un desplazamiento de índices.

En cambio, para una clase ***LinkedList\*** tendremos una lista enlazada en la que los elementos se añaden en cualquier parte de la lista muy fácilmente. Aquí, para encontrar un elemento hay que recorrer la lista. Es eficiente cuando el tamaño fluctúa y sobre todo en posiciones centrales.

¿Qué sucede si utilizas ***List\*** como lo haces en tu caso?, que podrías cambiar el tipo de *zzz* a ***LinkedList\*** en su definición y todo sería compatible 100% y fácilmente. No obstante, en el primer caso de *xxx* tendrías que cambiar en todos los sitios el tipo debido a que estas implementando un ***ArrayList\*** y podrías tener problemas si lo cambias a ***LinkedLIst\***. Por eso se recomienda el uso de ***List\***, como lo haces de *zzz*.



https://windoctor7.github.io/API-Stream-Java8.html

https://www.java67.com/2014/11/java-8-comparator-example-using-lambda-expression.html

https://www.oracle.com/technical-resources/articles/java/architect-lambdas-part2.html