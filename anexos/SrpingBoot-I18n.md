# Hola Mundo: Multi-idoma.

[TOC]

## Abstract.

<div style="text-align: justify">
    <p>
        En este manual, vamos a crear un servidor web donde se ofrece el servicio de dar soporte en distintos idiomas, de una manera rápida y sencilla.<br>
        Además, ara no hacerlo muy complejo, vamos a proporiconar un mensaje "Hola Mundo!" en varios idiomas, siendo la estructura de neustra aplicación web resultante la siguiente:
    </p>
</div>

<img src="C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\app-multilenguage\project.png" style="zoom:80%;" />

## 1. Dependencias.

| Dependencia                   | Descripción                                                  |
| ----------------------------- | ------------------------------------------------------------ |
| spring-boot-starter-thymeleaf | Motor de plantillas de Spring Boot que reemplaza JSP.        |
| spring-boot-starter-web       | Trae de forma transitiva todas las dependencias relacionadas con el desarrollo web. También reduce el recuento de dependencias de compilación. |
| spring-boot-devtools          | Herramienta de Spring Boot que nos permite reiniciar de forma automática nuestras aplicaciones cada vez que se produce un cambio en nuestro código |
| spring-boot-starter-test      | Herramienta de Spring Boot que nos permite realizar diferentes pruebas de nuestra aplicación web. |

## 2. Clase Principal.

![](C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\app-multilenguage\img-package_principal.png)

<div style="text-align: justify">
    <p>
        Como toda aplicaicón Spring Boot, es habitual encontrar una aplicación con el método "main", la cual, esta anotada con la etiqueta "@SpringBootApplication".
    </p>
</div>

```java
package com.externalizar.thymelaf.multilenguaje;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultilenguajeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultilenguajeApplication.class, args);
    }
}
```

## 3. Mensajes i18n - _messages.properties_.

<div style="text-align: justify">
    <p>
        Lo prmiero que vamos hacer es crear los ficheros donde van a ir las variables clave – valor de nuestros idioma. Para ello, vamos a crear un direcotrio dentro de “resources” que se va a llamar <b>i18n</b>, Posteriormente dentro de este direcotiro podmeos crear la estructura que deseamos, en el ejemplo expuesto, para no tener todos los mensajes en un mismo fichero, vamos a clasificar los manesajes en secciones:
        <ul>
            <li>i18n/common -> Alojara los mensajes comunes para las distintas páginas de nuestra alicación.</li>
            <li>i18n/nav -> Alojara los mensajes comunes que se incluirá en la barra de navegación.</li>
    </ul>
    En cada subdirectioro, se añadirian los los ficheros que se utilzarán para las traduccions, así en el ejemplo:
        <ul>
            <li>i18n/common:</li>
            <ul>
                <li>messages.properties –> en este fichero irán las traducciones de idioma por defecto, en nuestro caso el español.</li>
            	<li>messages_en.properties –> en este irán las traducciones al inglés.</li>
            </ul>
            <li>i18n/nav:</li>
            <ul>
                <li>messages-nav.properties –> en este fichero irán las traducciones de idioma por defecto, en nuestro caso el español.</li>
            	<li>messages-nav_en.properties –> en este irán las traducciones al inglés.</li>
            </ul>
    </ul>
    Por supuesto, podéis crear tantos ficheros como queráis, por ejemplo, podéis crear un messages_fr para el francés y otro messages_it para el italiano, etc. En definiciva, tendremos la siguiente estrucutra:<br>        
    </p>
</div>

![Mansajes](C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\app-multilenguage\igm-resorces.png)


<div>
    <p>
    El contenido de los ficheros tendrá la siugiente forma:
    </p>
</div>

* messages-nav.properties

```properties
navbar.a.language=Idioma
navbar.a.language.spanish=Español
navbar.a.language.english=Ingles
```

* messages-nav_en.properties

```properties
navbar.a.language=Lenguage
navbar.a.language.spanish=Spanish
navbar.a.language.english=English
```

* messages-nav.properties

```properties
message.a.language=Bienvenido {0}!	
```

* messages-nav_en.properties

```properties
message.a.language=Welcomen  {0}!
```

> :notebook: **Nota**: Mediante lo opción {0}, {1}, ... {N}, podemos pasarme parámetros de entrada al mensaje, desde la vista, de la siguiente manera:
>
> * Parámetro de entrada estático:
>
> ```html
> <etiqueta>[[#{message.a.language(Andres)}]</etiqueta>
> ```
>
> * Parámetro de entrada dinámico (_dato recibido del controlador_):
>
> ```html
> <etiqueta>[[#{message.a.language(${name})}]</etiqueta>
> ```

## 4. Archivo de Configuración - _LengugeConfiguration.java_.

<div style="text-align: justify">
    <p>
        A continuación vamos a crear dentro un nuevo paquete llamado "configruation", el cual, partirá del paquete que contiene la clase principal de la aplición. En el vamos alojar los diferntes ficheros de configuración, en especial, el archivo de configuración "LengaugeConfiguration.java" y "MessagesResolverConfiguration.java".
        <ul>
            <li>LengaugeConfiguration.java -> Clase que implementa la interfaz "<b>WebMvcConfigurer</b>", la cual, cuenta con el método "addInterceptors", a traves del cual, utillizamoremos para detectar camibos en el parametro previamente definico en un beans. Por lo tanto, esta clase será la encarga de establecer la confirgución del idioma en nuestra aplicaicón, aunque al implementar la iterfaz WebMvcConfigurer, se pueden añadri otras funcionalidades de modelo-vista-controlador previamente definidas en el framework.</li>
            <li>MessagesResolverConfiguration.java -> Clase con la que definrimos los ficheros donde se alojan los pares de clave-valor, los cuales, serán los mensajes que se traducirán, cuando se cambien de idoma automáticamente.</li>
    </ul>
    En la imagen, veriamos la estrutura citada:
    </p>
</div>

![Package Configuration](C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\app-multilenguage\img-configuration.PNG)

### 4.1. Configuración para la resolución de los mensajes.

<div style="text-align: justify">
    <p>
       En la clase <b>MessagesResolverConfiguration</b>, se instanciará un Beans a <b>ResourceBundleMessageSource</b>, el cual, indicará el nombre genérico de los ficheros que alojan los mensajes y su ubicación (<i>por defecto, cuelgan de la carpeta resources</i>).
    </p>
</div>

```java
package com.externalizar.thymelaf.multilenguaje.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class MessagesResolverConfiguration {

    /* MENSSAGES CONFIGURATION */
    @Bean
    @Description("Spring Messages Resolver")
    public ResourceBundleMessageSource messageSource() {
        //logger.info("[messageSource2] - "+Locale.getDefault());
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        //messageSource.setBasename("messages");
        messageSource.addBasenames("i18n/common/messages","i18n/nav/messages-nav");
        return messageSource;
    }
    /* END MENSSAGES CONFIGURATION */
}
```

> :notebook: Nota: Los Beans no son nada mas que objetos que son creados y gestionados directamente por el contenedor de spring.
>     

### 4.2. Configuración del cambio de idoma.

<div style="text-align: justify">
    <p>
       En la clase <b>LenguageConfiguration</b>, se crearán 2 Beans y el método "<b>addInterceptors</b>" de la interfaz "<i>WebMvcConfigurer</i>":
        <ul>
            <li>Beans: LocaleResolver -> Con el definimos el idioma por defecto <i>en nuestro caso será "es" (español), pero podéis generar cualquier otro tipo siempre siguiendo la convención ISO-639. Además, Spring proporciona variables estáticas de idioma como por ejemplo la <b>Locale.US</b> o la <b>Localte.FR</b> pero por desgracia no esta la <b>ES</b>, así que hay que crear un objeto del tipo <b>Locale</b>, donde hay que pasarle el idioma, y opcionalmente el país.</i></li>
            <li>Beans: LocaleChangeInterceptor -> Con el definimos un parámetro "lang" en url en la que establecemos el idioma, asoicandolo a un bojeto de tipo "LocaleChangeInterceptor".</li>
            <li>Método: addInterceptors -> Permite añadir un registro de tipo "localeChangeInterceptor", esto nos permite detectar cambios de forma automatica sobre el parámetro lang</li>
    </ul>
        De esta manere, tendriamos configurado el idioma y la posiblidiad de cambiarlo en neustra aplicaicón web.
    </p>
</div>

```java
package com.externalizar.thymelaf.multilenguaje.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

@Configuration
public class LenguageConfiguration implements WebMvcConfigurer {
    /* LANGUAGUE CONFIGURATION */
    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        //slr.setDefaultLocale(Locale.US);
        slr.setDefaultLocale(new Locale("es"));
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }
    /* END LANGUAGUE CONFIGURATION */

}
```

## 5. Vistas.

<div style="text-align: justify">
    <p>
       Vamos a definir dos vistas en la carpeta "templates", en ambos, se crearán unos enlaces para que el usuario pueda cambiar de idoma:
        <ul>
            <li>View:home -> Vista simple con los mensajes.</li>
            <li>View:home-page -> Vista que se añade boostrap + los mensajes.</li>
    </ul>
    Por otro lado y como podréis observar en el código, ambas páginas contiene dos enlaces, uno que añade <i>?lang=es</i> a la url actual y otro a <i>?lang=en</i>. Esta parámetro en la url permite al spring saber que idioma queremos usar, y aparte se la guarda en cookie para que se nos quede guardada durante la sesión.<br>
    Por otro lado, en la parte superior hay un código muy interesante:<br>
    <code>
    [[#{navbar.a.language}]]
	</code><br>
	Recordemos, que con thymeleaf 3 podíamos incluir una variablefuera de la etiqueta html con los dos corchetes, que equivaldría a hacer un:
    <br>
    <code>
    th:text=”#{navbar.a.languague}
	</code><br>
    En la própia tag, pero, ¿que indica la almohadilla # ?, la almohadilla es el carácter que utiliza thymeleaf para saber que esa variable no es del modelo, sino de los archivos de traducción, de este modo thymeleaf accede directamente a nuestros archivos y busca esa variable según el idioma del usuario.
</div>

![](C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\app-multilenguage\img-view.png)

### 5.1. View: home.

```html
<!DOCTYPE html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
<head>

</head>
<body>
    <nav>
        <ul>
            <li><a th:href="@{?lang=es}">[[#{navbar.a.language.spanish}]]</a></li>
            <li><a th:href="@{?lang=en}">[[#{navbar.a.language.english}]]</a></li>
        </ul>
    </nav>
    <div>
        <p>Texto externacionalizado: [[#{message.a.language(Andres)}]]</p>
    </div>

</body>
</html>
```

![](C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\app-multilenguage\img-view-home.png)

### 5.2. View: home-page.

```html
<!DOCTYPE html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
<head th:fragment="header"> <!--Esto indica que es un fragmento que puede ser llamado desde cualquier otro html-->
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content="Hello World 2"/>
    <meta name="author" content="Alejandro Moneo"/>

    <title>Aplicacion Externalizada</title>

    <!-- BOOTSTRAP 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <!-- END BOOTSTRAP 4 -->

    <!-- FONT AWESOME -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- END FONT AWESOME -->
</head>
<body>
    <!-- NAV -->
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
        <h1 class="navbar-brand mb-0">Hello World APP</h1>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="my-navbar-dropdown-languague" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-globe fa-lg" aria-hidden="true"></i> [[#{navbar.a.language}]]
                    </a>
                    <div class="dropdown-menu" aria-labelledby="my-navbar-dropdown-languague">
                        <a class="dropdown-item" th:href="@{?lang=es}">Español</a>
                        <a class="dropdown-item" th:href="@{?lang=en}">English</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- END NAVA -->
    <p>hola: [[#{message.a.language(Andres)}]]</p>
</body>
</html>
```

![](C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\app-multilenguage\img-view-home_page.png)



## 6. Controlador.

![](C:\Users\andre\Documents\Proyectos\cursoSpringBoot2\img\anexos\app-multilenguage\img-controller.png)

<div style="text-align: justify">
    <p>
       En el controlador, tendremos dos handler para resolver las peticiones get "/home" y "/home-page", tal que se devuelve como rescurso las vistas creadas.
</div> 

```java
package com.externalizar.thymelaf.multilenguaje.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    /*
     * http://localhost:8080/home-pag
     */
    @GetMapping("/home-page")
    public String getHomePage(){
        return "home-page";
    }

    /*
     * http://localhost:8080/home
     */
    @GetMapping("/home")
    public String getHome(){
        return "home";
    }
}
```

## 7. Propiedades.

<div style="text-align: justify">
    <p>
       Fichero donde se dica las propiedades generales de nuestra aplicación eb.
</div> 

```yaml
spring:
  application:
    name: Saludo
  thymeleaf:
    cache: false
    mode: HTML
server:
  port: 8080
```

## Bibliografía

[1] [Hola mundo con multi-idioma, vamos a aprender a configurar la internacionalidad con i18n en spring boot](https://maresmewebdevelopers.wordpress.com/2017/11/02/hola-mundo-con-multi-idioma-vamos-a-aprender-a-configurar-la-internacionalidad-con-i18n-en-spring-boot/)