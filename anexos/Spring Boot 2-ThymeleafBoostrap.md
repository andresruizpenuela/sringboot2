# Spring Boot 2 - Thymeleaf - Boostrap

[TOC]

## 1. Integración con CDN

### 1.1. CSS

<div style="text-align: justify;">Copie y pegue la hoja de estilo '&lt;link&gt;' en su '&lt;header&gt;' antes de todas las dem&aacute;s hojas de estilo para cargar nuestro CSS.</div>

* Versión 4.6.0

```html
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
```

* Versión 5.0.0

```html
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous
```

### 1.2. JS

<div style="text-align: justify;">Muchos de los componentes de Boostrap requieren el uso de <b>JavaScript</b> para funcionar. Específicamente, requieren el uso de <b>jQuery</b>, <b>Popper</b> y nuestros propios complementos de <i>JavaScript</i>. Además, se puedes la versión delgada o redudica de jQuery, pero la versión completa también es compatible.<br>
    Coloque uno de los siguientes &lt;scripts>&gt; cerca del final de sus páginas, justo antes de la etiqueta &lt;body/&gt; de cierre, para habilitarlos.<br>
    Primero debe aparecer jQuery, luego Popper y luego nuestros complementos de JavaScript.</div>

```html
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
```

<div style="text-align: justify;">Considere que se puede incluir todos los complementos de JavaScript Bootstrap con uno de nuestros dos paquetes. Tanto bootstrap.bundle.js como bootstrap.bundle.min.js incluyen Popper para nuestra información sobre herramientas y ventanas emergentes, pero no jQuery, por tanto, se debe incluir jQuery primero, luego un paquete de JavaScript Bootstrap. Para obtener más información sobre lo que se incluye en Bootstrap, consulte nuestra sección de contenido..</div>

```html
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
```

<div style="text-align: justify;">Con Boostrap 5, jQuery no es necesario, es decir, la versión 5 de Bootstrap está diseñado para usarse sin jQuery, pero aún es posible usar nuestros componentes con jQuery.<br>
    Si Bootstrap detecta jQuery en el objeto de la ventana, agregará todos nuestros componentes en el sistema de complementos de jQuery; esto significa que podrá hacer <code>$('[data-bs-toggle = "tooltip"]').tooltip()</code> para habilitar la información sobre herramientas. <br>
    Lo mismo ocurre con nuestros otros componentes.<br>
</div>

```html
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
```

<div style="text-align: justify;">Además, al igual que en versiones anterioes, se puede incluir todos los complementos de JavaScript Bootstrap con uno de nuestros dos paquetes. Tanto bootstrap.bundle.js como bootstrap.bundle.min.jsb.
</div>

```html
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
```

### 1.3. Template integración Thymeleaf + Boostrap 4.6.0 via CDN.

```html
<!doctype html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
  <head>
    <!-- Etiquetas <meta> obligatorias para Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Enlazando el CSS de Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">


    <title>Spring Boot Thymeleaf - Bootstrap WebJars</title>
  </head>
  <body>
    <h1>Hello World!</h1>

    <!-- Opcional: enlazando el JavaScript de Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
```

### 1.4. Template integración Thymeleaf Boostrap 5.0.0. vía CDND.

```html
<!doctype html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
  <head>
    <!-- Etiquetas <meta> obligatorias para Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Enlazando el CSS de Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>Spring Boot Thymeleaf - Bootstrap WebJars</title>
  </head>
  <body>
    <h1>Hello World!</h1>

    <!-- Opcional: enlazando el JavaScript de Bootstrap-->
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
  </body>
</html>
```

## 2. Integración con CSS y JS compilados.

<div style="text-align: justify;">Descargue el código compilado listo para usar para Bootstrap en su web oficial <a link="https://getbootstrap.com">getbootstrap</a> para colocarlo fácilmente en su proyecto, que incluye:
    <ul>
        <li>Los Paquetes de CSS compilados y minimizados (consulte Comparación de archivos CSS)</li>
        <li>Complementos de JavaScript compilzados y minimizados (consulte Comparación de archvios JS)</li>
    </ul>
    En la versión de boostrap 5 (Bootstrap v5.0.0-beta3) no se incluye dependencias de JavaScript opcionales como Popper.<br>
	En el siguiente esquema se puede ver todos los archivos precompilados disponibles tras la descarga para un uso rápido y directo en casi cualquier proyecto web:
    <ul>
        <li>Archivos CSS y JS compilados (bootstrap.*)</li>
        <li>Archivos CSS y JS compilados y minificados (bootstrap.min.*)</li>
        <li>Los mapas de origen (bootstrap. *. map) están disponibles para su uso con las herramientas de desarrollo de ciertos navegadores</li>
        <li>Los archivos JS incluidos (bootstrap.bundle.js y bootstrap.bundle.min.js minificado) incluyen Popper.</li>
    </ul>
	Como se puede comprobar, la dependenda de JavaScript JQuery no esta incluida, esta se ha descargar e insertar en nuestro proyecto de forma manual desde su página oficial <a href="https://code.jquery.com">jquery.com</a>, la cual, ya no es necesaria para boostrap 5 en adelente.</div>

* Archivos compilados disponibles en Boostrap 4.6.0.

```
bootstrap/
├── css/
│   ├── bootstrap-grid.css
│   ├── bootstrap-grid.css.map
│   ├── bootstrap-grid.min.css
│   ├── bootstrap-grid.min.css.map
│   ├── bootstrap-reboot.css
│   ├── bootstrap-reboot.css.map
│   ├── bootstrap-reboot.min.css
│   ├── bootstrap-reboot.min.css.map
│   ├── bootstrap.css
│   ├── bootstrap.css.map
│   ├── bootstrap.min.css
│   └── bootstrap.min.css.map
└── js/
    ├── bootstrap.bundle.js
    ├── bootstrap.bundle.js.map
    ├── bootstrap.bundle.min.js
    ├── bootstrap.bundle.min.js.map
    ├── bootstrap.js
    ├── bootstrap.js.map
    ├── bootstrap.min.js
    └── bootstrap.min.js.map
```

* Archivos compilados disponibles en Boostrap 5.0.0.

```
bootstrap/
├── css/
│   ├── bootstrap-grid.css
│   ├── bootstrap-grid.css.map
│   ├── bootstrap-grid.min.css
│   ├── bootstrap-grid.min.css.map
│   ├── bootstrap-grid.rtl.css
│   ├── bootstrap-grid.rtl.css.map
│   ├── bootstrap-grid.rtl.min.css
│   ├── bootstrap-grid.rtl.min.css.map
│   ├── bootstrap-reboot.css
│   ├── bootstrap-reboot.css.map
│   ├── bootstrap-reboot.min.css
│   ├── bootstrap-reboot.min.css.map
│   ├── bootstrap-reboot.rtl.css
│   ├── bootstrap-reboot.rtl.css.map
│   ├── bootstrap-reboot.rtl.min.css
│   ├── bootstrap-reboot.rtl.min.css.map
│   ├── bootstrap-utilities.css
│   ├── bootstrap-utilities.css.map
│   ├── bootstrap-utilities.min.css
│   ├── bootstrap-utilities.min.css.map
│   ├── bootstrap-utilities.rtl.css
│   ├── bootstrap-utilities.rtl.css.map
│   ├── bootstrap-utilities.rtl.min.css
│   ├── bootstrap-utilities.rtl.min.css.map
│   ├── bootstrap.css
│   ├── bootstrap.css.map
│   ├── bootstrap.min.css
│   ├── bootstrap.min.css.map
│   ├── bootstrap.rtl.css
│   ├── bootstrap.rtl.css.map
│   ├── bootstrap.rtl.min.css
│   └── bootstrap.rtl.min.css.map
└── js/
    ├── bootstrap.bundle.js
    ├── bootstrap.bundle.js.map
    ├── bootstrap.bundle.min.js
    ├── bootstrap.bundle.min.js.map
    ├── bootstrap.esm.js
    ├── bootstrap.esm.js.map
    ├── bootstrap.esm.min.js
    ├── bootstrap.esm.min.js.map
    ├── bootstrap.js
    ├── bootstrap.js.map
    ├── bootstrap.min.js
    └── bootstrap.min.js.map
```

### 2.1. Comparación de archivos CSS.

Bootstrap incluye un puñado de opciones para incluir algunos o todos nuestros CSS compilados.

#### 2.1.1. Boostrap 4.6.0.

| CSS files                                            | Layout                                                       | Content                                                      | Components   | Utilities                                                    |
| ---------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------ | ------------------------------------------------------------ |
| `bootstrap.css` <br>`bootstrap.min.css`              | Included                                                     | Included                                                     | Included     | Included                                                     |
| `bootstrap-grid.css`<br>`bootstrap-grid.min.css`     | [Only grid system](https://getbootstrap.com/docs/4.6/layout/grid/) | Not included                                                 | Not included | [Only flex utilities](https://getbootstrap.com/docs/4.6/utilities/flex/) |
| `bootstrap-reboot.css`<br>`bootstrap-reboot.min.css` | Not included                                                 | [Only Reboot](https://getbootstrap.com/docs/4.6/content/reboot/) | Not included | Not included                                                 |

#### 2.1.2.Boostrap 5.0.0

| CSS files                                                    | Layout                                                       | Content                                                      | Components | Utilities                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ---------- | ------------------------------------------------------------ |
| `bootstrap.css`<br/>`bootstrap.rtl.css`<br/>`bootstrap.min.css`<br/>`bootstrap.rtl.min.css` | Included                                                     | Included                                                     | Included   | Included                                                     |
| `bootstrap-grid.css`<br/>`bootstrap-grid.rtl.css`<br/>`bootstrap-grid.min.css`<br/>`bootstrap-grid.rtl.min.css` | [Only grid system](https://getbootstrap.com/docs/5.0/layout/grid/) | —                                                            | —          | [Only flex utilities](https://getbootstrap.com/docs/5.0/utilities/flex/) |
| `bootstrap-utilities.css`<br/>`bootstrap-utilities.rtl.css`<br/>`bootstrap-utilities.min.css`<br/>`bootstrap-utilities.rtl.min.css` | —                                                            | —                                                            | —          | Included                                                     |
| `bootstrap-reboot.css`<br/>`bootstrap-reboot.rtl.css`<br/>`bootstrap-reboot.min.css`<br/>`bootstrap-reboot.rtl.min.css` | —                                                            | [Only Reboot](https://getbootstrap.com/docs/5.0/content/reboot/) | —          | —                                                            |

### 2.2. Comparación de archivos JS.

Del mismo modo, tenemos opciones para incluir algunos o todos nuestros JavaScript compilados.

#### 2.2.1. Boostrap 4.6.0

| JS files                                            | Popper       | jQuery       |
| --------------------------------------------------- | ------------ | ------------ |
| `bootstrap.bundle.js`<br/>`bootstrap.bundle.min.js` | Included     | Not included |
| `bootstrap.js`<br/>`bootstrap.min.js`               | Not included | Not included |

#### 2.2.2. Boostrap 5.0.0.

| JS files                                            | Popper   |
| --------------------------------------------------- | -------- |
| `bootstrap.bundle.js`<br/>`bootstrap.bundle.min.js` | Included |
| `bootstrap.js`<br/>`bootstrap.min.js`               | —        |

### 2.3. Template integación Thumeleaf Boostrap

Tras la descarga de **Boostrap** y **Jquery**, copiamos los ficheros en nuestro proyecto en la ruta "*static*" por defecto.

![](..\img\anexos\boostrap\img1.png)

Añadimos los ficheros a nuestro html mediante las etiquetas '*script*', *link*, ...

#### 2.3.1 Integracion en HTML 5.

```html
<!doctype html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
  <head>
    <!-- Etiquetas <meta> obligatorias para Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Enlazando el CSS de Bootstrap -->
	<link href="/bootstrap-5.0.0/css/bootstrap.css" rel="stylesheet" />
    <title>Spring Boot Thymeleaf - Bootstrap WebJars</title>
  </head>
  <body>
    <h1>Hola Mundo!</h1>
      
    <!-- Opcional: enlazando el JavaScript de Bootstrap -->
    <!-- Orden de inserccion:
    	1º jquery-3.3.1.slim.min.js (u otra version)
    	2º popper.min.js
    	3º bootstrap.min.js
    Nota:
    Si utilizas bootstrap.bundle.js o bootstrap.bundle.min.js, la libreria Popper ya esta incluida, pero jQuery no. 	-->
    <script src="/jquery/3.0.0/jquery-3.0.0.min.js"></script>
    <script src="/bootstrap-5.0.0/js/bootstrap.bundle.js"></script>  
  </body>
</html>
```

#### 2.3.2. Integración con Thymeleaf.

```html
<!doctype html>
<html lang="es" xmlns:th="http://www.thymeleaf.org">
  <head>
    <!-- Etiquetas <meta> obligatorias para Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Enlazando el CSS de Bootstrap -->
	<link th:href="@{/bootstrap-5.0.0/css/bootstrap.css}" rel="stylesheet" />
    <title>Spring Boot Thymeleaf - Bootstrap WebJars</title>
  </head>
  <body>
    <h1>Hola Mundo!</h1>
      
    <!-- Opcional: enlazando el JavaScript de Bootstrap -->
    <!-- Orden de inserccion:
    	1º jquery-3.3.1.slim.min.js (u otra version)
    	2º popper.min.js
    	3º bootstrap.min.js
    Nota:
    Si utilizas bootstrap.bundle.js o bootstrap.bundle.min.js, la libreria Popper ya esta incluida, pero jQuery no. 	-->
    <script th:src="@{/jquery/3.0.0/jquery-3.0.0.min.js}"></script>
    <script th:src="@{/bootstrap-5.0.0/js/bootstrap.bundle.js}"></script>  
  </body>
</html>
```

