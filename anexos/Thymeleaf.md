# Thymeleaf

[TOC]

## Incorporar Thymeleaf en nuestro documento HTML.

```html
<!DOCTYPE HTML>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    ...
</head>
<body>
	...
</body>
</html>
```

## Texto

### Get Atributos.

```html
<etiqueta th:text="'[optional_text]'+#{attribute}"><etiqueta>
<etiqueta th:text="'[optional_text]'+#{attribute(param1, param2, ...)}"><etiqueta>
<etiqueta data-th-text="'[optional_text]'+#{attribute}"></etiqueta>
<etiqueta data-th-text="'[optional_text]'+#{attribute(param1, param2, ...)}"></etiqueta>  
<etiqueta>[[${attribute}]]</etiqueta>
<etiqueta>[[${attribute(param1, param2, ...)}]]</etiqueta>
```

### Get texto Externalizado.

> Nota: Su uso depende la creación de un beans.

```html
<etiqueta>[[#{text}]]</etiqueta>
<etiqueta>[[#{text(param1, param2, ...)}]]</etiqueta>
```

## IF-ELSE

Condiciones:

* gt (>), lt (<)

 <ul>
			<li>gt (>), lt (<) </li>
			<li>ge (>=), le (<=) </li>
			<li>not (!) </li>
			<li>eq (==), neq/ne (!=) </li>
         </ul>

```html
<!-- operador elvis -->
<etiqueta> th:text="${condicion} ? sentencias-true : sentences-false" </etiqueta> 
<!-- atritubo etiqueta -->
<etiqueta th:if="${condicion}">La variable "status" es true.</etiqueta>
<etiqueta th:unless="${condicion}">La variable "status" es false.</etiqueta>
```

## Swich-Case

```html
<etiqueta th:switch="atributo">
  <etiqueta th:case="condicion">Caso A</etiqueta>
  <etiqueta th:case="condicion">Caso B</etiqueta>
  ...
  <etiqueta th:case="condicion">Caso N</etiqueta>
  <etiqueta th:case="*">Caso por defectog</etiqueta>
</etiqueta>
```

## Each

```html
<!-- Opción A-->
<etiqueta th:each="variable:atributo" th:text="variable"></etiqueta>

<!-- Opción B-->
<etiqueta th:each="variable:atributo">
	<etiqueta th:text="variable" /> o <etiqueta>[[${variable}]]</etiqueta>
</etiqueta>
```

## Template o Layout

```html
<!-- Contenido marcado como fragmento -->
<etiqueta th:fragment="name_fragment">
    ....
</etiqueta>

<!-- Invocación -->
<etiqueta th:insert="ruta del html :: name del fragmento" />
<etiqueta th:remplace="ruta del html :: name del fragmento" />
<etiqueta th:include="ruta del html :: name del fragmento" />
```

