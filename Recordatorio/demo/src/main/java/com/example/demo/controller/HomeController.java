package com.example.demo.controller;

import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
    public String getIndex(Model model){
        model.addAttribute("mensaje","Andres!");
        model.addAttribute("fecha",new Date());
        return "/body/IndexView";
    }
	
	@GetMapping("/loggin")
	public String getLogginView(Model model) {
		 
		return "loggin";
	}
}
