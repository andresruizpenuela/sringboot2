package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

@Controller
public class HomeController {
    @GetMapping("/")
    public String getIndex(Model model){
        // model.addAttribute("mensaje","Hola Mundo!");
        model.addAttribute("fecha",new Date());
        return "index";
    }
}
